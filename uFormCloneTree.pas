unit uFormCloneTree;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  dmData, Data.DB,  adsdata, adsfunc, adstable, adsset, Vcl.DBCtrls,
  Vcl.ComCtrls, dxtree, dxdbtree;

type
  Tfrm_CloneTree = class(TForm)
    Label1: TLabel;
    edt_stationdept: TEdit;
    Panel1: TPanel;
    btn_OK: TBitBtn;
    btn_Cancel: TBitBtn;
    rg_TreeBranch: TRadioGroup;
    cb_stationDept: TComboBox;
    btn_selectDept: TBitBtn;
    dsDestTreeTags: TDataSource;
    rg_destConfig: TRadioGroup;
    pnl_DestBranch: TPanel;
    Label2: TLabel;
    dxDBTreeView1: TdxDBTreeView;
    procedure btn_OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btn_selectDeptClick(Sender: TObject);
    procedure cb_stationDeptClick(Sender: TObject);
    procedure edt_stationdeptExit(Sender: TObject);
    procedure rg_destConfigClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure setStationDeptItems(list : TStrings);
  end;

var
  frm_CloneTree: Tfrm_CloneTree;

implementation

{$R *.dfm}


procedure Tfrm_CloneTree.setStationDeptItems(list : TStrings);
var
  i : integer ;
Begin
   i:=0;
   cb_stationDept.Clear;
   while i < list.Count do
   begin
      cb_stationDept.Items.Add(list[i]);
      i := i + 1;
   end;

End;

procedure Tfrm_CloneTree.btn_OKClick(Sender: TObject);
begin
  ModalResult := mrNone;

  if edt_stationdept.Text = '' then
  begin
    ShowMessage('Please fill in the Station name...');
    exit;
  end;


  if (DataModule1.checkStationExists(edt_stationdept.Text)) and
     (rg_TreeBranch.ItemIndex < 0)  then
  begin
    ShowMessage('Please select under which branch of Destination Station Dept!' + #13 +
                'you would like to clone selected sub-tree...');
    exit;
  end;



  ModalResult := mrOk;

end;

procedure Tfrm_CloneTree.btn_selectDeptClick(Sender: TObject);
begin
{  With DataModule1 do
  Begin
      q_StationDept.Close;
      q_StationDept.Open;
      q_StationDept.First;
      cb_stationDept.Items.Clear;
      While not q_StationDept.Eof do
      begin
        cb_stationDept.Items.Add(q_StationDept.FieldByName('STATION_DEPT').AsString);
        q_StationDept.Next;
      end;

      cb_stationDept.Visible := True;
      cb_stationDept.DroppedDown := True;
  End;
}
      cb_stationDept.Visible := True;
      cb_stationDept.DroppedDown := True;

end;

procedure Tfrm_CloneTree.cb_stationDeptClick(Sender: TObject);
begin
  if cb_stationDept.ItemIndex >= 0 then
  begin
    edt_stationdept.Text := cb_stationDept.Items[cb_stationDept.ItemIndex];
    cb_stationDept.Visible := False;
    rg_destConfigClick(nil);
  end;
end;

procedure Tfrm_CloneTree.edt_stationdeptExit(Sender: TObject);
begin
   if (edt_stationdept.Text <> '')  then
     rg_destConfigClick(nil);
end;

procedure Tfrm_CloneTree.FormActivate(Sender: TObject);
begin
  if edt_stationdept.Text <> '' then
         rg_destConfigClick(self);
end;

procedure Tfrm_CloneTree.FormCreate(Sender: TObject);
begin
   pnl_DestBranch.Enabled := false ;
   cb_stationDept.Visible := false ;
end;

procedure Tfrm_CloneTree.rg_destConfigClick(Sender: TObject);
var
  q : TadsQuery ;
begin
   if rg_destConfig.ItemIndex = 0 then
          pnl_DestBranch.Enabled := false
   else if rg_destConfig.ItemIndex = 1 then
   begin
      try
          if edt_stationdept.Text <> ''
             //and DataModule1.checkStationExists(edt_stationdept.Text)
             then
          begin
            DataModule1.OpenDestStationQuery(edt_stationdept.Text);
            if Assigned( dxDBTreeView1.Selected ) then
               dxDBTreeView1.Selected.Expand(false);
            pnl_DestBranch.Enabled := true ;

          end
          else
          begin
            ShowMessage('Please fill in the Station name...');
            rg_destConfig.ItemIndex := 0;
            exit;
          end;
      except
           rg_destConfig.ItemIndex := 0;
      end;
   end;
   pnl_DestBranch.Invalidate;
end;


end.
