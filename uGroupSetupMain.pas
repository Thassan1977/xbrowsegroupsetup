unit uGroupSetupMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, cxClasses, cxPropertiesStore, Vcl.Menus;

type
  TfrmMain = class(TForm)
    cxPropertiesStore1: TcxPropertiesStore;
    pnlAction: TPanel;
    btnExit: TButton;
    Button1: TButton;
    flwpnlSetUp1: TFlowPanel;
    lbledtPrimaryAlias: TLabeledEdit;
    btnSetup: TButton;
    chkLockLookAdnFeel: TCheckBox;
    procedure btnExitClick(Sender: TObject);
    procedure btnSetupClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses uformGroupSetup, dmData;

procedure TfrmMain.btnExitClick(Sender: TObject);
begin
  cxPropertiesStore1.StoreTo();
  Application.Terminate;
end;


procedure TfrmMain.btnSetupClick(Sender: TObject);
begin
 { TODO : Password protect }
 flwpnlSetUp1.Visible := (not flwpnlSetUp1.Visible) ;
end;

procedure TfrmMain.Button1Click(Sender: TObject);
begin
  frmGroupSetup.Show;
end;



procedure TfrmMain.FormShow(Sender: TObject);
begin
  cxPropertiesStore1.RestoreFrom;
  dmData.DataModule1.POSConnectionBeforeConnect ;
  dmData.DataModule1.LoadData ;
end;

end.
