unit uformGroupSetup;
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, cxTextEdit, Vcl.StdCtrls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  dxTokenEdit, Vcl.Samples.Spin, cxMaskEdit, cxDropDownEdit, cxColorComboBox, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, Vcl.ExtCtrls, Utils, math,
  dximctrl, cxImageComboBox, LMDControl, LMDCustomControl, LMDCustomPanel, LMDCustomBevelPanel, LMDBaseEdit,
  LMDCustomEdit,
  LMDCustomBrowseEdit, LMDCustomFileEdit, LMDFileOpenEdit, Vcl.DBCtrls, Vcl.Buttons, dxDateRanges, Vcl.ComCtrls, dxtree,
  dxdbtree, cxTL, cxTLdxBarBuiltInMenu, cxInplaceContainer, cxTLData, cxDBTL, Vcl.AppEvnts,
  dxSkinsDefaultPainters, dxdbtrel, Vcl.Menus, Vcl.Mask, System.Generics.Collections, uFormAddStationDept,
  LMDCustomMaskEdit, LMDCustomExtCombo, LMDCustomListComboBox, LMDDBFieldComboBox, uGroupSetupMain,
  cxSpinEdit, cxDBEdit, dxScrollbarAnnotations;

Const
  DEFAULT_TREE_TEXT = 'DESC_CNT';

type
  TmyColrList = record
    Name: string;
    hex500: string;
    hexA100: string;
  end;
  
  TTreeDataPtr = ^TTreeData;

  TTreeData = Record
    INDX: integer;
    MORDER: String;
    MY_TAG: String;
    PARENT_TAG: String;
    DESC: String;
    OUTPUT: String;
    CARD_COLOR: integer;
    STATION_DEPT: String;
    BREADCRUMBS: String;
    SMLDESC: String;
    INACTIVE: Boolean;
    LINEID: String;
    CARD_FONT: String;
    CARD_WIDTH: integer;
    CARD_DESC_LINES: integer;
    CARD_SORT_BY: String;
    CARD_AS_GRID: Boolean;
    CARD_PICTURE: String;
    CARD_SHOW_PRICE: Boolean;
    CARD_SHOW_CODE: Boolean;
    CARD_SHOW_INACTIVE: Boolean;
    CARD_PRESTRING: String;
    CARD_USE_MODIFIER: Boolean;
    CODE: String;
    Price: Real;

  End;

  TSearchData = class(TObject)
    MY_TAG: String;
    PARENT_TAG: String;
    DESC: String;
  End;

  TfrmGroupSetup = class(TForm)
    pnlCard: TPanel;
    btnCardFont: TLMDSpeedButton;
    btnCardSample: TLMDSpeedButton;
    lblCard: TLabel;
    cxColorComboBoxCard: TcxColorComboBox;
    pnlButton: TPanel;
    btnUpdate: TButton;
    pnlMisc: TPanel;
    lblMiscHeading: TLabel;
    lblinfo2: TLabel;
    lbledtPicture222: TLMDLabeledFileOpenEdit;
    pnlTagfilter: TPanel;
    SourceTag: TLabel;
    pnlBotm: TPanel;
    edtTestfilter: TEdit;
    btnRefresh: TSpeedButton;
    pnlDescrip: TPanel;
    lblShowsup: TLabel;
    pnlType: TPanel;
    lblType: TLabel;
    icbTable: TcxImageComboBox;
    lblCaption: TLabel;
    ApplicationEvents1: TApplicationEvents;
    lblParenValue: TLabel;
    lbMyTag: TLabel;
    lblMyTagValue: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Splitter2: TSplitter;
    dsTagGroup: TDataSource;
    dxDBTreeView1: TdxDBTreeView;
    Label1: TLabel;
    cb_searchCombo: TComboBox;
    btn_search: TButton;
    ds_search: TDataSource;
    Panel6: TPanel;
    Panel7: TPanel;
    lbl_breadcrumb: TLabel;
    cardFontDialog: TFontDialog;
    pnlMyTag: TPanel;
    lblSelfTag: TLabel;
    rg_colexpand: TRadioGroup;
    PopupMenu1: TPopupMenu;
    mnuCreateChild: TMenuItem;
    rg_NodeName: TRadioGroup;
    lbledtCaption: TDBEdit;
    edtTagList: TDBEdit;
    lbledtTagFilter: TDBEdit;
    btn_Clear: TButton;
    btnDeleteNode: TButton;
    btnAddChild: TButton;
    btnAddStationDept: TButton;
    cb_SearchStationDept: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    mnuCreateParent: TMenuItem;
    N1: TMenuItem;
    DeleteSelectedNode1: TMenuItem;
    Panel9: TPanel;
    btn_UpNode: TBitBtn;
    btn_DownNode: TBitBtn;
    btn_tagDropDown: TSpeedButton;
    dsTagDropDown: TDataSource;
    cb_tagDropDown: TComboBox;
    N3: TMenuItem;
    mnuCloneTreeByStationDept: TMenuItem;
    lblCardDescLines: TLabel;
    lblCardWidth: TLabel;
    pnlSortby: TPanel;
    lblSortby: TLabel;
    icbSort: TcxImageComboBox;
    icbStationDept: TcxImageComboBox;
    Image1: TImage;
    Makeselectednodearootnode1: TMenuItem;
    chkCardPrice: TDBCheckBox;
    chkCardCode: TDBCheckBox;
    seCardWidth: TcxDBSpinEdit;
    lbledtCardPreCode: TDBEdit;
    chkUseModifier: TDBCheckBox;
    chkshowInactive: TDBCheckBox;
    seDescLines: TcxDBSpinEdit;
    edtCardFont: TDBEdit;
    Label2: TLabel;
    dlg_selectPicture: TOpenDialog;
    pnlPicture: TPanel;
    Label3: TLabel;
    lbledtPicture: TDBEdit;
    btn_browsePics: TBitBtn;
    cxGrid1: TcxGrid;
    TableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    TableView1MY_TAG: TcxGridDBColumn;
    TableView1PARENT_TAG: TcxGridDBColumn;
    TableView1desc: TcxGridDBColumn;
    TableView1OUTPUT: TcxGridDBColumn;
    TableView1CardColor: TcxGridDBColumn;
    TableView1STATION_DEPT: TcxGridDBColumn;
    TableView1BREADCRUMBS: TcxGridDBColumn;
    TableView1smldesc: TcxGridDBColumn;
    TableView1InActive: TcxGridDBColumn;
    TableView1LineID: TcxGridDBColumn;
    TableView1CardFont: TcxGridDBColumn;
    TableView1CardWidth: TcxGridDBColumn;
    TableView1CardDescLines: TcxGridDBColumn;
    TableView1CardSortBy: TcxGridDBColumn;
    TableView1CardAsGrid: TcxGridDBColumn;
    TableView1CardPicture: TcxGridDBColumn;
    TableView1CardShowPrice: TcxGridDBColumn;
    TableView1CardShowCode: TcxGridDBColumn;
    TableView1CardShowInactive: TcxGridDBColumn;
    TableView1CardPreString: TcxGridDBColumn;
    TableView1CardUseModifier: TcxGridDBColumn;
    TableView1code: TcxGridDBColumn;
    TableView1price: TcxGridDBColumn;
    TableView1TAG_CNT: TcxGridDBColumn;
    TableView1DESC_CNT: TcxGridDBColumn;
    TableView1BOTH_CNT: TcxGridDBColumn;
    TableView1INDX: TcxGridDBColumn;
    TableView1MORDER: TcxGridDBColumn;
    DBNavigator1: TDBNavigator;

    procedure cxColorComboBoxCardPropertiesChange(Sender: TObject);
    procedure btnUpdateClick(Sender: TObject);
    procedure dsTagGroupDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure rg_colexpandClick(Sender: TObject);
    procedure dxDBTreeView1RefreshNode(Sender: TObject; DBTreeNode: TdxDBTreeNode);
    procedure dxDBTreeView1CustomDraw(Sender: TObject; TreeNode: TTreeNode; AFont: TFont; var AColor, ABkColor: TColor);
    procedure btnCardFontClick(Sender: TObject);
    procedure mnuCreateChildClick(Sender: TObject);
    procedure rg_NodeNameClick(Sender: TObject);
    procedure btn_searchClick(Sender: TObject);
    procedure btn_ClearClick(Sender: TObject);
    procedure q_TagGroupAfterScroll(DataSet: TDataSet);
    procedure btnAddChildClick(Sender: TObject);
    procedure btnDeleteNodeClick(Sender: TObject);
    procedure btnAddStationDeptClick(Sender: TObject);
    procedure mnuCreateParentClick(Sender: TObject);
    procedure DeleteSelectedNode1Click(Sender: TObject);
    procedure cb_SearchStationDeptClick(Sender: TObject);
    procedure btn_UpNodeClick(Sender: TObject);
    procedure btn_DownNodeClick(Sender: TObject);
    procedure btn_tagDropDownClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dxDBTreeView1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
    procedure mnuCloneTreeByStationDeptClick(Sender: TObject);
    procedure Makeselectednodearootnode1Click(Sender: TObject);
    procedure dxDBTreeView1Click(Sender: TObject);
    function breadCrumbsGetSplit(Delimiter: Char; Str: string): TStringList;
    function GuidIdString: string;
    procedure dxDBTreeView1DragDropTreeNode(Destination, Source: TTreeNode;
      var Accept: Boolean);
    procedure dxDBTreeView1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure icbTablePropertiesChange(Sender: TObject);
    procedure icbSortPropertiesChange(Sender: TObject);
    procedure cxColorComboBoxCardExit(Sender: TObject);
    procedure seCardWidthClick(Sender: TObject);
    procedure btn_browsePicsClick(Sender: TObject);
    procedure cb_tagDropDownCloseUp(Sender: TObject);
  private
    StationDept_List: TDictionary<String, String>;
    FLastLoadedImageFile: string;
    Mutex: Cardinal;

    function getNodeColor(nodeColor: integer; parentKey,pStationDept: String): integer;
    function getBreadCrumnbs(pStationDept, pBreadCrumbs: String): String;
    procedure colrfil;
    procedure FilterByTag(sFilter, Sfield: string);
    procedure prepareTree(displayField: String = DEFAULT_TREE_TEXT);
    procedure populateCombos;

    function readTagRow(DataSet: TDataSet): TTreeDataPtr;
    procedure displayTagProperties(ADataSet: TDataSet);
    procedure populateMyTagFromTagDropDown(pNewVal : String);
    procedure DoFirst;
    { Private declarations }
  public
    { Public declarations }
    procedure fixTreeNodeType(pParentTag, pStationDept: String);
  end;

var
  frmGroupSetup: TfrmGroupSetup;
  FGuidLastTimePart: integer;

const
  SWindowClassName = 'XBrowse2RegisterHelperApp';
  clrList: array [1 .. 21] of TmyColrList = ((name: 'Red'; hex500: '$F44336'; hexA100: '$FF8A80'), (name: 'Pink';
    hex500: '$E91E63'; hexA100: '$FF80AB'), (name: 'Purple'; hex500: '$9C27B0'; hexA100: '$EA80FC'),
    (name: 'Deep Purple'; hex500: '$673AB7'; hexA100: '$B388FF'), (name: 'Indigo'; hex500: '$3F51B5';
    hexA100: '$8C9EFF'), (name: 'Blue'; hex500: '$2196F3'; hexA100: '$82B1FF'), (name: 'Light Blue'; hex500: '$03A9F4';
    hexA100: '$80D8FF'), (name: 'Cyan'; hex500: '$00BCD4'; hexA100: '$84FFFF'), (name: 'Teal'; hex500: '$009688';
    hexA100: '$A7FFEB'), (name: 'Green'; hex500: '$4CAF50'; hexA100: '$B9F6CA'), (name: 'Light Green';
    hex500: '$8BC34A'; hexA100: '$CCFF90'), (name: 'Lime'; hex500: '$CDDC39'; hexA100: '$F4FF81'), (name: 'Yellow';
    hex500: '$FFEB3B'; hexA100: '$FFFF8D'), (name: 'Amber'; hex500: '$FFC107'; hexA100: '$FFE57F'), (name: 'Orange';
    hex500: '$FF9800'; hexA100: '$FFD180'), (name: 'Deep Orange'; hex500: '$FF5722'; hexA100: '$FF9E80'),
    (name: 'Brown'; hex500: '$795548'; hexA100: '$A1887F'), (name: 'Grey'; hex500: '$9E9E9E'; hexA100: '$BDBDBD'),
    (name: 'Blue Grey'; hex500: '$607D8B'; hexA100: '$CFD8DC'), (name: 'Black'; hex500: '$000000'; hexA100: '$000000'),
    (name: 'White'; hex500: '$FFFFFF'; hexA100: '$FFFFFF'));

implementation

// uses PrdData, MainBrowse;

uses dmData, uFormCloneTree;
{$R *.dfm}

function TfrmGroupSetup.getNodeColor(nodeColor: integer; parentKey,pStationDept: String): integer;
begin
  if (nodeColor = 0) and (parentKey <> '') then
    Result := DataModule1.getParentColor(parentKey, pStationDept)
  else
    Result := nodeColor;
end;

function TfrmGroupSetup.getBreadCrumnbs(pStationDept, pBreadCrumbs: String): String;
begin
  if pStationDept <> '' then
    Result := pBreadCrumbs.Replace(pStationDept + '|', '', [])
  else
    Result := pBreadCrumbs;
end;

procedure TfrmGroupSetup.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
var
  s: string;
begin
  // btnTagsetup.Enabled := (icbTable.ItemIndex=1) and (lbledtTagFilter.Text >'')  ;
  s := IIF(icbTable.ItemIndex = 1, 'My Tag - Parent of Sub-Menu', 'My Tag - Use same TAG as Items to display');
  lblSelfTag.Caption := s;
  chkCardCode.Visible := (icbTable.ItemIndex = 0);
  chkCardPrice.Visible := (icbTable.ItemIndex = 0);
  pnlSortby.Visible := (icbTable.ItemIndex = 0);
  lbledtPicture.Visible := (icbTable.ItemIndex = 1);
  pnlPicture.Visible :=  (icbTable.ItemIndex = 1);
  // Image1.Visible := (icbTable.ItemIndex = 1);
  if (lbledtPicture.Text <> '') and FileExists(lbledtPicture.Text) then
  begin
    if FLastLoadedImageFile <> lbledtPicture.Text then
      try
        FLastLoadedImageFile := lbledtPicture.Text;
        Image1.Picture.LoadFromFile(lbledtPicture.Text);
        Image1.Visible := True;
      except
      end;
  end
  else
  begin
    Image1.Visible := False;
    FLastLoadedImageFile := '';
  end;
  // chkCardAsGrid.Visible := (icbTable.ItemIndex=0) ;
  // pnlGrid.Visible :=        (icbTable.ItemIndex=0) ;
  // pnlCard.Visible := not    chkCardAsGrid.Checked ;
end;

procedure TfrmGroupSetup.btnAddChildClick(Sender: TObject);
begin
  mnuCreateChildClick(self);
end;

procedure TfrmGroupSetup.btnAddStationDeptClick(Sender: TObject);
var
  pCurrent_Station: string;
  Item: TcxImageComboBoxItem;
begin
  if not Assigned(frm_AddStatioDept) then
    frm_AddStatioDept := Tfrm_AddStatioDept.Create(self);
  if frm_AddStatioDept.ShowModal = mrOK then
  begin
    pCurrent_Station := { UpperCase( } frm_AddStatioDept.edt_stationdept.Text { ) };
    // add Item to Station Combo
    Item := icbStationDept.Properties.Items.Add;
    Item.Value := pCurrent_Station;
    Item.Description := pCurrent_Station;
    Item.ImageIndex := -1;

    // add new Item to  StationDept_List
    StationDept_List.Add(pCurrent_Station, pCurrent_Station);

    // station search combo
    cb_SearchStationDept.Items.Add(pCurrent_Station);
    // set currently added Station  in cb_SearchStationDept
    cb_SearchStationDept.ItemIndex := cb_SearchStationDept.Items.Count - 1;

    // do search for the newly added station
    btn_searchClick(self);
  end;
end;

procedure TfrmGroupSetup.btnCardFontClick(Sender: TObject);
begin
  if cardFontDialog.Execute then
  begin
    edtCardFont.Text := cardFontDialog.Font.Name;
    btnCardSample.Font := cardFontDialog.Font;


  end;
end;

procedure TfrmGroupSetup.btnDeleteNodeClick(Sender: TObject);
var
  ADataSet: TDataSet;
begin
  if MessageDlg('Are you sure you want to delete node "' + dxDBTreeView1.Selected.Text + '" ?', mtConfirmation,
    [mbYes, mbNo], 0) = mrNo then
    Exit;

  // if node has children then exit
  if dxDBTreeView1.Selected.HasChildren then
  Begin
    ShowMessage('can''t delete node, The selected node has children');
    Exit;
  End;

  ADataSet := dxDBTreeView1.DataSource.DataSet;

  // delete node
  DataModule1.deleteNode(ADataSet.FieldByName('My_TAG').AsString, ADataSet.FieldByName('PARENT_TAG').AsString,
    ADataSet.FieldByName('STATION_DEPT').AsString);
  dxDBTreeView1.Selected.Delete;
  fixTreeNodeType(ADataSet.FieldByName('PARENT_TAG').AsString, ADataSet.FieldByName('STATION_DEPT').AsString);
  ADataSet.Refresh;
  dxDBTreeView1.Refresh;
end;

procedure TfrmGroupSetup.btnRefreshClick(Sender: TObject);
var
  AColumn: TcxGridColumn;
begin

  AColumn := TableView1.GetColumnByFieldName('PARENT_TAG');
  FilterByTag(edtTestfilter.Text, 'PARENT_TAG');
  if Assigned(AColumn) then
    AColumn.Visible := not(edtTestfilter.Text > '');
  pnlTagfilter.Visible := not(edtTestfilter.Text > '');
  TableView1.ApplyBestFit;
end;

procedure TfrmGroupSetup.btn_browsePicsClick(Sender: TObject);
var
  ADataset : TDataSet ;
begin
  ADataset := dsTagGroup.DataSet ;
  if dlg_selectPicture.Execute then
  begin
      if not (ADataset.State in [dsInsert, dsEdit]) then
           ADataset.Edit;

      ADataSet.FieldByName('CardPicture').AsString := dlg_selectPicture.FileName;

  end;

end;

function TfrmGroupSetup.breadCrumbsGetSplit(Delimiter: Char; Str: string): TStringList;
var
  ListOfStrings, res: TStringList;
begin

  res := TStringList.Create;
  try
    ListOfStrings := TStringList.Create;

    ListOfStrings.Clear;
    ListOfStrings.Delimiter := Delimiter;
    ListOfStrings.StrictDelimiter := True;
    ListOfStrings.DelimitedText := Str;

    res.Clear;
    res.Add(ListOfStrings[ListOfStrings.Count - 1]);
    res.Add(copy(Str, 0, Length(Str) - Length(ListOfStrings[ListOfStrings.Count - 1])));

  finally
    ListOfStrings.Free;
  end;
  Result := res;
end;

procedure TfrmGroupSetup.btnUpdateClick(Sender: TObject);
var
  ADataSet: TDataSet;
  sCaption, sTagSort, sTable, sPicture, sTagFilter, sCardPreCode, sCardFont, sTagList, sStationDept, old_myTag,
    old_station_dept, sTmp, parent_breadCrumbs: string;
  bCardCode, bCardPrice, bshowInactive, bUseModifier, bStationDeptChanged: Boolean;
  iDescLines, iCardWidth, iCardColor: integer;
  breadCrumbsList: TStringList;
begin
  ADataSet := dsTagGroup.DataSet;
  if not( ADataSet.State in [dsEdit, dsInsert]) then
     exit;

  if lbledtTagFilter.Text = '' then
  begin
    ShowMessage('My-Tag Cant be empty');
    Exit
  end;




  try

 {
    old_station_dept := ADataSet.FieldByName('STATION_DEPT').AsString;

    breadCrumbsList := breadCrumbsGetSplit('|', ADataSet.FieldByName('BREADCRUMBS').AsString);
    if breadCrumbsList.Count <= 1 then
    begin
      ShowMessage(' Can''t update node data...');
      Exit
    end;

    old_myTag := breadCrumbsList[0];
    parent_breadCrumbs := breadCrumbsList[1];



    // old_myTag := ADataSet.FieldByName('MY_TAG').asString;
    // showmessage( );

    if edtTestfilter.Text > '' then
      edtTagList.Text := edtTestfilter.Text;

    sCaption := lbledtCaption.Text;
    sTagSort := VarToStr(icbSort.EditValue);

    if icbStationDept.ItemIndex <= 0 then
      sStationDept := ''
    else
      sStationDept := VarToStr(icbStationDept.EditValue);

    sTable := VarToStr(icbTable.EditValue);

    sPicture := lbledtPicture.Text;
    sTagFilter := lbledtTagFilter.Text;

    sCardPreCode := lbledtCardPreCode.Text;
    sTagList := edtTagList.Text;
    bCardCode := chkCardCode.Checked;
    bCardPrice := chkCardPrice.Checked;
    bshowInactive := chkshowInactive.Checked;
    bUseModifier := chkUseModifier.Checked;
    sCardFont := edtCardFont.Text;
    iCardColor := cxColorComboBoxCard.ColorValue;
    iDescLines := seDescLines.Value;
    iCardWidth := seCardWidth.Value;

    bStationDeptChanged := (not old_station_dept.Equals(sStationDept));

    if (bStationDeptChanged) and (not String.IsNullOrEmpty(sTagList)) then
    begin
      ShowMessage('Can''t change Station-Dept for child node');
      Exit
    end;

    ADataSet.Edit;

    // check if tag is already used in database within the same station_dept
    if (not old_myTag.Equals(sTagFilter)) and (DataModule1.checkTagExists(sTagFilter, sStationDept)) then
    begin
      ADataSet.FieldByName('My_TAG').AsString := old_myTag;
      lbledtTagFilter.Text := old_myTag;
      lbledtTagFilter.SetFocus;
      ShowMessage('Item Tag already exist in tree, please choose another Item Tag.');
      abort;
    end;

    //ADataSet.FieldByName('BREADCRUMBS').AsString := parent_breadCrumbs + sTagFilter;

    ADataSet.FieldByName('Desc').AsString := sCaption;
    ADataSet.FieldByName('CardWidth').AsInteger := iCardWidth;
    ADataSet.FieldByName('CardSortBy').AsString := sTagSort;
    ADataSet.FieldByName('CardDescLines').AsInteger := iDescLines;
    ADataSet.FieldByName('output').AsString := sTable;
    ADataSet.FieldByName('PARENT_TAG').AsString := sTagList;
    ADataSet.FieldByName('STATION_DEPT').AsString := sStationDept;
    ADataSet.FieldByName('CardFont').AsString := sCardFont;
    ADataSet.FieldByName('CardShowCode').AsBoolean := bCardCode;
    ADataSet.FieldByName('CardShowPrice').AsBoolean := bCardPrice;
    ADataSet.FieldByName('CardColor').AsInteger := iCardColor;
    ADataSet.FieldByName('CardPicture').AsString := sPicture;
    ADataSet.FieldByName('My_TAG').AsString := sTagFilter;
    ADataSet.FieldByName('CardShowInactive').AsBoolean := bshowInactive;
    ADataSet.FieldByName('CardPreString').AsString := sCardPreCode;
    ADataSet.FieldByName('CardUseModifier').AsBoolean := bUseModifier;
    if ADataSet.FieldByName('code').AsString.IsEmpty then
      ADataSet.FieldByName('code').AsString := GuidIdString;

    // update breadcrumbs in case station dept is changed
    // sTmp := ADataSet.FieldByName('BREADCRUMBS').AsString;
    // sTmp := sStationDept + Copy(sTmp, pos('|', sTmp), Length(sTmp));

    // ADataSet.FieldByName('BREADCRUMBS').AsString := sStationDept + '|' + lbl_breadcrumb.Caption;
    ADataSet.FieldByName('BREADCRUMBS').AsString := parent_breadCrumbs + sTagFilter;
   }
    ADataSet.Post;
    {

    // in case user changed the station-dept of a tree branch
    // all children in that branch needs to
    // if and only if the current node is a root node
    // or else the branch will move to the newly selected station_dept as orphan
    // with no root ....

    try
      if not old_station_dept.equals(sStationDept) then
        DataModule1.changeBranchStationDept(sTagFilter,
           old_station_dept,
           sStationDept
        );
    except

    end;

    // fix breadcrumbs when my_tag changes...
    // it's important to fix breadcrumbs since we use it
    // to traverse tree and to order nodes ...
    if not old_myTag.Equals(sTagFilter) then
      DataModule1.updateNodeBreadccrumbs(old_myTag, sTagFilter, sStationDept);
    }
    //fixTreeNodeType(sTagFilter);

    ADataSet.Refresh;
    dxDBTreeView1.RefreshItems;
    if Assigned(dxDBTreeView1.Selected) then
      dxDBTreeView1.Selected.expand(True);

  except
    ADataSet.Cancel;
    ShowMessage('Error: when trying to update node properties...!');
  end;

end;

procedure TfrmGroupSetup.btn_ClearClick(Sender: TObject);
begin
  cb_searchCombo.ItemIndex := -1;
  DataModule1.searchTagTable('');

end;

procedure TfrmGroupSetup.btn_DownNodeClick(Sender: TObject);
var
  ADataSet: TDataSet;
  SavePlace: TBookmark;
  myTag, parentTag, stationDept, BREADCRUMBS: string;
  aDirection: integer;
begin

  ADataSet := dxDBTreeView1.DataSource.DataSet;

  // check if node is the bottom most node
  if (ADataSet.FieldByName('INDX').AsInteger >= DataModule1.getMaxChildrenCount(ADataSet.FieldByName('PARENT_TAG')
    .AsString, ADataSet.FieldByName('STATION_DEPT').AsString)) then
    Exit;

  ADataSet.DisableControls;
  try
    SavePlace := ADataSet.GetBookmark;

    myTag := ADataSet.FieldByName('MY_TAG').AsString;
    parentTag := ADataSet.FieldByName('PARENT_TAG').AsString;
    stationDept := ADataSet.FieldByName('STATION_DEPT').AsString;
    aDirection := 1;

    DataModule1.moveNode(myTag, parentTag, stationDept, aDirection);
    fixTreeNodeType('', ADataSet.FieldByName('STATION_DEPT').AsString);

    rg_NodeNameClick(self);

    ADataSet.GotoBookmark(SavePlace);

    if Assigned(dxDBTreeView1.Selected) then
      dxDBTreeView1.Selected.expand(True);
  finally
    ADataSet.FreeBookmark(SavePlace);
  end;
  ADataSet.EnableControls
end;

procedure TfrmGroupSetup.btn_searchClick(Sender: TObject);
var
  pWhereStmnt: String;
begin

  pWhereStmnt := ' Where 1>0 ';
  if cb_searchCombo.ItemIndex >= 0 then
    pWhereStmnt := pWhereStmnt + ' AND "BREADCRUMBS" LIKE ''%' +
      TSearchData(cb_searchCombo.Items.Objects[cb_searchCombo.ItemIndex]).MY_TAG + '%''';
  if cb_SearchStationDept.ItemIndex >= 0 then
    pWhereStmnt := pWhereStmnt + ' AND "STATION_DEPT" = ''' + cb_SearchStationDept.Items
      [cb_SearchStationDept.ItemIndex] + '''';

  DataModule1.searchTagTable(pWhereStmnt);
  rg_colexpandClick(nil);

end;

procedure TfrmGroupSetup.btn_tagDropDownClick(Sender: TObject);
var
  ADataSet: TDataSet;
begin

  ADataSet := dsTagDropDown.DataSet;
  if dsTagGroup.DataSet.FieldByName('OUTPUT').AsString = SUBMENU_NODE then
    Exit;

  ADataSet.Close;
  ADataSet.Open;
  ADataSet.First;
  cb_tagDropDown.Items.Clear;
  While not ADataSet.Eof do
  begin
    cb_tagDropDown.Items.Add(ADataSet.FieldByName('TXT').AsString);
    ADataSet.Next;
  end;

  cb_tagDropDown.Visible := True; { }
  cb_tagDropDown.BringToFront;
  cb_tagDropDown.DroppedDown := True;
  cb_tagDropDown.SetFocus;
  cb_tagDropDown.ItemIndex := 0;

end;

procedure TfrmGroupSetup.btn_UpNodeClick(Sender: TObject);
var
  ADataSet: TDataSet;
  SavePlace: TBookmark;
  myTag, parentTag, stationDept, BREADCRUMBS: string;
  aDirection: integer;
begin
  ADataSet := dxDBTreeView1.DataSource.DataSet;

  // check if node is the upper most node
  if (ADataSet.FieldByName('INDX').AsInteger <= 1) then
    Exit;

  ADataSet.DisableControls;
  try
    SavePlace := ADataSet.GetBookmark;
    myTag := ADataSet.FieldByName('MY_TAG').AsString;
    parentTag := ADataSet.FieldByName('PARENT_TAG').AsString;
    stationDept := ADataSet.FieldByName('STATION_DEPT').AsString;
    aDirection := -1;
    DataModule1.moveNode(myTag, parentTag, stationDept, aDirection);
    fixTreeNodeType('', ADataSet.FieldByName('STATION_DEPT').AsString);

    rg_NodeNameClick(self);

    ADataSet.GotoBookmark(SavePlace);
    if Assigned(dxDBTreeView1.Selected) then
      dxDBTreeView1.Selected.expand(True);
  finally
    ADataSet.FreeBookmark(SavePlace);
  end;

  ADataSet.EnableControls

end;

function CustomSortProc(Node1, Node2: TTreeNode; aDirection: Longint): integer; stdcall;
var
  val: integer;
  Data1, Data2: TTreeDataPtr;

begin
  Data1 := Node1.Data;
  Data2 := Node2.Data;

  // val := -1 ;
  if Assigned(Data1) and Assigned(Data2) then
  begin
    { if aDirection <> 0 then
      begin
      if Data1^.MORDER < Data2^.INDX then
      val := 1
      else if Data1^.INDX = Data2^.INDX then
      val := 0 ;
      end;
    }
    if aDirection = 0 then
      val := AnsiStrIComp(Pchar(Data1^.MORDER), Pchar(Data2^.MORDER))
    else
      val := -AnsiStrIComp(Pchar(Data1^.MORDER), Pchar(Data2^.MORDER));

  end;
  Exit(val);
end;

procedure TfrmGroupSetup.prepareTree(displayField: String = DEFAULT_TREE_TEXT);
var
  ADataSet: TDataSet;
begin
  ADataSet := dxDBTreeView1.DataSource.DataSet;
  if Assigned(ADataSet) then
  begin
    // aDataSet.Close;
    dxDBTreeView1.DataSource   := nil;
    dxDBTreeView1.DataSource   := dsTagGroup;
    dxDBTreeView1.KeyField     := 'MY_TAG';
    dxDBTreeView1.ParentField  := 'PARENT_TAG';
    dxDBTreeView1.displayField := displayField;
    dxDBTreeView1.ListField    := displayField;
    // aDataSet.Open;
    dxDBTreeView1.CustomSort(@CustomSortProc, 0);
    lbl_breadcrumb.Caption := getBreadCrumnbs(ADataSet.FieldByName('STATION_DEPT').AsString,
      ADataSet.FieldByName('BREADCRUMBS').AsString);
  end;
end;

procedure TfrmGroupSetup.rg_colexpandClick(Sender: TObject);
begin
  if rg_colexpand.ItemIndex > 0 then
    dxDBTreeView1.FullCollapse
  else
    dxDBTreeView1.FullExpand;

end;

procedure TfrmGroupSetup.rg_NodeNameClick(Sender: TObject);
begin
  case rg_NodeName.ItemIndex of
    0:
      prepareTree('TAG_CNT');
    1:
      prepareTree('DESC_CNT');
    2:
      prepareTree('BOTH_CNT');
  end;

  rg_colexpandClick(nil);
end;

procedure TfrmGroupSetup.seCardWidthClick(Sender: TObject);
begin
  btnCardSample.Width := Max(seCardWidth.Value, 160);
end;

procedure TfrmGroupSetup.populateCombos;
var
  searchData: TSearchData;
  Items: TcxImageComboBoxItems;
  Item:  TcxImageComboBoxItem;
Begin
  // search combo
  cb_searchCombo.Clear;
  With DataModule1 do
  Begin
    q_search.Close;
    q_search.Open;
    while not q_search.Eof do
    Begin
      // cb_searchCombo.Items.Add(q_search.FieldByName('TXT').AsString);
      searchData := TSearchData.Create;
      searchData.MY_TAG := q_search.FieldByName('MY_TAG').AsString;
      searchData.PARENT_TAG := q_search.FieldByName('PARENT_TAG').AsString;
      searchData.DESC := q_search.FieldByName('TXT').AsString;
      cb_searchCombo.AddItem(q_search.FieldByName('TXT').AsString, searchData);

      q_search.Next;
    End;
  End;

  // station dept combs
  icbStationDept.Clear;
  cb_SearchStationDept.Clear;
  Items := icbStationDept.Properties.Items;
  Items.BeginUpdate;
  try
    Items.Clear;

    With DataModule1 do
    Begin
      q_StationDept.Close;
      q_StationDept.Open;
      Item := Items.Add as TcxImageComboBoxItem; // Add a new item
      Item.Value := 'NONE'; // Set the item's value
      Item.Description := 'select a station dept...'; // Set the item's description
      Item.ImageIndex := -1; // Set the item's image index

      while not q_StationDept.Eof do // Some iteration
      begin
        Item := Items.Add as TcxImageComboBoxItem; // Add a new item
        Item.Value := q_StationDeptSTATION_DEPT.AsString; // Set the item's value
        Item.Description := Item.Value; // Set the item's description
        Item.ImageIndex := -1; // Set the item's image index

        // station search combo
        cb_SearchStationDept.Items.Add(q_StationDeptSTATION_DEPT.AsString);
        q_StationDept.Next;

      end;
    End;

  finally
    Items.EndUpdate;
  end;
  // show the tree corresponding t a specific station
  cb_SearchStationDept.ItemIndex := 0;
  btn_searchClick(nil);

End;

procedure TfrmGroupSetup.fixTreeNodeType(pParentTag, pStationDept: String);
begin
  try
    DataModule1.fixTagType(True,  ITEMS_NODE, pParentTag, pStationDept);
    DataModule1.fixTagType(False, SUBMENU_NODE, pParentTag, pStationDept);
  except
    ShowMessage('Error updating tree node OUTPUT type!');
  end;
end;

function TfrmGroupSetup.readTagRow(DataSet: TDataSet): TTreeDataPtr;
var
  currentRow: TTreeDataPtr;
Begin
  New(currentRow);
  with DataSet do
  Begin
    currentRow^.MY_TAG := FieldByName('MY_TAG').AsString;
    currentRow^.PARENT_TAG := FieldByName('PARENT_TAG').AsString;
    currentRow^.DESC := FieldByName('DESC').AsString;
    currentRow^.OUTPUT := FieldByName('OUTPUT').AsString;
    currentRow^.CARD_COLOR := FieldByName('CardColor').AsInteger;
    {
      // if node is not a top node and it has no color then use parent color
      try
      if (currentRow^.CARD_COLOR = 0) and (currentRow^.PARENT_TAG <> '') then
      currentRow^.CARD_COLOR := DataModule1.getParentColor(currentRow^.PARENT_TAG);
      except

      end;
    }

    currentRow^.INDX   := FieldByName('INDX').AsInteger;
    currentRow^.MORDER := FieldByName('MORDER').AsString;

    currentRow^.STATION_DEPT := FieldByName('STATION_DEPT').AsString;
    currentRow^.BREADCRUMBS  := FieldByName('BREADCRUMBS').AsString;

    currentRow^.SMLDESC    := FieldByName('smldesc').AsString;
    currentRow^.INACTIVE   := FieldByName('InActive').AsBoolean;
    currentRow^.LINEID     := FieldByName('LineID').AsString;
    currentRow^.CARD_FONT  := FieldByName('CardFont').AsString;
    currentRow^.CARD_WIDTH := FieldByName('CardWidth').AsInteger;
    currentRow^.CARD_DESC_LINES := FieldByName('CardDescLines').AsInteger;
    currentRow^.CARD_SORT_BY := FieldByName('CardSortBy').AsString;
    currentRow^.CARD_AS_GRID := FieldByName('CardAsGrid').AsBoolean;
    currentRow^.CARD_PICTURE := FieldByName('CardPicture').AsString;
    currentRow^.CARD_SHOW_PRICE := FieldByName('CardShowPrice').AsBoolean;
    currentRow^.CARD_SHOW_CODE := FieldByName('CardShowCode').AsBoolean;
    currentRow^.CARD_SHOW_INACTIVE := FieldByName('CardShowInactive').AsBoolean;
    currentRow^.CARD_PRESTRING := FieldByName('CardPreString').AsString;
    currentRow^.CARD_USE_MODIFIER := FieldByName('CardUseModifier').AsBoolean;
    currentRow^.CODE := FieldByName('code').AsString;
    currentRow^.Price := FieldByName('price').AsFloat;
  end;

  Result := currentRow;

End;

procedure TfrmGroupSetup.displayTagProperties(ADataSet: TDataSet);
var
  aI: integer;
  sCaption, sTagSort, sTable, sPicture,
  sCardPreCode, sCardFont : string;
  bCardCode, bCardPrice, bshowInactive, bUseModifier,
  bStationDeptChanged: Boolean;
  iDescLines, iCardWidth, iCardColor: integer;

begin

   if not assigned(ADataSet) then
       exit;

   lbl_breadcrumb.Caption := getBreadCrumnbs(
                            ADataSet.FieldByName('STATION_DEPT').AsString,
                            ADataSet.FieldByName('BREADCRUMBS').AsString);
  // Card settings...
  icbTable.EditValue := LowerCase(ADataSet.FieldByName('output').AsString);
  if icbTable.ItemIndex = -1 then // if empty or not in list default to ITEM
    icbTable.ItemIndex := 0;

  icbStationDept.EditValue := ADataSet.FieldByName('STATION_DEPT').AsString;
  if icbStationDept.ItemIndex = -1 then
    icbStationDept.ItemIndex := 0;

  lbledtCaption.Text   := ADataSet.FieldByName('Desc').AsString;
  edtTagList.Text      := ADataSet.FieldByName('PARENT_TAG').AsString;
  lbledtTagFilter.Text := ADataSet.FieldByName('My_TAG').AsString;
  //chkCardPrice.Checked := ADataSet.FieldByName('CardShowPrice').AsBoolean;
  //chkCardCode.Checked := ADataSet.FieldByName('CardShowCode').AsBoolean;
  //dtCardFont.Text := ADataSet.FieldByName('CardFont').AsString;
  // aI := getNodeColor(ADataSet.FieldByName('CardColor').AsInteger,
  //                    edtTagList.Text,
  //                    ADataSet.FieldByName('STATION_DEPT').AsString );
  aI := ADataSet.FieldByName('CardColor').AsInteger;
  if aI > 0 then
    cxColorComboBoxCard.ColorValue := IIF(aI > 0, aI, cxColorComboBoxCard.ColorValue)
  else
    cxColorComboBoxCard.ItemIndex := -1;

  // Misc Settings ...
 {
  chkshowInactive.Checked := ADataSet.FieldByName('CardShowInactive').AsBoolean;
  lbledtCardPreCode.Text  := ADataSet.FieldByName('CardPreString').AsString;
  chkUseModifier.Checked  := ADataSet.FieldByName('CardUseModifier').AsBoolean;

  // Group Settings ...
  icbSort.EditValue := LowerCase(ADataSet.FieldByName('CardSortBy').AsString);
  seDescLines.Value := ADataSet.FieldByName('CardDescLines').AsInteger;
  seCardWidth.Value := ADataSet.FieldByName('CardWidth').AsInteger;
}
  lbledtPicture.Text := ADataSet.FieldByName('CardPicture').AsString;

  lblParenValue.Caption := ADataSet.FieldByName('PARENT_TAG').AsString;
  lblMyTagValue.Caption := ADataSet.FieldByName('My_Tag').AsString;

end;

procedure TfrmGroupSetup.cxColorComboBoxCardExit(Sender: TObject);
var
  dataset : TDataSet ;
begin


  dataset := dsTagGroup.DataSet ;
  if Assigned(dataset) and
     cxColorComboBoxCard.ModifiedAfterEnter then
  begin
      if not (dataset.State in [dsInsert, dsEdit]) then
          dataset.Edit;

      dataset.FieldByName('CardColor').AsInteger := cxColorComboBoxCard.ColorValue ;
  end;

end;

procedure TfrmGroupSetup.cxColorComboBoxCardPropertiesChange(Sender: TObject);
begin
    if cxColorComboBoxCard.ColorValue > 0 then
    btnCardSample.Color := IIF(cxColorComboBoxCard.ColorValue > 0, cxColorComboBoxCard.ColorValue, btnCardSample.Color)
  else
    btnCardSample.Color := clBtnFace;
end;

procedure TfrmGroupSetup.DeleteSelectedNode1Click(Sender: TObject);
begin
  btnDeleteNodeClick(self);
end;

procedure TfrmGroupSetup.dsTagGroupDataChange(Sender: TObject; Field: TField);
begin
  displayTagProperties(dsTagGroup.DataSet);
//  rg_colexpandClick(nil);
    dxDBTreeView1.FullExpand;
//    dxDBTreeView1.RefreshItems;   //freez

end;

procedure TfrmGroupSetup.dxDBTreeView1Click(Sender: TObject);
var
  ADataSet: TDataSet;
  AColumn: TcxGridColumn;

  aI: integer;
  sCaption, sTagSort, sTable, sPicture,
  sCardPreCode, sCardFont : string;
  bCardCode, bCardPrice, bshowInactive, bUseModifier,
  bStationDeptChanged: Boolean;
  iDescLines, iCardWidth, iCardColor: integer;

begin
  ADataSet := dsTagGroup.DataSet;

  if ADataSet.State in [dsEdit, dsInsert] then
  begin
    dxDBTreeView1.EndDrag(false);
    // Show a confirmation dialog
    if MessageDlg('There are unsaved data, would you like to save it? ', mtInformation, mbOKCancel, 0) = mrOK then
    begin

      ADataSet.Post; //btnUpdateClick(self)
      exit;
    end
    else
      ADataSet.Cancel;

  end;

  AColumn := TableView1.GetColumnByFieldName('PARENT_TAG');
  edtTestfilter.Text := ADataSet.FieldByName('PARENT_TAG').AsString;
  // FilterByTag(edtTestfilter.Text,'PARENT_TAG');
  if Assigned(AColumn) then
    AColumn.Visible    := (edtTestfilter.Text > '');
  pnlTagfilter.Visible := (edtTestfilter.Text > '');
  TableView1.ApplyBestFit;

  displayTagProperties(ADataSet);


end;

procedure TfrmGroupSetup.dxDBTreeView1CustomDraw(Sender: TObject; TreeNode: TTreeNode; AFont: TFont;
  var AColor, ABkColor: TColor);
var
  Data: TTreeDataPtr;
  ParentData: TTreeDataPtr;
  R: TRect;
begin

  try
    if not Assigned(TreeNode)  then  exit;

    Data := TreeNode.Data;
    if Assigned(TreeNode.Parent) then
      ParentData := TreeNode.Parent.Data;
    if Assigned(Data) then
    begin
      AFont.Name := Data^.CARD_FONT;
      if Data^.CARD_COLOR > 0 then
        AColor := TColor(Data^.CARD_COLOR);

      {
        // if node has color then show it
        if Data^.CARD_COLOR  <> 0 then
        AColor := TColor(Data^.CARD_COLOR)
        else if assigned(ParentData) then
        AColor := TColor(ParentData^.CARD_COLOR);

      }

    end
  except
    AFont.Style := [];
  end;

end;



procedure TfrmGroupSetup.dxDBTreeView1DragDropTreeNode(Destination, Source: TTreeNode;
 var Accept: Boolean);
var
  srcKey, dstKey, dstBreadCrumbs, pStationDept: String;
  dstData: TTreeDataPtr;
begin

  try
    if not Assigned(Source) or
       not Assigned(Destination) then
         Exit;




    srcKey := TdxDBTreeNode(Source).KeyFieldValue;
    dstKey := TdxDBTreeNode(Destination).KeyFieldValue;
    dstData := TdxDBTreeNode(Destination).Data;

    if  not Assigned(dstData) then
         exit;


    dstBreadCrumbs := dstData^.BREADCRUMBS;
    pStationDept := dstData^.STATION_DEPT;

    {
      if assigned(dstData) then
      begin

      if dstData^.OUTPUT = ITEMS_NODE then
      begin
      ShowMessage('you can''t drag this sub-tree under an "ITEMS" node!');
      Accept := false ;
      exit;
      end;

      end;
    }
    DataModule1.updateNodeParent(srcKey, dstKey, dstBreadCrumbs, pStationDept);
    fixTreeNodeType('' { dstKey } , pStationDept);
    //Source.MoveTo(Destination, naAddChild);
    //RefreshItems

    Accept := True;
  except on e : Exception do
    begin
    Accept := False;
    ShowMessage('Can not drag the selected node...'+ e.Message);
    end;
  end;
    dxDBTreeView1.RefreshItems;

end;



procedure TfrmGroupSetup.dxDBTreeView1DragOver(Sender, Source: TObject; X, Y: integer; State: TDragState;
  var Accept: Boolean);
var
  Src, Dst: TTreeNode;
begin
  Src := dxDBTreeView1.Selected;
  Dst := dxDBTreeView1.GetNodeAt(X, Y);
  Accept := Assigned(Dst) and (Src <> Dst);
end;

procedure TfrmGroupSetup.dxDBTreeView1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState;
  X, Y: integer);
var
  Item: TTreeNode;
begin
  if Button = mbRight then
  begin
    try

      // get old selected node and de-select it
      Item := dxDBTreeView1.Selected;
      if Assigned(Item) then
        Item.Selected := False;

      // take the new node and select it
      Item := dxDBTreeView1.GetNodeAt(X, Y);
      if Assigned(Item) then
        Item.Selected := True;

      dxDBTreeView1Click(self);

      PopupMenu1.Popup(X, Panel5.top + dxDBTreeView1.top + Y + 30);
    Except
      ShowMessage('Error Selecting tree node...');

    end;
  end;

end;

procedure TfrmGroupSetup.dxDBTreeView1RefreshNode(Sender: TObject; DBTreeNode: TdxDBTreeNode);
var
  currentRow: TTreeDataPtr;
begin
  if Assigned(TdxDBTreeView(Sender).DataSource.DataSet) then
  begin
    currentRow := readTagRow(TdxDBTreeView(Sender).DataSource.DataSet);
    DBTreeNode.Data := currentRow;
  end;
end;

procedure TfrmGroupSetup.FilterByTag(sFilter, Sfield: string);
var
  ADataSet: TDataSet;
begin
  ADataSet := dxDBTreeView1.DataSource.DataSet;
  if Assigned(ADataSet) then
  begin
    ADataSet.DisableControls;
    try
      ADataSet.Filtered := False;
      if sFilter = '' then
        ADataSet.Filter := ''
      else
      begin
        ADataSet.Filter := Format('Contains(%s, %s)', [Sfield, QuotedStr(sFilter)]);
        // dmPrdData.tblTagGroup.Filter := Format('Contains(TagList, %s)', [QuotedStr(sFilter)]   );
        ADataSet.Filtered := True;
      end;
    finally
      ADataSet.EnableControls;
    end;
  end;

end;

procedure TfrmGroupSetup.q_TagGroupAfterScroll(DataSet: TDataSet);
var
  ADataSet: TDataSet;
begin
  ADataSet := dsTagGroup.DataSet;
  displayTagProperties(ADataSet);

  lbl_breadcrumb.Caption := getBreadCrumnbs(ADataSet.FieldByName('STATION_DEPT').AsString,
    ADataSet.FieldByName('BREADCRUMBS').AsString);
end;

procedure TfrmGroupSetup.DoFirst();
var
  Item: TcxImageComboBoxItem;
  i: integer;
begin
  // init a hash list that will contains all distinct station_dept
  StationDept_List := System.Generics.Collections.TDictionary<String, String>.Create;
  StationDept_List.Clear;

  if assigned(dsTagGroup.DataSet)  then
     fixTreeNodeType('', dsTagGroup.DataSet.FieldByName('STATION_DEPT').AsString);

  populateCombos;

  // after populating station combo for the first time
  // we populate StationDept_List
  // so when user adds new station we save it in StationDept_List
  for i := 0 to icbStationDept.Properties.Items.Count - 1 do
    StationDept_List.Add(icbStationDept.Properties.Items[i].Value, icbStationDept.Properties.Items[i].Value);

  prepareTree;
  dxDBTreeView1.FullExpand;
  dxDBTreeView1.DataSource.DataSet.First;
  displayTagProperties(dxDBTreeView1.DataSource.DataSet);

  Mutex := CreateMutex(nil, False, pWideChar(Application.Name));

  cb_tagDropDown.SendToBack;
  cb_tagDropDown.Visible := false ;
  lbledtTagFilter.BringToFront;
  lbledtTagFilter.Visible := true ;
end;



procedure TfrmGroupSetup.FormDestroy(Sender: TObject);
begin
  // destroy our mutex
  CloseHandle(Mutex);
end;

procedure TfrmGroupSetup.FormShow(Sender: TObject);
var
  AColumn: TcxGridColumn;
begin
  DoFirst ;
  colrfil();
  TableView1.ApplyBestFit();
  // DataModule1.POSConnection.IsConnected := True;
  // dxDBTreeView1.DataSource.DataSet.Active := True;

        // TableView1.DataController.CreateAllItems;

  {
    AColumn := TableView1.GetColumnByFieldName('TagList');
    if Assigned(AColumn) then
    begin
    AColumn.PropertiesClass := TTagTokenEditorProperties;
    AColumn.Caption := 'Parent Tag' ;
    //TTagTokenEditorProperties(AColumn.Properties).PopupDataSource
    end;


    AColumn := TableView1.GetColumnByFieldName('CardFilterTag');
    if Assigned(AColumn) then
    begin
    AColumn.PropertiesClass := TTagTokenEditorProperties;
    AColumn.Caption := 'My Tag' ;
    end;
  }

  {
    AColumn := TableView1.GetColumnByFieldName('LineID');
    if Assigned(AColumn) then
    AColumn.Visible := False;

    AColumn := TableView1.GetColumnByFieldName('SmlDesc');
    if Assigned(AColumn) then
    AColumn.Visible := False;

    AColumn := TableView1.GetColumnByFieldName('Price');
    if Assigned(AColumn) then
    AColumn.Visible := False;

    AColumn := TableView1.GetColumnByFieldName('code');
    if Assigned(AColumn) then
    AColumn.Visible := False;


    AColumn := TableView1.GetColumnByFieldName('cat');
    if Assigned(AColumn) then
    AColumn.Visible := False;

    if  edtTestfilter.Text >'' then
    begin
    pnlTagfilter.Visible := False ;
    AColumn := TableView1.GetColumnByFieldName('PARENT_TAG');
    FilterByTag(edtTestfilter.Text,'PARENT_TAG');
    if Assigned(AColumn) then
    AColumn.Visible :=  False ;
    end;
    TableView1.ApplyBestFit;


    dxDBTreeView1.KeyField := 'My_Tag' ;
    //dxDBTreeView1.ListField := 'My_Tag' ;
    dxDBTreeView1.DisplayField := 'Desc' ;
    dxDBTreeView1.ParentField := 'PARENT_TAG' ;

  }

  // TagTokenEditor1.Visible := False ; //not used yet
  cxColorComboBoxCard.Visible := not frmMain.chkLockLookAdnFeel.Checked;
  btnCardFont.Visible := not frmMain.chkLockLookAdnFeel.Checked;
  edtCardFont.Visible := not frmMain.chkLockLookAdnFeel.Checked;
end;

procedure TfrmGroupSetup.cb_SearchStationDeptClick(Sender: TObject);
begin
  if cb_SearchStationDept.ItemIndex >= 0 then
  begin
    btn_searchClick(self);
    rg_NodeNameClick(self);
  end;
end;

procedure TfrmGroupSetup.populateMyTagFromTagDropDown(pNewVal : String);
var
  sSelectedTag: String;
  tagGroupDataset: TDataSet;
begin
     if not pNewVal.IsEmpty then
     begin
        sSelectedTag := pNewVal;
        if LastDelimiter('(', sSelectedTag) > 0 then
          sSelectedTag := Trim(
                               copy(sSelectedTag,
                               0,
                               LastDelimiter('(', sSelectedTag) - 1
                               )
                          );

        tagGroupDataset := dsTagGroup.DataSet;
        if not (tagGroupDataset.State in [dsEdit, dsInsert]) then
           tagGroupDataset.Edit;
        tagGroupDataset.FieldByName('MY_TAG').AsString := sSelectedTag;
        lbledtTagFilter.Text :=  sSelectedTag;
     end;
     cb_tagDropDown.Visible := False;
     cb_tagDropDown.SendToBack;
     lbledtTagFilter.Visible := true ;
     lbledtTagFilter.SetFocus;
     lbledtTagFilter.BringToFront;
end;

procedure TfrmGroupSetup.cb_tagDropDownCloseUp(Sender: TObject);
var
  sSelectedTag: String;
  tagGroupDataset: TDataSet;
begin
   if cb_tagDropDown.ItemIndex >= 0 then
    begin
      sSelectedTag := cb_tagDropDown.Items[cb_tagDropDown.ItemIndex];
      populateMyTagFromTagDropDown(sSelectedTag);

    end;

end;

procedure TfrmGroupSetup.colrfil();
var
  AColor: TColor;
  intcolor: integer;
  i: integer;
begin
  TcxColorComboBoxProperties(cxColorComboBoxCard.Properties).NamingConvention := cxncNone;

  // AColor := RGB(255,255,1);  // Yellow

  for i := 1 to Length(clrList) do
  begin
    intcolor := clrList[i].hexA100.ToInteger();
    AColor := RGB(GetBValue(intcolor), GetGValue(intcolor), GetRValue(intcolor)); // red
    // TcxColorComboBoxProperties(cxColorComboBox01.Properties).CustomColors.AddColor(AColor, clrList[i].Name);
    TcxColorComboBoxProperties(cxColorComboBoxCard.Properties).CustomColors.AddColor(AColor, clrList[i].Name);
  end;

end;

function TfrmGroupSetup.GuidIdString: string;

var
  Year, Month, Day: Word;
  CurTimePart: integer;
begin

  DecodeDate(Now, Year, Month, Day);
  Result := '';
  // Result := Result + LocationCode; // 1 LocationCode
  // Result := Result + StationCode; // 1 StationCode
  Result := Result + IntToHex(Year - 2000, 1); // 1 Year - in year 2016 it will addtnl digit
  Result := Result + Chr(Month + 64); // 1 Month
  if Day < 10 then
    Result := Result + IntToStr(Day) // 1 Day in Month
  else
    Result := Result + Chr(Day + 55);
  CurTimePart := Round(Frac(Now) * 24 * 60 * 60);
  FGuidLastTimePart := Max(CurTimePart, FGuidLastTimePart + 1);
  Result := Result + IntToHex(FGuidLastTimePart, 5); // 5 Time
  Result := Result + IntToHex(RandomRange(1, 32), 2); // 2 digit Random
end;

procedure TfrmGroupSetup.icbSortPropertiesChange(Sender: TObject);
var
  dataset : TDataSet ;
begin
  dataset := dsTagGroup.DataSet ;
  if not Assigned(dataset) or
     not icbSort.ModifiedAfterEnter then
      exit;

  if not (dataset.State in [dsInsert, dsEdit]) then
      dataset.Edit;

  dataset.FieldByName('CardSortBy').AsString := VarToStr(icbSort.EditValue)  ;

end;

procedure TfrmGroupSetup.icbTablePropertiesChange(Sender: TObject);
var
  dataset : TDataSet ;
begin
  dataset := dsTagGroup.DataSet ;
  if not Assigned(dataset) or
     not icbTable.ModifiedAfterEnter then
      exit;



  if not (dataset.State in [dsInsert, dsEdit]) then
      dataset.Edit;

  dataset.FieldByName('output').AsString := VarToStr(icbTable.EditValue)  ;

end;

procedure TfrmGroupSetup.Makeselectednodearootnode1Click(Sender: TObject);
var
  ADataSet: TDataSet;
  SavePlace: TBookmark;
  myTag, parentTag, stationDept, BREADCRUMBS: string;

  SearchOptions: TLocateOptions;
begin
  SearchOptions := [];

   // check station combos
  if (icbStationDept.ItemIndex < 0) and (cb_SearchStationDept.ItemIndex < 0) then
  begin
    ShowMessage('Choose a station, please ...');
    Exit;
  end;


  ADataSet := dsTagGroup.DataSet;

  ADataSet.DisableControls;
  try
    // SavePlace := ADataSet.GetBookmark ;
    // check if node is already a root node
    if ( ( ADataSet.FieldByName('PARENT_TAG').isnull  ) or (ADataSet.FieldByName('PARENT_TAG').asString='') ) then
    begin
      ShowMessage('Current node is already a root node...');
      Exit;
    end;

    try
      // take ownership of our mutex
      WaitForSingleObject(Mutex, INFINITE);
      myTag       := ADataSet.FieldByName('MY_TAG').AsString;
      parentTag   := '';
      stationDept := ADataSet.FieldByName('STATION_DEPT').AsString;
      BREADCRUMBS := ADataSet.FieldByName('BREADCRUMBS').AsString;
      DataModule1.makeNodeARootNode(myTag,  parentTag, BREADCRUMBS,  stationDept);

    finally
      ReleaseMutex(Mutex);

    end;


    fixTreeNodeType('', ADataSet.FieldByName('STATION_DEPT').AsString);

    ADataSet.Close;
    ADataSet.Open;
    dxDBTreeView1.Refresh;
    dxDBTreeView1.FullExpand;
         rg_NodeNameClick(self);
    // ADataSet.GotoBookmark(SavePlace);
    ADataSet.Locate('MY_TAG', myTag, SearchOptions);

  finally
    // ADataSet.FreeBookmark(SavePlace);
  end;

  ADataSet.EnableControls
end;


procedure TfrmGroupSetup.mnuCloneTreeByStationDeptClick(Sender: TObject);
var
  dest_StationDept, src_StationDept, srcTreeNode, destTreeNode: string;
  Item: TcxImageComboBoxItem;
  srcDataSet, destDataSet: TDataSet;

begin

 try
  if not Assigned(frm_CloneTree) then
    frm_CloneTree := Tfrm_CloneTree.Create(self);
  frm_CloneTree.setStationDeptItems(cb_SearchStationDept.Items);

  if frm_CloneTree.ShowModal = mrOK then
  begin
    dest_StationDept := { UpperCase( } frm_CloneTree.edt_stationdept.Text { ) };
    src_StationDept := cb_SearchStationDept.Items[cb_SearchStationDept.ItemIndex];

    if dest_StationDept.IsEmpty or
       src_StationDept.IsEmpty  then
    begin
          ShowMessage('One of the source/destination station dept is empty...');
          exit;
    end;

    if dest_StationDept.Equals(src_StationDept)  then
    begin
          ShowMessage('Can not clone: source and destination stations are the same...');
          exit;
    end;


    srcDataSet := dsTagGroup.DataSet;
    destDataSet := frm_CloneTree.dsDestTreeTags.DataSet ;

    srcTreeNode := '' ;
    if frm_CloneTree.rg_TreeBranch.itemindex = 1 then
         srcTreeNode := srcDataSet.FieldByName('MY_TAG').AsString ;

    destTreeNode := '' ;
    if frm_CloneTree.rg_destConfig.itemindex = 1 then
         destTreeNode := destDataSet.FieldByName('MY_TAG').AsString ;


    // destination station dept already exists
    if DataModule1.checkStationExists(dest_StationDept) then
    begin

        // if no tag in selected source tree/brach exists in
        // destination dept then we can clone source tree/branch
        // if one ore more tags already exist in destination dept
        // then we can't clone
        if not DataModule1.checkStationTagsExist( src_StationDept,
                                           dest_StationDept,
                                           srcTreeNode) then
        begin
          ShowMessage('Can not clone Tree/SubTree, some tags already exist '+#13+
                      'in destination station dept...');
          exit;
        end;

        DataModule1.cloneStationDept(src_StationDept, dest_StationDept,
                                     srcTreeNode, destTreeNode );




    end
    // dest station doesn't exist
    else
    begin
        DataModule1.cloneStationDept(src_StationDept, dest_StationDept,
                                     srcTreeNode, destTreeNode );

        // add Item to Station Combo
        Item := icbStationDept.Properties.Items.Add;
        Item.Value := dest_StationDept;
        Item.Description := dest_StationDept;
        Item.ImageIndex := -1;


        if cb_SearchStationDept.Items.IndexOf(dest_StationDept)<0 then
        begin
            // add new Item to  StationDept_List
            StationDept_List.Add(dest_StationDept, dest_StationDept);
            // station search combo
            cb_SearchStationDept.Items.Add(dest_StationDept);
        end;
        // set currently added Station  in cb_SearchStationDept
        //cb_SearchStationDept.ItemIndex := cb_SearchStationDept.Items.Count - 1;

        // do search for the newly added station
        btn_searchClick(self);
    end;
    ShowMessage('Clonig finished successfully...');
  end;
 except
    ShowMessage('Clonig failed...');
 end;
end;

procedure TfrmGroupSetup.mnuCreateChildClick(Sender: TObject);
var
  ADataSet:  TDataSet;
  SavePlace: TBookmark;
  myTag, parentTag, stationDept, BREADCRUMBS: string;
  CardColor: Integer;


  SearchOptions: TLocateOptions;
begin
  SearchOptions := [];

  if MessageDlg('create a child under "' + dxDBTreeView1.Selected.Text + '" ?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
  then
    Exit;

  ADataSet := dsTagGroup.DataSet;

  {
    //  'Items' output can't have children
    if icbTable.ItemIndex <= 0  then
    begin
    ShowMessage('you can''t add a child to an "ITEMS" node ');
    exit ;
    end;
  }

  ADataSet.DisableControls;
  try
    // SavePlace := ADataSet.GetBookmark ;

    try
      // take ownership of our mutex
      WaitForSingleObject(Mutex, INFINITE);
      myTag := 'new child' + IntToStr(DataModule1.getAllTagsCount + 1);

    finally
      ReleaseMutex(Mutex);

    end;

    parentTag   := ADataSet.FieldByName('MY_TAG').AsString;
    stationDept := ADataSet.FieldByName('STATION_DEPT').AsString;
    BREADCRUMBS := ADataSet.FieldByName('BREADCRUMBS').AsString;
    CardColor   := ADataSet.FieldByName('CardColor').AsInteger;
    DataModule1.createTagNode(myTag, parentTag, stationDept, BREADCRUMBS + '|' + myTag, CardColor);

    fixTreeNodeType('', stationDept);

    rg_NodeNameClick(self);

    // ADataSet.GotoBookmark(SavePlace);
    ADataSet.Locate('MY_TAG', myTag, SearchOptions);

  finally
    // ADataSet.FreeBookmark(SavePlace);
  end;

  ADataSet.EnableControls

end;

procedure TfrmGroupSetup.mnuCreateParentClick(Sender: TObject);
var
  ADataSet: TDataSet;
  SavePlace: TBookmark;
  myTag, parentTag, stationDept, BREADCRUMBS: string;
  CardColor: Integer;

  SearchOptions: TLocateOptions;
begin
  SearchOptions := [];

  // check station combos
  if (icbStationDept.ItemIndex < 0) and (cb_SearchStationDept.ItemIndex < 0) then
  begin
    ShowMessage('Choose a station, please ...');
    Exit;
  end;

  ADataSet := dxDBTreeView1.DataSource.DataSet;
  ADataSet.DisableControls;
  try
    // SavePlace := ADataSet.GetBookmark ;

    try
      // take ownership of our mutex
      WaitForSingleObject(Mutex, INFINITE);
      myTag := 'new parent' + IntToStr(DataModule1.getAllTagsCount + 1);

    finally
      ReleaseMutex(Mutex);

    end;

    parentTag := '';

    // choose station from station combo
    if cb_SearchStationDept.ItemIndex < 0 then
      stationDept := VarToStr(icbStationDept.EditValue)
    else
      stationDept := cb_SearchStationDept.Items[cb_SearchStationDept.ItemIndex];

    BREADCRUMBS := stationDept + '|' + myTag;
    CardColor := 0 ;   //will nned to set a defaul
    DataModule1.createTagNode(myTag, parentTag, stationDept, BREADCRUMBS,CardColor);

    fixTreeNodeType('', stationDept);

    ADataSet.Close;
    ADataSet.Open;
    dxDBTreeView1.Refresh;
    dxDBTreeView1.FullExpand;

    // ADataSet.GotoBookmark(SavePlace);
    ADataSet.Locate('MY_TAG', myTag, SearchOptions);

  finally
    // ADataSet.FreeBookmark(SavePlace);
  end;

  ADataSet.EnableControls
end;

end.


{   if cb_tagDropDown.text <> '' then
   begin
      populateMyTagFromTagDropDown(cb_tagDropDown.text);

      cb_tagDropDown.SendToBack;
      cb_tagDropDown.Visible := false ;
      lbledtTagFilter.BringToFront;
      lbledtTagFilter.Visible := true ;
   end;
}
