program XbrowseGroupSetup;

uses
  Vcl.Forms,
  uGroupSetupMain in 'uGroupSetupMain.pas' {frmMain},
  uformGroupSetup in 'uformGroupSetup.pas' {frmGroupSetup},
  dmData in 'dmData.pas' {DataModule1: TDataModule},
  Utils in 'Utils.pas',
  uFormCloneTree in 'uFormCloneTree.pas' {frm_CloneTree},
  uFormAddStationDept in 'uFormAddStationDept.pas' {frm_AddStatioDept};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TDataModule1, DataModule1);
  Application.CreateForm(TfrmGroupSetup, frmGroupSetup);
  Application.CreateForm(Tfrm_CloneTree, frm_CloneTree);
  Application.CreateForm(Tfrm_AddStatioDept, frm_AddStatioDept);
  Application.Run;

end.
