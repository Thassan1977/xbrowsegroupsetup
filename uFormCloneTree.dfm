object frm_CloneTree: Tfrm_CloneTree
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Clone whole tree or a tree branch'
  ClientHeight = 390
  ClientWidth = 469
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 24
    Width = 95
    Height = 13
    Caption = 'Destination Station:'
  end
  object cb_stationDept: TComboBox
    Left = 120
    Top = 21
    Width = 222
    Height = 21
    TabOrder = 3
    OnClick = cb_stationDeptClick
  end
  object edt_stationdept: TEdit
    Left = 120
    Top = 21
    Width = 222
    Height = 21
    TabOrder = 0
    OnExit = edt_stationdeptExit
  end
  object Panel1: TPanel
    Left = 0
    Top = 350
    Width = 469
    Height = 40
    Align = alBottom
    TabOrder = 1
    object btn_OK: TBitBtn
      Left = 24
      Top = 8
      Width = 81
      Height = 25
      Caption = 'OK'
      Default = True
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      ModalResult = 1
      NumGlyphs = 2
      TabOrder = 0
      OnClick = btn_OKClick
    end
    object btn_Cancel: TBitBtn
      Left = 304
      Top = 8
      Width = 83
      Height = 25
      Kind = bkCancel
      NumGlyphs = 2
      TabOrder = 1
    end
  end
  object rg_TreeBranch: TRadioGroup
    Left = 24
    Top = 50
    Width = 346
    Height = 70
    Caption = 'Source Configuration'
    ItemIndex = 0
    Items.Strings = (
      'Clone whole source tree to destination dept'
      'Clone selected source branch to destination dept')
    TabOrder = 2
  end
  object btn_selectDept: TBitBtn
    Left = 343
    Top = 21
    Width = 27
    Height = 21
    Caption = '::'
    TabOrder = 4
    OnClick = btn_selectDeptClick
  end
  object rg_destConfig: TRadioGroup
    Left = 24
    Top = 126
    Width = 344
    Height = 64
    Caption = 'Destination Configuration'
    ItemIndex = 0
    Items.Strings = (
      'Clone to destination as root node'
      'Clone to distination under specific tree branch')
    TabOrder = 5
    OnClick = rg_destConfigClick
  end
  object pnl_DestBranch: TPanel
    Left = 24
    Top = 196
    Width = 344
    Height = 117
    BevelOuter = bvNone
    TabOrder = 6
    object Label2: TLabel
      Left = 6
      Top = 5
      Width = 232
      Height = 13
      Caption = 'choose destination branch under which to clone:'
    end
    object dxDBTreeView1: TdxDBTreeView
      Left = 6
      Top = 24
      Width = 322
      Height = 87
      ShowNodeHint = True
      RowSelect = True
      DataSource = dsDestTreeTags
      DisplayField = 'MY_TAG'
      KeyField = 'MY_TAG'
      ListField = 'MY_TAG'
      ParentField = 'PARENT_TAG'
      RootValue = Null
      SeparatedSt = ' - '
      RaiseOnError = True
      Indent = 19
      ParentColor = False
      Options = [trDBCanDelete, trDBConfirmDelete, trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
      SelectedIndex = -1
      TabOrder = 0
    end
  end
  object dsDestTreeTags: TDataSource
    DataSet = DataModule1.q_CloneBrachList
    Left = 328
    Top = 56
  end
end
