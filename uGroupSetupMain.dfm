object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Xbrowse Button Designer'
  ClientHeight = 523
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlAction: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 477
    Width = 629
    Height = 43
    Align = alBottom
    Color = clWindow
    ParentBackground = False
    TabOrder = 0
    object btnExit: TButton
      AlignWithMargins = True
      Left = 550
      Top = 4
      Width = 75
      Height = 35
      Align = alRight
      Caption = 'Exit'
      TabOrder = 0
      OnClick = btnExitClick
    end
    object Button1: TButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 75
      Height = 35
      Align = alLeft
      Caption = 'Design Tree'
      TabOrder = 1
      OnClick = Button1Click
    end
    object btnSetup: TButton
      AlignWithMargins = True
      Left = 469
      Top = 4
      Width = 75
      Height = 35
      Align = alRight
      Caption = 'Setup'
      TabOrder = 2
      OnClick = btnSetupClick
    end
  end
  object flwpnlSetUp1: TFlowPanel
    Left = 434
    Top = 304
    Width = 193
    Height = 65
    TabOrder = 1
    object lbledtPrimaryAlias: TLabeledEdit
      Left = 1
      Top = 1
      Width = 185
      Height = 21
      EditLabel.Width = 115
      EditLabel.Height = 13
      EditLabel.Caption = 'Primary Alias  - XBrowse'
      TabOrder = 0
    end
    object chkLockLookAdnFeel: TCheckBox
      Left = 1
      Top = 35
      Width = 120
      Height = 17
      Align = alTop
      Caption = 'Lock Look & Feel'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
  end
  object cxPropertiesStore1: TcxPropertiesStore
    Active = False
    Components = <
      item
        Component = chkLockLookAdnFeel
        Properties.Strings = (
          'Checked')
      end
      item
        Component = lbledtPrimaryAlias
        Properties.Strings = (
          'Text')
      end>
    StorageName = 'setupBrowseGroup'
    Left = 511
    Top = 103
  end
end
