unit dmData;

interface

uses
  System.SysUtils, System.StrUtils, System.Classes, Data.DB, adsdata,
  adsfunc, adstable, adsset, adscnnct, system.variants,
  System.Types, Vcl.Dialogs, Winapi.Windows, Vcl.Forms, Utils;

Const
  ITEMS_NODE = 'prdmstr';
  SUBMENU_NODE = 'taggroup';

type
  TDataModule1 = class(TDataModule)
    tblTagGroup: TAdsTable;
    POSConnection: TAdsConnection;
    AdsSettings1: TAdsSettings;
    q_search: TAdsQuery;
    q_searchTXT: TAdsStringField;
    q_tmp: TAdsQuery;
    tblTagGroupMY_TAG: TAdsStringField;
    tblTagGroupPARENT_TAG: TAdsStringField;
    tblTagGroupdesc: TAdsStringField;
    tblTagGroupOUTPUT: TAdsStringField;
    tblTagGroupCardColor: TIntegerField;
    tblTagGroupSTATION_DEPT: TAdsStringField;
    tblTagGroupBREADCRUMBS: TAdsStringField;
    tblTagGroupsmldesc: TAdsStringField;
    tblTagGroupInActive: TBooleanField;
    tblTagGroupLineID: TAdsStringField;
    tblTagGroupCardFont: TAdsStringField;
    tblTagGroupCardWidth: TIntegerField;
    tblTagGroupCardDescLines: TIntegerField;
    tblTagGroupCardSortBy: TAdsStringField;
    tblTagGroupCardAsGrid: TBooleanField;
    tblTagGroupCardPicture: TAdsStringField;
    tblTagGroupCardShowPrice: TBooleanField;
    tblTagGroupCardShowCode: TBooleanField;
    tblTagGroupCardShowInactive: TBooleanField;
    tblTagGroupCardPreString: TAdsStringField;
    tblTagGroupCardUseModifier: TBooleanField;
    tblTagGroupcode: TAdsStringField;
    tblTagGroupprice: TAdsBCDField;
    tblTagGrouptagdesc: TStringField;
    q_searchMY_TAG: TAdsStringField;
    q_searchPARENT_TAG: TAdsStringField;
    q_TagList: TAdsQuery;
    q_TagListLineID: TAdsStringField;
    q_TagListTableName: TAdsStringField;
    q_TagListTagName: TAdsStringField;
    q_TagListCnt: TIntegerField;
    q_TagListUpdatedOn: TDateTimeField;
    q_TagGroup: TAdsQuery;
    q_TagGroupMY_TAG: TAdsStringField;
    q_TagGroupPARENT_TAG: TAdsStringField;
    q_TagGroupdesc: TAdsStringField;
    q_TagGroupOUTPUT: TAdsStringField;
    q_TagGroupCardColor: TIntegerField;
    q_TagGroupSTATION_DEPT: TAdsStringField;
    q_TagGroupBREADCRUMBS: TAdsStringField;
    q_TagGroupsmldesc: TAdsStringField;
    q_TagGroupInActive: TBooleanField;
    q_TagGroupLineID: TAdsStringField;
    q_TagGroupCardFont: TAdsStringField;
    q_TagGroupCardWidth: TIntegerField;
    q_TagGroupCardDescLines: TIntegerField;
    q_TagGroupCardSortBy: TAdsStringField;
    q_TagGroupCardAsGrid: TBooleanField;
    q_TagGroupCardPicture: TAdsStringField;
    q_TagGroupCardShowPrice: TBooleanField;
    q_TagGroupCardShowCode: TBooleanField;
    q_TagGroupCardShowInactive: TBooleanField;
    q_TagGroupCardPreString: TAdsStringField;
    q_TagGroupCardUseModifier: TBooleanField;
    q_TagGroupcode: TAdsStringField;
    q_TagGroupprice: TAdsBCDField;
    q_TagGroupTAG_CNT: TStringField;
    q_TagGroupDESC_CNT: TStringField;
    q_TagGroupBOTH_CNT: TStringField;
    q_StationDept: TAdsQuery;
    q_StationDeptSTATION_DEPT: TAdsStringField;
    qInsetNode: TAdsQuery;
    q_TagGroupINDX: TIntegerField;
    q_TagGroupMORDER: TStringField;
    q_getNodes: TAdsQuery;
    q_tmp2: TAdsQuery;
    q_tagDropDown: TAdsQuery;
    q_tagDropDownTagName: TAdsStringField;
    q_tagDropDownCnt: TIntegerField;
    q_tagDropDownTXT: TStringField;
    qCloneStationDept: TAdsQuery;
    AdsStringField1: TAdsStringField;
    AdsStringField2: TAdsStringField;
    AdsStringField3: TAdsStringField;
    q_CloneBrachList: TAdsQuery;
    AdsStringField4: TAdsStringField;
    AdsStringField5: TAdsStringField;
    AdsStringField6: TAdsStringField;
    AdsStringField7: TAdsStringField;
    IntegerField1: TIntegerField;
    AdsStringField8: TAdsStringField;
    AdsStringField9: TAdsStringField;
    AdsStringField10: TAdsStringField;
    BooleanField1: TBooleanField;
    AdsStringField11: TAdsStringField;
    AdsStringField12: TAdsStringField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    AdsStringField13: TAdsStringField;
    BooleanField2: TBooleanField;
    AdsStringField14: TAdsStringField;
    BooleanField3: TBooleanField;
    BooleanField4: TBooleanField;
    BooleanField5: TBooleanField;
    AdsStringField15: TAdsStringField;
    BooleanField6: TBooleanField;
    AdsStringField16: TAdsStringField;
    AdsBCDField1: TAdsBCDField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    IntegerField4: TIntegerField;
    StringField4: TStringField;

    procedure q_TagGroupCalcFields(DataSet: TDataSet);

    procedure q_tagDropDownCalcFields(DataSet: TDataSet);

    procedure q_TagGroupBeforePost(DataSet: TDataSet);
    procedure q_TagGroupAfterPost(DataSet: TDataSet);

    { procedure q_TagGroupUpdateRecord(DataSet: TDataSet; UpdateKind: TUpdateKind;
      var UpdateAction: TUpdateAction);
    }
  private
    currentTagWhereStmnt : string ;
    old_station_dept, old_mytag : string;
    function breadCrumbsGetLastItem(Delimiter: Char; Str: string): string;
    procedure OpenData(Data: TAdsTable);
    function UserName: string;





    { Private declarations }
  public
    procedure moveNode(nodeKey, nodeParent, nodeStation: string; aDirection: integer);
    procedure updateNodeBreadccrumbs(old_nodeKey, new_nodeKey, stationDept: string);
    procedure updateNodeParent(nodeKey, nodeParent, parentBreadCrumbs, pStationDept: string);
    procedure updateNodeIndx(nodeKey, pStationDept: string; pIndex: integer);
    procedure createTagNode(tagKey, parentKey, stationDept, breadCrumbs: string; CardColor: Integer);
    function checkTagExists(pMY_TAG, pSTATION_DEPT: string): Boolean;
    function getTagIndex(pMY_TAG, pPARENT_TAG, pSTATION_DEPT: string): integer;
    function getMaxChildrenCount(pParentTag, pStationDept: string): integer;
    function getChildrenCount(pParentTag, pStationDept: string): integer;
    function getAllTagsCount: integer;
    function getParentColor(nodeParentKey, pStationDept: string): integer;
    procedure searchTagTable(sFilter: string);
    Function getTagCount(pTagName, pTableName: string): integer;
    procedure fixTagType(pIsITEMSTag: Boolean; pTagType,
                         pParentTag, pStationDept: String);
    procedure deleteNode(nodeKey, nodeParent, pStationDept: string);
    function checkStationExists(pStationDept: string): Boolean;
    procedure changeBranchStationDept(nodeKey, pOldStationDept, pNewStationDept: string);
    procedure cloneStationDept(pOldStationDept, pNewStationDept,
                               pSrcSubTreeNode , pDestSubTreeNode: string);
    function LockAllExists: Boolean;

    procedure makeNodeARootNode(myTag, parentTag, nodeBreadCrumbs,
                                nodeStationDept: string);
    function checkStationTagsExist(pSrcStationDept, pDestSrcStationDept,
                                   pSubTreeNode: string): Boolean;

     procedure OpenDestStationQuery( pDestStationDept : string);
     procedure POSConnectionBeforeConnect();
     procedure LoadData;

    { Public declarations }
  end;

var
  DataModule1: TDataModule1;
  FRestartAllTime: TDateTime;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uGroupSetupMain, uformGroupSetup, cxDateUtils;

{$R *.dfm}

procedure TDataModule1.POSConnectionBeforeConnect();
var exepath: string;
begin
  if frmMain.lbledtPrimaryAlias.Text<>'' then
  begin
    POSConnection.ConnectPath := '';
    POSConnection.AliasName := frmMain.lbledtPrimaryAlias.Text;
  end
  //else if frmMain.lbledtPrimaryAlias.Text <> '' then             //need some way to didect if connection or alies
  //  POSConnection.ConnectPath := frmMain.lbledtPrimaryAlias.Text
  else
     begin
        exepath := ExtractFilePath(ParamStr(0)) ;
        exepath := exepath + 'tiswin3dd.add' ;
        if FileExists(exepath) then
           POSConnection.ConnectPath := exepath
         else
          ShowMessage(exepath+ ' Not found');
     end;
    POSConnection.Username := 'DeliveryControl';
    POSConnection.Password := UserName;
end;

procedure TDataModule1.q_tagDropDownCalcFields(DataSet: TDataSet);
begin
  q_tagDropDownTXT.asString := q_tagDropDownTagName.asString + '( ' + q_tagDropDownCnt.asString + ' )';
end;



procedure log(s : string ) ;
var
  myFile : TextFile;
begin
  AssignFile(myFile, 'Test.txt');
  if not fileExists('Test.txt') then
       ReWrite(myFile);
  append(myFile);

  Write(myFile, s);
  WriteLn(myFile, '---------------------------------------');

  CloseFile(myFile);

end;




procedure TDataModule1.q_TagGroupAfterPost(DataSet: TDataSet);
begin
   if (old_station_dept <> '') and  (old_mytag <> '') then
   begin

      if (not q_TagGroupSTATION_DEPT.IsNull) and
         (not q_TagGroupSTATION_DEPT.AsString.Equals(old_station_dept)) then
          changeBranchStationDept(q_TagGroupMY_TAG.AsString, // MY_TAG
           old_station_dept, // old_statioDept
           q_TagGroupSTATION_DEPT.AsString // new_StationDept
          );

      if (not q_TagGroupMY_TAG.IsNull) and
         (not q_TagGroupMY_TAG.AsString.Equals(old_mytag)) then
          updateNodeBreadccrumbs(old_myTag,
                            q_TagGroupMY_TAG.AsString,
                            q_TagGroupSTATION_DEPT.AsString);


   end;

   if Assigned(frmGroupSetup.dxDBTreeView1) and
      Assigned(frmGroupSetup.dxDBTreeView1.DataSource) then
      begin
        //frmGroupSetup.dxDBTreeView1.items.beginupdate;
        try
         frmGroupSetup.fixTreeNodeType('', q_TagGroupSTATION_DEPT.AsString);  //q_TagGroupMY_TAG.AsString
         frmGroupSetup.dxDBTreeView1.RefreshItems ;
        except
        //finally
         //frmGroupSetup.dxDBTreeView1.items.endupdate;
        end;
      end;
end;

procedure TDataModule1.q_TagGroupBeforePost(DataSet: TDataSet);
var
  tmpStr: String;

  ADataSet: TDataSet;
  sCaption, sTagSort, sTable, sPicture, sTagFilter,
  sCardPreCode, sCardFont, sTagList, sStationDept,
  sTmp, parent_breadCrumbs: string;
  bCardCode, bCardPrice, bshowInactive, bUseModifier,
  bStationDeptChanged: Boolean;
  iDescLines, iCardWidth, iCardColor: integer;
  breadCrumbsList: TStringList;
begin
  with frmGroupSetup do
   begin
     { if q_TagGroupMY_TAG.AsString = '' then
      begin
        ShowMessage('My-Tag Cant be empty');
        abort;
      end;
     }
      try


        // breadcrumbs is not changed yet
        // we can take old MY_TAG and old STATION_DEPT from it ;
        old_myTag        := '';
        old_station_dept := '' ;
        sTmp             := q_TagGroupBREADCRUMBS.AsString ;
        //get old station dept
        if POS('|' , sTmp)>0 then
           old_station_dept := copy(sTmp, 0 , POS('|' , sTmp)-1);
        // get old my_tag
        if LastDelimiter('|', sTmp) > 0 then
        begin
           old_myTag := copy(sTmp, LastDelimiter('|' , sTmp)+1, Length(sTmp));
           parent_breadCrumbs := copy(sTmp, 0, LastDelimiter('|' , sTmp)-1 ) ;
        end;


        if edtTestfilter.Text > '' then
          edtTagList.Text := edtTestfilter.Text;

        sCaption := lbledtCaption.Text;
        sTagSort := VarToStr(icbSort.EditValue);

        if icbStationDept.ItemIndex <= 0 then
          sStationDept := ''
        else
          sStationDept := VarToStr(icbStationDept.EditValue);

        sTable := VarToStr(icbTable.EditValue);

        sPicture := lbledtPicture.Text;
        sTagFilter := lbledtTagFilter.Text;

        sCardPreCode  := lbledtCardPreCode.Text;
        sTagList      := edtTagList.Text;
        bCardCode     := chkCardCode.Checked;
        bCardPrice    := chkCardPrice.Checked;
        bshowInactive := chkshowInactive.Checked;
        bUseModifier := chkUseModifier.Checked;
        sCardFont    := edtCardFont.Text;
        iCardColor   := cxColorComboBoxCard.ColorValue;
        iDescLines   := seDescLines.Value;
        iCardWidth   := seCardWidth.Value;

        bStationDeptChanged := (not old_station_dept.Equals(sStationDept));

        if (bStationDeptChanged) and (not String.IsNullOrEmpty(sTagList)) then
        begin
          ShowMessage('Can''t change Station-Dept for child node');
          abort;
        end;

        // check if tag is already used in database within the same station_dept
        if (not old_myTag.Equals(sTagFilter)) and (DataModule1.checkTagExists(sTagFilter, sStationDept)) then
        begin
          q_TagGroupMy_TAG.AsString := old_myTag;
          lbledtTagFilter.Text := old_myTag;
          lbledtTagFilter.SetFocus;
          ShowMessage('Item Tag already exist in tree, please choose another Item Tag.');
          abort;
        end;

        //ADataSet.FieldByName('BREADCRUMBS').AsString := parent_breadCrumbs + sTagFilter;

        q_TagGroupDesc.AsString       := sCaption;
        q_TagGroupCardWidth.AsInteger := iCardWidth;
        q_TagGroupCardSortBy.AsString := sTagSort;
        q_TagGroupCardDescLines.AsInteger := iDescLines;
        q_TagGroupOUTPUT.AsString         := sTable;
        q_TagGroupPARENT_TAG.AsString := sTagList;
        q_TagGroupSTATION_DEPT.AsString := sStationDept;
        q_TagGroupCardFont.AsString := sCardFont;
        q_TagGroupCardShowCode.AsBoolean := bCardCode;
        q_TagGroupCardShowPrice.AsBoolean := bCardPrice;
        q_TagGroupCardColor.AsInteger := iCardColor;
        q_TagGroupCardPicture.AsString := sPicture;
        q_TagGroupMy_TAG.AsString := sTagFilter;
        q_TagGroupCardShowInactive.AsBoolean := bshowInactive;
        q_TagGroupCardPreString.AsString := sCardPreCode;
        q_TagGroupCardUseModifier.AsBoolean := bUseModifier;
        if q_TagGroupcode.AsString.IsEmpty then
          q_TagGroupcode.AsString := GuidIdString;

        // update breadcrumbs in case station dept is changed
        // sTmp := ADataSet.FieldByName('BREADCRUMBS').AsString;
        // sTmp := sStationDept + Copy(sTmp, pos('|', sTmp), Length(sTmp));

        // ADataSet.FieldByName('BREADCRUMBS').AsString := sStationDept + '|' + lbl_breadcrumb.Caption;
        //q_TagGroupBREADCRUMBS.AsString := parent_breadCrumbs + '|' + sTagFilter;

       if q_TagGroupMY_TAG.AsString.IsEmpty then
       begin
         ShowMessage('can not save data '+#13+
                     'My Tag is empty...');
         q_TagGroup.Cancel;
         exit;
       end;

       if q_TagGroupSTATION_DEPT.AsString.IsEmpty then
       begin
         ShowMessage('can not save data '+#13+
                     'Station deptartment is empty...');
         q_TagGroup.Cancel;
         exit;
       end;

       // checks if MY_TAG already exist in dept
       if not(old_mytag.Equals(q_TagGroupMY_TAG.AsString)) and
         checkTagExists(q_TagGroupMY_TAG.AsString,
                         q_TagGroupSTATION_DEPT.AsString) then
        begin
         ShowMessage('can not save data '+#13+
                     'My Tag already exist...');
         q_TagGroup.Cancel;
         exit;
        end;

        {
      tmpStr := StringReplace(q_TagGroupBREADCRUMBS.asString, breadCrumbsGetLastItem('|', q_TagGroupBREADCRUMBS.asString),
        q_TagGroupMY_TAG.asString, [rfReplaceAll]);

      q_TagGroupBREADCRUMBS.asString := q_TagGroupSTATION_DEPT.asString + Copy(tmpStr, Pos('|', tmpStr), Length(tmpStr));
      }
      except
         abort;
      end;
  end;

end;

procedure TDataModule1.q_TagGroupCalcFields(DataSet: TDataSet);
var
  pCount: string;
  tmpStr: string;
  strArray: TStringDynArray;
  charArray: Array [0 .. 0] of Char;
  strBreadcrumbs, sParent, sStation: string;
  I: integer;
begin
  try
    q_TagGroupTAG_CNT.asString := q_TagGroupMY_TAG.asString;

    q_TagGroupDESC_CNT.asString := q_TagGroupdesc.asString;

    q_TagGroupBOTH_CNT.asString := q_TagGroupMY_TAG.asString + '/' + q_TagGroupdesc.asString;

    if q_TagGroupOUTPUT.asString = ITEMS_NODE then
    begin
      pCount := '(' + intToStr(getTagCount(q_TagGroupMY_TAG.asString, q_TagGroupOUTPUT.asString)) + ')';

      q_TagGroupTAG_CNT.asString := q_TagGroupTAG_CNT.asString + pCount;

      q_TagGroupDESC_CNT.asString := q_TagGroupDESC_CNT.asString + pCount;

      q_TagGroupBOTH_CNT.asString := q_TagGroupBOTH_CNT.asString + pCount;

    end;
    strBreadcrumbs := q_TagGroupBREADCRUMBS.asString;
    sParent := '';
    sStation := q_TagGroupSTATION_DEPT.asString;
    tmpStr := '';
    charArray[0] := '|';
    if strBreadcrumbs <> '' then
    begin
      strArray := SplitString(strBreadcrumbs, '|');
      for I := 1 to Length(strArray) - 1 do
      begin
        tmpStr := tmpStr + intToStr(getTagIndex(strArray[I], sParent, sStation));
        sParent := strArray[I];
      end;
    end;

    q_TagGroupMORDER.asString := tmpStr;

    // q_TagGroupTAG_CNT.AsString := q_TagGroupTAG_CNT.asstring + '/ ' + tmpStr ;
  except

  end;
end;

procedure TDataModule1.createTagNode(tagKey, parentKey, stationDept, breadCrumbs: string; CardColor: Integer);
var
  aParam: TParam;
begin
  qInsetNode.Close;

  // desc
  qInsetNode.Params.ParamByName('p1').asString := tagKey;
  // MY_TAG
  qInsetNode.Params.ParamByName('p2').asString := tagKey;
  // PARENT_TAG
  qInsetNode.Params.ParamByName('p3').asString := parentKey;
  // STATION_DEPT
  qInsetNode.Params.ParamByName('p4').asString := stationDept;
  // BREADCRUMBS
  qInsetNode.Params.ParamByName('p5').asString := breadCrumbs;
  // OUTPUT
  qInsetNode.Params.ParamByName('p6').asString := ITEMS_NODE;
  // OINDX
  qInsetNode.Params.ParamByName('p7').AsInteger := getMaxChildrenCount(parentKey, stationDept) + 1;

  qInsetNode.Params.ParamByName('p8').AsInteger := CardColor;

  qInsetNode.ExecSQL;
end;



Procedure TDataModule1.OpenData(Data: TAdsTable);
begin
  If not Data.Active then
  begin
    Data.Open;
  end;
end;

function TDataModule1.LockAllExists: Boolean;
var
  APath: string;
  APos: integer;
  fad: TWin32FileAttributeData;
begin
  APath := POSConnection.GetConnectionPath;
  APos := Pos(':6262', APath);
  if APos > 0 then
    APath := Copy(APath, 1, APos - 1) + Copy(APath, APos + 5, Length(APath));
  Result := FileExists(APath + '\Lock.all');
  if Result then
    MessageDlg('Program Is Being Updated, Please Try Again Later...' + #13#10 + ' (Lock / Close .All)', mtError,
      [mbOK], 0)
  else
  begin
    if FileExists(APath + '\Restart.all') then
    begin
      GetFileAttributesEx(PChar(APath + '\Restart.all'), GetFileExInfoStandard, @fad);
      FRestartAllTime := FileTimeToLocalDateTime(fad.ftLastWriteTime);
    end
    else
      FRestartAllTime := NullDate;
  end;
end;

procedure TDataModule1.moveNode(nodeKey, nodeParent, nodeStation: string; aDirection: integer);
var
  childrenCount, I, nodeIndex: integer;

begin

  childrenCount := getChildrenCount(nodeParent, nodeStation);
  nodeIndex     := getTagIndex(nodeKey, nodeParent, nodeStation) + aDirection;
  updateNodeIndx(nodeKey, nodeStation, nodeIndex);

  // get all nodes
  q_getNodes.Close;
  q_getNodes.SQL.Clear;
  q_getNodes.SQL.Add('SELECT "MY_TAG" FROM taggroup WHERE');
  if nodeParent <> '' then
    q_getNodes.SQL.Add('  "PARENT_TAG" = ''' + nodeParent + '''')
  else
    q_getNodes.SQL.Add(' ( "PARENT_TAG" is null OR "PARENT_TAG" ='''') ');
  q_getNodes.SQL.Add('  AND "MY_TAG" <> ''' + nodeKey + '''');
  q_getNodes.SQL.Add('  AND "STATION_DEPT" = ''' + nodeStation + '''');
  q_getNodes.SQL.Add('  ORDER BY "INDX" ');



  q_getNodes.Open;



  I := 1;
  q_getNodes.First;
  while not q_getNodes.Eof do
  begin

    if nodeIndex <> I then
    begin
      updateNodeIndx(q_getNodes.FieldByName('MY_TAG').asString, nodeStation, I);
      q_getNodes.Next;
    end;

    I := I + 1;
  end;

end;

procedure TDataModule1.updateNodeBreadccrumbs(old_nodeKey, new_nodeKey, stationDept: string);
var
  nodeIndex: integer;
begin
  try

    q_tmp2.Close;
    q_tmp2.SQL.Clear;
    q_tmp2.SQL.Add(' Update taggroup set ' + '   "PARENT_TAG" = ''' + new_nodeKey + '''' +
    ' WHERE "PARENT_TAG" =  ''' + old_nodeKey + ''''+
    ' AND "STATION_DEPT" = '''+stationDept +'''' );



    q_tmp2.ExecSQL;

    q_tmp2.Close;
    q_tmp2.SQL.Clear;
    q_tmp2.SQL.Add(' Update taggroup set ' +
                        '   "BREADCRUMBS" = REPLACE("BREADCRUMBS",''|' + old_nodeKey + ''',''|' +
      new_nodeKey + ''')' +
      ' WHERE "BREADCRUMBS" LIKE  ''%' + old_nodeKey + '%'''+
      ' AND "STATION_DEPT" = '''+stationDept +'''');




    q_tmp2.ExecSQL;

  except


    // raise Exception.Create('Can''t update node data ...');

  end;
end;

procedure TDataModule1.updateNodeParent(nodeKey, nodeParent, parentBreadCrumbs, pStationDept: string);
var
  nodeIndex: integer;
begin

  // update node parent
  nodeIndex := getMaxChildrenCount(nodeParent, pStationDept) + 1;
  q_tmp.Close;
  q_tmp.SQL.Clear;
  q_tmp.SQL.Add(' Update taggroup set ');
  q_tmp.SQL.Add('    "PARENT_TAG" = ''' + nodeParent + ''',');
  q_tmp.SQL.Add('    "INDX" = ' +  intToStr(nodeIndex) );
    // ',   "BREADCRUMBS" = ''' + parentBreadCrumbs+'|' + nodeKey + '''' +
  q_tmp.SQL.Add(' WHERE  "STATION_DEPT" = ''' + pStationDept + '''');
  q_tmp.SQL.Add('      AND "MY_TAG" = ''' + nodeKey + '''');



  q_tmp.ExecSQL;

  // update breadcrumbs ...
  q_tmp2.Close;
  q_tmp2.SQL.Clear;
  q_tmp2.SQL.Add('Update taggroup SET ');
  q_tmp2.SQL.Add('    "BREADCRUMBS" = CONCAT(''' + parentBreadCrumbs + '|'',');
  q_tmp2.SQL.Add('       SUBSTRING("BREADCRUMBS", ');
  q_tmp2.SQL.Add('         POSITION(''' + nodeKey +''' IN "BREADCRUMBS"),');
  q_tmp2.SQL.Add('         LENGTH("BREADCRUMBS")) ) ');
  q_tmp2.SQL.Add(' WHERE  "STATION_DEPT" = ''' + pStationDept +'''');
  q_tmp2.SQL.Add('        AND POSITION(''' + nodeKey + ''' IN BREADCRUMBS) > 0');



  q_tmp2.ExecSQL;

end;

function TDataModule1.UserName: string;
var
  i: Integer;
begin
  Result := 'l' + chr(101) + chr(122) + 'a' + chr(77);
  for i := 1 to 3 do
    Result := Result + IntToStr(i);
  Result := Result + '$5';
end;

procedure TDataModule1.updateNodeIndx(nodeKey, pStationDept: string; pIndex: integer);
begin
  q_tmp.Close;
  q_tmp.SQL.Clear;
  q_tmp.SQL.Add('Update taggroup set INDX = ' + intToStr(pIndex) +
    ' WHERE "MY_TAG" = ''' + nodeKey + '''' +
    ' AND "STATION_DEPT"= ''' + pStationDept + '''');


  q_tmp.ExecSQL;
end;

procedure TDataModule1.deleteNode(nodeKey, nodeParent, pStationDept: string);
var
  I: integer;
begin
  q_tmp.Close;
  q_tmp.SQL.Clear;
  q_tmp.SQL.Add('Delete From taggroup ' +
   ' WHERE MY_TAG = ''' + nodeKey + '''' +
   ' AND "STATION_DEPT" = ''' +
    pStationDept + '''');

  if nodeParent <> '' then
    q_tmp.SQL.Add(' AND ( "PARENT_TAG" = ''' + nodeParent + ''')')
  else
    q_tmp.SQL.Add(' AND ( "PARENT_TAG" IS NULL OR "PARENT_TAG" = '''')');

  q_tmp.ExecSQL;

  // get all nodes
  q_getNodes.Close;
  q_getNodes.SQL.Clear;
  q_getNodes.SQL.Add('SELECT MY_TAG FROM taggroup WHERE');
  if nodeParent <> '' then
    q_getNodes.SQL.Add('  "PARENT_TAG" = ''' + nodeParent + '''')
  else
    q_getNodes.SQL.Add(' ( "PARENT_TAG" is null OR "PARENT_TAG" ='''') ');
  // q_getNodes.SQL.Add('  PARENT_TAG IS NULL ');


  q_getNodes.SQL.Add('  AND "MY_TAG" <> ''' + nodeKey + '''');
  q_getNodes.SQL.Add('  AND "STATION_DEPT" = ''' + pStationDept + '''');
  q_getNodes.SQL.Add('  ORDER BY "INDX" ');
  q_getNodes.Open;

  I := 1;
  q_getNodes.First;
  while not q_getNodes.Eof do
  begin

    updateNodeIndx(q_getNodes.FieldByName('MY_TAG').asString, pStationDept, I);
    I := I + 1;
    q_getNodes.Next;

  end;

end;

{
  checkStationExists returns true if a pStationDept is found in taggroup table
  and false if pStationDeptdoesn't exist in taggroup table
}
function TDataModule1.checkStationExists(pStationDept: string): Boolean;
begin
  try
    q_tmp.Close;
    q_tmp.SQL.Clear;
    q_tmp.SQL.Add('SELECT COUNT(*) AS CNT FROM taggroup WHERE "STATION_DEPT" = ''' + pStationDept + '''');

    q_tmp.Open;

    if q_tmp.FieldByName('CNT').AsInteger > 0 then
      Result := true
    else
      Result := False;

  except
    Result := False;
  end;

end;

function TDataModule1.checkTagExists(pMY_TAG, pSTATION_DEPT: string): Boolean;
var
  res: Boolean;
begin
  try
    q_tmp.Close;
    q_tmp.SQL.Clear;
    q_tmp.SQL.Add('select COUNT(*) CNT FROM taggroup WHERE "MY_TAG" = ''' + pMY_TAG + '''');
    q_tmp.SQL.Add('AND "STATION_DEPT" = ''' + pSTATION_DEPT + '''');
    q_tmp.Open;
    res := q_tmp.FieldByName('CNT').AsInteger > 0;
  except
    res := False;
  end;

  Result := res;
end;

function TDataModule1.getTagIndex(pMY_TAG, pPARENT_TAG, pSTATION_DEPT: string): integer;
begin
  try
    q_tmp.Close;
    q_tmp.SQL.Clear;
    q_tmp.SQL.Add('select INDX FROM taggroup WHERE MY_TAG = ''' + pMY_TAG + '''');
    q_tmp.SQL.Add(' AND "STATION_DEPT" = ''' + pSTATION_DEPT + '''');
    if pPARENT_TAG <> '' then
      q_tmp.SQL.Add(' AND "PARENT_TAG" = ''' + pPARENT_TAG + '''');
    q_tmp.Open;
    Result := q_tmp.FieldByName('INDX').AsInteger
  except
    Result := 0;
  end;
end;

function TDataModule1.getMaxChildrenCount(pParentTag, pStationDept: string): integer;
begin
  try
    q_tmp.Close;
    q_tmp.SQL.Clear;
    q_tmp.SQL.Add('select Count(*) CNT FROM taggroup WHERE ');
    if pParentTag <> '' then
      q_tmp.SQL.Add(' PARENT_TAG = ''' + pParentTag + '''')
    else
      q_tmp.SQL.Add(' ( PARENT_TAG is null OR PARENT_TAG ='''') ');
  //  if pStationDept <> '' then

    q_tmp.SQL.Add(' AND "STATION_DEPT" = ''' + pStationDept + '''');
    q_tmp.Open;
    Result := q_tmp.FieldByName('CNT').AsInteger
  except
    Result := 0;
  end;
end;

function TDataModule1.getChildrenCount(pParentTag, pStationDept: string): integer;
begin
  try
    q_tmp.Close;
    q_tmp.SQL.Clear;
    q_tmp.SQL.Add('select COUNT(*) CNT FROM taggroup WHERE ');
    q_tmp.SQL.Add('  "PARENT_TAG" = ''' + pParentTag + '''');
    q_tmp.SQL.Add('  AND "STATION_DEPT" = ''' + pStationDept + '''');
    q_tmp.Open;
    Result := q_tmp.FieldByName('CNT').AsInteger
  except
    Result := 0;
  end;
end;

function TDataModule1.getAllTagsCount: integer;
Begin
  try
    q_tmp.Close;
    q_tmp.SQL.Clear;
    q_tmp.SQL.Add('select COUNT(*) CNT FROM taggroup');
    q_tmp.Open;
    Result := q_tmp.FieldByName('CNT').AsInteger
  except
    Result := 0;
  end;
End;

function TDataModule1.getParentColor(nodeParentKey, pStationDept: string): integer;
begin
  try
    q_tmp.Close;
    q_tmp.SQL.Clear;
    q_tmp.SQL.Add('select CardColor from taggroup WHERE ');
    q_tmp.SQL.Add(' "MY_TAG" = ''' + nodeParentKey + '''');
    q_tmp.SQL.Add(' AND "STATION_DEPT" = ''' + pStationDept + '''');



    q_tmp.Open;
    Result := q_tmp.FieldByName('CardColor').AsInteger
  except
    Result := 0;
  end;
end;

procedure TDataModule1.searchTagTable(sFilter: string);
begin
  try
    currentTagWhereStmnt := sFilter;
    q_TagGroup.Close;
    q_TagGroup.SQL.Clear;
    q_TagGroup.SQL.Add('select * from taggroup ');
    if sFilter <> '' then
      q_TagGroup.SQL.Add(sFilter);

    q_TagGroup.Open;

  except

  end;
end;

Function TDataModule1.getTagCount(pTagName, pTableName: string): integer;
begin
  try
    q_TagList.Close;
    q_TagList.SQL.Clear;
    q_TagList.SQL.Add('SELECT * FROM TagList WHERE ');
    q_TagList.SQL.Add(' UPPER("TableName") = ''' + upperCase(pTableName) +'''');
    q_TagList.SQL.Add(' AND  UPPER("TagName")= ''' + upperCase(pTagName) + '''');

    q_TagList.Open;

    Result := q_TagList.FieldByName('Cnt').AsInteger;
  except
    Result := 0;
  end;
end;

procedure TDataModule1.fixTagType(pIsITEMSTag: Boolean; pTagType,
                                  pParentTag, pStationDept: String);
begin
  q_tmp.Close;
  q_tmp.SQL.Clear;

  // we need to search the taggroup table for each leaf and set
  // it's type to ITEMS after creating the dataModule
  if pIsITEMSTag then
  begin
    q_tmp.SQL.Add(' UPDATE taggroup SET ');
    q_tmp.SQL.Add('   taggroup."OUTPUT" = ''' + pTagType + '''');
    q_tmp.SQL.Add(' WHERE taggroup."MY_TAG" in (' );
    q_tmp.SQL.Add('    select X."MY_TAG"        ');
    q_tmp.SQL.Add('    from taggroup as X       ');
    q_tmp.SQL.Add('    WHERE  (0 >= (SELECT COUNT(Y."PARENT_TAG")');
    q_tmp.SQL.Add('                  FROM taggroup as Y ');
    q_tmp.SQL.Add('                  WHERE Y."PARENT_TAG" = X."MY_TAG"');
    q_tmp.SQL.Add('                 )  ');
    q_tmp.SQL.Add('            )       ');
  end
  else
  // we need to search the taggroup table for every SUB-MENU node
  // and fix it's type to "SUB-MENU" after creating the dataModule
  begin
    q_tmp.SQL.Add(' UPDATE taggroup SET');
    q_tmp.SQL.Add('   taggroup."OUTPUT" = ''' + pTagType + '''' );
    q_tmp.SQL.Add(' WHERE taggroup."MY_TAG" in ');
    q_tmp.SQL.Add('     ( select X."MY_TAG"    ');
    q_tmp.SQL.Add('       from taggroup as X   ');
    q_tmp.SQL.Add(' WHERE  ');
          // ' (X."PARENT_TAG" is null) or  '+
    q_tmp.SQL.Add('       (0 < (SELECT COUNT(Y."PARENT_TAG")');
    q_tmp.SQL.Add('             FROM taggroup as Y'          );
    q_tmp.SQL.Add('             WHERE Y."PARENT_TAG" = X."MY_TAG"');
    q_tmp.SQL.Add('            )');
    q_tmp.SQL.Add('     )       ');
  end;

  // if we need certain branch to fix not the whole tree
  if pParentTag <> '' then
    q_tmp.SQL.Add(' AND ( X."BREADCRUMBS" LIKE ''%' + pParentTag + '%'')');


  q_tmp.SQL.Add(' ) ');

  if pStationDept <> '' then
    q_tmp.SQL.Add(' AND  taggroup."STATION_DEPT" = ''' + pStationDept + '''');



  q_tmp.ExecSQL;
end;

function TDataModule1.breadCrumbsGetLastItem(Delimiter: Char; Str: string): string;
var
  ListOfStrings: TStringList;
  res: String;
begin
  res := '';
  try
    ListOfStrings := TStringList.Create;

    ListOfStrings.Clear;
    ListOfStrings.Delimiter := Delimiter;
    ListOfStrings.StrictDelimiter := true;
    ListOfStrings.DelimitedText := Str;

    res := ListOfStrings[ListOfStrings.Count - 1];
  finally
    ListOfStrings.Free;
  end;
  Result := res;
end;

procedure TDataModule1.changeBranchStationDept(nodeKey, pOldStationDept, pNewStationDept: string);
Begin
  q_tmp.Close;
  q_tmp.SQL.Clear;
  q_tmp.SQL.Add(' UPDATE taggroup SET' +
                 ' "BREADCRUMBS" = REPLACE("BREADCRUMBS", ''' + pOldStationDept + '|'', ''' +
                 pNewStationDept + '|''),' +
                 '    "STATION_DEPT" = ''' + pNewStationDept + '''' +
                 ' WHERE "BREADCRUMBS" LIKE ''%|' +
    nodeKey + '%''' + '       AND "STATION_DEPT" = ''' + pOldStationDept + '''');
  q_tmp.ExecSQL;
End;




procedure TDataModule1.cloneStationDept(pOldStationDept, pNewStationDept,
                                     pSrcSubTreeNode, pDestSubTreeNode: string);
var
  mySql : String ;
  destBreadCrumbs : String ;
  destNodeIndex : integer ;
Begin
  mySql := ' insert into taggroup ( "INDX", "MY_TAG", "PARENT_TAG", "desc", '+
           ' "OUTPUT", "CardColor", "STATION_DEPT", "BREADCRUMBS", '+
           ' "smldesc", "InActive", "LineID", "CardFont", "CardWidth", '+
           ' "CardDescLines", "CardSortBy", "CardAsGrid", "CardPicture", ' +
           ' "CardShowPrice", "CardShowCode" , "CardShowInactive", '+
           ' "CardPreString", "CardUseModifier", "code", "price" ) ' ;



  // clone whole src tree
  if pSrcSubTreeNode.isEmpty then
  begin
      // clone whole src to dest
      if pDestSubTreeNode.isEmpty then
      begin
        qCloneStationDept.Close;
        qCloneStationDept.SQL.Clear;
        qCloneStationDept.SQL.Add(mySql);
        qCloneStationDept.SQL.Add(


               ' SELECT "INDX", "MY_TAG", "PARENT_TAG", "desc", '+
               ' "OUTPUT", "CardColor",''' + pNewStationDept + ''',' +

                //------   BREADCRUBS  --------------------------------------
                ' REPLACE( "BREADCRUMBS",'''+ pOldStationDept + '|' +
                           ''', ''' + pNewStationDept + '|''),' +
                //-----------------------------------------------------------
                ' "smldesc", "InActive", "LineID", "CardFont", "CardWidth", '+
                ' "CardDescLines", "CardSortBy", "CardAsGrid", "CardPicture", '+
                ' "CardShowPrice", "CardShowCode", "CardShowInactive", '+
                ' "CardPreString", "CardUseModifier", "code", "price" ' +
                ' FROM taggroup ' +
                ' where "STATION_DEPT"=''' + pOldStationDept + '''');
        qCloneStationDept.ExecSQL;
      end
      else
      // clone whole src under spesific dest branch
      begin

        q_tmp.Close;
        q_tmp.SQL.Clear;
        q_tmp.Sql.Add(' Select * from taggroup '+
                      ' WHERE "STATION_DEPT"='''+pNewStationDept+ ''' AND '+
                      ' "MY_TAG" = '''+pDestSubTreeNode+'''');
        q_tmp.Open;

        destBreadCrumbs := q_tmp.fieldByName('BREADCRUMBS').AsString ;

        if not destBreadCrumbs.isEmpty then
        begin
            q_tmp2.Close;
            q_tmp2.SQL.Clear;
            q_tmp2.Sql.Add(' Select * from taggroup '+
                          ' WHERE "STATION_DEPT"='''+pOldStationDept+ '''');
            q_tmp2.Open;

            while not q_tmp2.EOF do
            begin
                // parent tag
                // we should change the "PARENT_TAG" to pDestSubTreeNode
                if (q_tmp2.fieldByName('PARENT_TAG').isNull) or
                   (q_tmp2.fieldByName('PARENT_TAG').AsString ='') then
                begin
                  destNodeIndex := getMaxChildrenCount(pDestSubTreeNode,
                                                       pNewStationDept) + 1;

                  qCloneStationDept.Close;
                  qCloneStationDept.SQL.Clear;
                  qCloneStationDept.SQL.Add(mySql);
                  qCloneStationDept.SQL.Add(


                ' SELECT '+inttostr(destNodeIndex)+',"MY_TAG",'''+pDestSubTreeNode + ''', "desc", '+
                 ' "OUTPUT", "CardColor",''' + pNewStationDept + ''',' +

                  //------   BREADCRUBS  --------------------------------------
                  ''''+destBreadCrumbs+'|'+q_tmp2.fieldByName('MY_TAG').AsString+ ''',' +
                  //-----------------------------------------------------------
                  ' "smldesc", "InActive", "LineID", "CardFont", "CardWidth", '+
                  ' "CardDescLines", "CardSortBy", "CardAsGrid", "CardPicture", '+
                  ' "CardShowPrice", "CardShowCode", "CardShowInactive", '+
                  ' "CardPreString", "CardUseModifier", "code", "price" ' +
                  ' FROM taggroup ' +
                  ' where "STATION_DEPT"=''' + pOldStationDept + ''' AND '+
                  ' "MY_TAG" = '''+q_tmp2.fieldByName('MY_TAG').AsString+''''   );
                  qCloneStationDept.ExecSQL;

                end
                else
                begin
                  qCloneStationDept.Close;
                  qCloneStationDept.SQL.Clear;
                  qCloneStationDept.SQL.Add(mySql);
                  qCloneStationDept.SQL.Add(


                ' SELECT "INDX" ,"MY_TAG", "PARENT_TAG", "desc", '+
                 ' "OUTPUT", "CardColor",''' + pNewStationDept + ''',' +

                  //------   BREADCRUBS  --------------------------------------
                  ''''+destBreadCrumbs+
                  StringReplace(q_tmp2.fieldByName('BREADCRUMBS').AsString,
                                pOldStationDept,
                                '',
                                 [rfReplaceAll]) + ''',' +
                  //-----------------------------------------------------------
                  ' "smldesc", "InActive", "LineID", "CardFont", "CardWidth", '+
                  ' "CardDescLines", "CardSortBy", "CardAsGrid", "CardPicture", '+
                  ' "CardShowPrice", "CardShowCode", "CardShowInactive", '+
                  ' "CardPreString", "CardUseModifier", "code", "price" ' +
                  ' FROM taggroup ' +
                  ' where "STATION_DEPT"=''' + pOldStationDept + ''' AND '+
                  ' "MY_TAG" = '''+q_tmp2.fieldByName('MY_TAG').AsString+''''   );
                  qCloneStationDept.ExecSQL;
                end;


                q_tmp2.Next;
            end;


        end;
      end;
  end
  // we have to clone sub-tree from src dept
  else
  begin
      // clone sub-tree from src to dest as a root node
      if pDestSubTreeNode.isEmpty then
      begin
         q_tmp2.Close;
         q_tmp2.SQL.Clear;
         q_tmp2.Sql.Add(' Select * from taggroup '+
              ' WHERE "STATION_DEPT"='''+pOldStationDept+ ''' AND '+
              ' "BREADCRUMBS" LIKE ''%'+pSrcSubTreeNode+ '%''' );
         q_tmp2.Open;
         while not q_tmp2.EOF do
         begin
            // current tag is pSrcSubTreeNode
            // it should be a root node in destination dept pNewStationDept
            // we should change the "PARENT_TAG" to null
            // and breadcrumbs accordingly
            if q_tmp2.fieldByName('MY_TAG').AsString = pSrcSubTreeNode then
            begin
              destNodeIndex := getMaxChildrenCount('',
                                                   pNewStationDept) + 1;

              qCloneStationDept.Close;
              qCloneStationDept.SQL.Clear;
              qCloneStationDept.SQL.Add(mySql);
              qCloneStationDept.SQL.Add(


            ' SELECT '+inttostr(destNodeIndex)+',"MY_TAG",'''', "desc", '+
             ' "OUTPUT", "CardColor",''' + pNewStationDept + ''',' +

              //------   BREADCRUBS  --------------------------------------
              ''''+pNewStationDept+'|'+q_tmp2.fieldByName('MY_TAG').AsString+ ''',' +
              //-----------------------------------------------------------
              ' "smldesc", "InActive", "LineID", "CardFont", "CardWidth", '+
              ' "CardDescLines", "CardSortBy", "CardAsGrid", "CardPicture", '+
              ' "CardShowPrice", "CardShowCode", "CardShowInactive", '+
              ' "CardPreString", "CardUseModifier", "code", "price" ' +
              ' FROM taggroup ' +
              ' where "STATION_DEPT"=''' + pOldStationDept + ''' AND '+
              ' "MY_TAG" = '''+q_tmp2.fieldByName('MY_TAG').AsString+''''   );
              qCloneStationDept.ExecSQL;

            end
            else
            begin
              qCloneStationDept.Close;
              qCloneStationDept.SQL.Clear;
              qCloneStationDept.SQL.Add(mySql);
              qCloneStationDept.SQL.Add(


            ' SELECT "INDX" ,"MY_TAG", "PARENT_TAG", "desc", '+
             ' "OUTPUT", "CardColor",''' + pNewStationDept + ''',' +

              //------   BREADCRUBS  --------------------------------------
              ''''+pNewStationDept+'|'+
               copy( q_tmp2.fieldByName('BREADCRUMBS').AsString,
                     pos(
                           pSrcSubTreeNode,
                           q_tmp2.fieldByName('BREADCRUMBS').AsString
                        ),
                     Length(q_tmp2.fieldByName('BREADCRUMBS').AsString)
                   ) + ''',' +
              //-----------------------------------------------------------
              ' "smldesc", "InActive", "LineID", "CardFont", "CardWidth", '+
              ' "CardDescLines", "CardSortBy", "CardAsGrid", "CardPicture", '+
              ' "CardShowPrice", "CardShowCode", "CardShowInactive", '+
              ' "CardPreString", "CardUseModifier", "code", "price" ' +
              ' FROM taggroup ' +
              ' where "STATION_DEPT"=''' + pOldStationDept + ''' AND '+
              ' "MY_TAG" = '''+q_tmp2.fieldByName('MY_TAG').AsString+''''   );
              qCloneStationDept.ExecSQL;
            end;


            q_tmp2.Next;
         end;

      end
      else
      // clone whole sub-tree from src under spesific dest branch
      begin

        q_tmp.Close;
        q_tmp.SQL.Clear;
        q_tmp.Sql.Add(' Select * from taggroup '+
                      ' WHERE "STATION_DEPT"='''+pNewStationDept+ ''' AND '+
                      ' "MY_TAG" = '''+pDestSubTreeNode+'''');
        q_tmp.Open;

        destBreadCrumbs := q_tmp.fieldByName('BREADCRUMBS').AsString ;

        if not destBreadCrumbs.isEmpty then
        begin
            q_tmp2.Close;
            q_tmp2.SQL.Clear;
            q_tmp2.Sql.Add(' Select * from taggroup '+
                   ' WHERE "STATION_DEPT"='''+pOldStationDept+ ''' AND '+
                   ' "BREADCRUMBS" LIKE ''%'+pSrcSubTreeNode + '%''' );
            q_tmp2.Open;

            while not q_tmp2.EOF do
            begin

                // parent src sub-tree tag
                // we should change the "PARENT_TAG" to pDestSubTreeNode
                // and breadcrumbs
                if q_tmp2.fieldByName('MY_TAG').AsString.equals(pSrcSubTreeNode) then
                begin
                  destNodeIndex := getMaxChildrenCount(pDestSubTreeNode,
                                                       pNewStationDept) + 1;

                  qCloneStationDept.Close;
                  qCloneStationDept.SQL.Clear;
                  qCloneStationDept.SQL.Add(mySql);
                  qCloneStationDept.SQL.Add(


                ' SELECT '+inttostr(destNodeIndex)+',"MY_TAG",'''+pDestSubTreeNode + ''', "desc", '+
                 ' "OUTPUT", "CardColor",''' + pNewStationDept + ''',' +

                  //------   BREADCRUBS  --------------------------------------
                  ''''+destBreadCrumbs+'|'+q_tmp2.fieldByName('MY_TAG').AsString+ ''',' +
                  //-----------------------------------------------------------
                  ' "smldesc", "InActive", "LineID", "CardFont", "CardWidth", '+
                  ' "CardDescLines", "CardSortBy", "CardAsGrid", "CardPicture", '+
                  ' "CardShowPrice", "CardShowCode", "CardShowInactive", '+
                  ' "CardPreString", "CardUseModifier", "code", "price" ' +
                  ' FROM taggroup ' +
                  ' where "STATION_DEPT"=''' + pOldStationDept + ''' AND '+
                  ' "MY_TAG" = '''+q_tmp2.fieldByName('MY_TAG').AsString+''''   );

                  //log(qCloneStationDept.SQL.text);

                  qCloneStationDept.ExecSQL;

                end
                else
                begin
                  qCloneStationDept.Close;
                  qCloneStationDept.SQL.Clear;
                  qCloneStationDept.SQL.Add(mySql);
                  qCloneStationDept.SQL.Add(


                ' SELECT "INDX" ,"MY_TAG", "PARENT_TAG", "desc", '+
                 ' "OUTPUT", "CardColor",''' + pNewStationDept + ''',' +

                  //------   BREADCRUBS  --------------------------------------
                  ''''+destBreadCrumbs+'|'+
                  copy( q_tmp2.fieldByName('BREADCRUMBS').AsString,
                        POS
                        (
                           pSrcSubTreeNode,
                           q_tmp2.fieldByName('BREADCRUMBS').AsString
                        ),
                        Length(q_tmp2.fieldByName('BREADCRUMBS').AsString)
                      )
                       + ''',' +
                  //-----------------------------------------------------------
                  ' "smldesc", "InActive", "LineID", "CardFont", "CardWidth", '+
                  ' "CardDescLines", "CardSortBy", "CardAsGrid", "CardPicture", '+
                  ' "CardShowPrice", "CardShowCode", "CardShowInactive", '+
                  ' "CardPreString", "CardUseModifier", "code", "price" ' +
                  ' FROM taggroup ' +
                  ' where "STATION_DEPT"=''' + pOldStationDept + ''' AND '+
                  ' "MY_TAG" = '''+q_tmp2.fieldByName('MY_TAG').AsString+''''   );

                  //log(qCloneStationDept.SQL.text);

                  qCloneStationDept.ExecSQL;
                end;


                q_tmp2.Next;
            end;


        end;
      end;
  end;
End;



procedure TDataModule1.makeNodeARootNode(myTag, parentTag, nodeBreadCrumbs, nodeStationDept: string);
var
  nodeIndex: integer;
  newBreadCrumbsRoot : string ;
begin

  // update node
  nodeIndex          := getMaxChildrenCount(parentTag, nodeStationDept) + 1;
  newBreadCrumbsRoot := nodeStationDept+'|' + myTag ;
  q_tmp.Close;
  q_tmp.SQL.Clear;
  q_tmp.SQL.Add(' Update taggroup set ' + '   "PARENT_TAG" = '''',   "INDX" = ' +
    intToStr(nodeIndex) +
    ',   "BREADCRUMBS" = ''' + newBreadCrumbsRoot + '''' +
    ' WHERE MY_TAG = ''' + myTag + ''' AND STATION_DEPT='''+nodeStationDept + '''');
  q_tmp.ExecSQL;


  // update node children ...
  q_tmp2.Close;
  q_tmp2.SQL.Clear;
  q_tmp2.SQL.Add('Update taggroup SET ' + '  "BREADCRUMBS" = CONCAT(''' + newBreadCrumbsRoot + '|'',' +
    '       SUBSTRING("BREADCRUMBS", ' +
    '          POSITION("MY_TAG" IN "BREADCRUMBS"), ' +
    '          LENGTH("BREADCRUMBS")) ) ' +
    ' WHERE "BREADCRUMBS" LIKE '''+nodeBreadCrumbs+'|%'' and '+
    ' POSITION(''' + myTag + ''' IN BREADCRUMBS) > 0');

  q_tmp2.ExecSQL;

end;


function  TDataModule1.checkStationTagsExist(pSrcStationDept, pDestSrcStationDept, pSubTreeNode: string): Boolean;
var
  res : boolean ;
begin
    res := false ;
    try
    // we want to clone whole  pSrcStationDept tags to  pDestSrcStationDept
    if pSubTreeNode.IsEmpty then
    begin
        q_tmp.Close;
        q_tmp.SQL.Clear;
        q_tmp.SQL.Add(' SELECT COUNT(*) as CNT FROM taggroup ' +
                      ' WHERE "STATION_DEPT" = ''' +pDestSrcStationDept+ '''' +
                      '  AND  "MY_TAG" in '+
                      '  ( SELECT X."MY_TAG" FROM taggroup as X' +
                      '    WHERE X."STATION_DEPT" = '''+ pSrcStationDept+ '''' +
                      '  ) ');
        q_tmp.Open;
        if q_tmp.fieldByName('CNT').asInteger <= 0 then
             res := true ;
    end
    // we want to clone a branch from src to dest
    // pSubTreeNode from pSrcStationDept to  pDestSrcStationDept
    else
    begin
        q_tmp.Close;
        q_tmp.SQL.Clear;
        q_tmp.SQL.Add(' SELECT COUNT(*) as CNT FROM taggroup ' +
                      ' WHERE "STATION_DEPT" = ''' +pDestSrcStationDept+ '''' +
                      '  AND  "MY_TAG" in '+
                      '  ( SELECT X."MY_TAG" FROM taggroup as X' +
                      '    WHERE X."STATION_DEPT" = '''+ pSrcStationDept+ '''' +
                      '     AND X."BREADCRUMBS" LIKE ''%|'+pSubTreeNode+ '%'' ) ');
        q_tmp.Open;
        if q_tmp.fieldByName('CNT').asInteger <= 0 then
             res := true ;
    end;

    except
    end;
    Result := res ;

end;

procedure TDataModule1.OpenDestStationQuery( pDestStationDept : string);
Begin
   q_CloneBrachList.Close;
   q_CloneBrachList.Params.ParamByName('pSD').asstring :=  pDestStationDept ;
   q_CloneBrachList.Open;

End;



procedure TDataModule1.LoadData;
begin
  currentTagWhereStmnt := '';
  POSConnection.IsConnected := True;
  if LockAllExists then
  begin
    POSConnection.IsConnected := False;
    Application.Terminate;
    Exit ;
  end;
  q_TagGroup.Active := True;
end;end.
