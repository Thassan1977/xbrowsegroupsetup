object frmGroupSetup: TfrmGroupSetup
  Left = 0
  Top = 0
  Caption = 'Group Setup'
  ClientHeight = 749
  ClientWidth = 1312
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 85
    Width = 1312
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitTop = 231
    ExplicitWidth = 518
  end
  object Panel5: TPanel
    Left = 0
    Top = 88
    Width = 1312
    Height = 661
    Align = alClient
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 1
      Top = 1
      Height = 659
      ExplicitLeft = 88
      ExplicitTop = 400
      ExplicitHeight = 100
    end
    object Panel2: TPanel
      Left = 379
      Top = 1
      Width = 932
      Height = 659
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object pnlBotm: TPanel
        AlignWithMargins = True
        Left = 3
        Top = 574
        Width = 926
        Height = 30
        Align = alBottom
        TabOrder = 0
        object btnRefresh: TSpeedButton
          AlignWithMargins = True
          Left = 893
          Top = 4
          Width = 23
          Height = 22
          Margins.Right = 9
          Align = alRight
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000000000010000
            0004000000040000000500000006000000090000000B0000000B0000000B0000
            0009000000060000000300000001000000000000000000000000000000030000
            00090000000B0000000D00000010000000140000001600000017000000160000
            00140000000F0000000A0000000500000002000000000000000000000006102F
            228700000015000000180A1F1663143A29A71B5039DC1F5E43FB1B5039DC143A
            29A70A1E1662000000120000000B000000050000000100000000000000082364
            49FF133628981336289A236348FC52AB8FFF74CEB9FF88E4D3FF76D0BCFF56AD
            93FF236348FC13362897030907270000000A0000000200000001000000092669
            4EFF4BA283FF4BA283FF6ED0B2FF64B399FF4A8D76FF2C6E54FF4F8F79FF78BB
            A8FF87DBC4FF4C9F82FF15392A960000000F0000000400000001000000082A6F
            53FF91E1C9FF91E1C9FF6DB79DFF1A4534A70C1F1751020403130C1F174D1A45
            33A469A690FF97DFCAFF2A6E52FC0E241B5E0000000500000001000000072F77
            5BFFCFF3E8FFCFF3E8FF81B7A3FF183D2F8B0000000A00000005000000060103
            02111D4A39A695C5B4FF91C3B1FF1D4938A30000000600000001000000063784
            66FF378466FF378466FF378466FF378466FF1C42338200000004000000060000
            000B0F251C53378466FF378466FF2E6F55DA0000000600000002000000060000
            00110000001700000017000000120000000A00000006000000050000000A0000
            0011000000180000001C0000001A000000140000000700000002000000051B50
            39DA205F44FF205F44FF091A13570000000E0000000900000009102F2286205F
            44FF205F44FF205F44FF205F44FF205F44FF000000080000000300000004163D
            2CA25AAF97FF4DA98AFF163F2DAA0103021D0000001200000012000000151233
            2691419A7BFF60CFACFF60CFACFF236449FF0000000900000003000000020C22
            195926684DFC8EDEC9FF4D9B80FF184131AA0B1D165B020403240B1D165B1841
            31AA5DAF94FF8CE0C9FF8BE0C8FF26694EFF0000000800000003000000010000
            0005173C2D907AB19EFFAFE7D7FF77BAA3FF498F75FF2E7458FF498F75FF77BA
            A3FFAFE7D7FF7FB6A3FF7EB6A3FF2A6F53FF0000000600000002000000000000
            0002040B081C194132902E765AFC97C4B4FFB9E0D4FFCEF1E6FFB9E0D4FF97C4
            B4FF2E765AFC1A413191194132902F775BFF0000000300000001000000000000
            00000000000100000003122A215622503E9F2F6F56D9368164FA2F6F56D92250
            3E9F122A215600000005000000041B4233810000000100000001000000000000
            0000000000000000000100000001000000020000000300000004000000030000
            0002000000010000000100000001000000010000000000000000}
          OnClick = btnRefreshClick
          ExplicitLeft = 1104
        end
        object edtTestfilter: TEdit
          AlignWithMargins = True
          Left = 763
          Top = 4
          Width = 121
          Height = 22
          Margins.Right = 6
          Align = alRight
          TabOrder = 0
          ExplicitHeight = 21
        end
      end
      object pnlButton: TPanel
        AlignWithMargins = True
        Left = 575
        Top = 3
        Width = 18
        Height = 540
        Align = alLeft
        TabOrder = 1
        object icbStationDept: TcxImageComboBox
          Left = 1
          Top = 511
          Margins.Left = 24
          Margins.Top = 0
          Margins.Right = 24
          Align = alBottom
          AutoSize = False
          BeepOnEnter = False
          Properties.Items = <
            item
              Description = 'ITEMS'
              ImageIndex = 0
              Value = 'prdmstr'
            end
            item
              Description = 'SUB-MENU'
              Value = 'taggroup'
            end>
          Properties.ValidateOnEnter = False
          TabOrder = 0
          Height = 28
          Width = 16
        end
      end
      object pnlCard: TPanel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 390
        Height = 540
        Align = alLeft
        TabOrder = 2
        object btnCardFont: TLMDSpeedButton
          Left = 16
          Top = 480
          Width = 65
          Height = 23
          Caption = 'Card Font'
          AutoSize = False
          OnClick = btnCardFontClick
          ThemeMode = ttmNone
        end
        object btnCardSample: TLMDSpeedButton
          Left = 212
          Top = 454
          Width = 102
          Height = 51
          Caption = 'Card Sample'
          AutoSize = False
          EnterColor = clBtnFace
          ImageIndex = -1
          ThemeMode = ttmNone
          ButtonStyle = ubsDelphi
        end
        object lblCard: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 13
          Width = 382
          Height = 16
          Margins.Top = 12
          Align = alTop
          Alignment = taCenter
          Caption = 'Card settings'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold, fsUnderline]
          ParentFont = False
          ExplicitWidth = 87
        end
        object lblCaption: TLabel
          AlignWithMargins = True
          Left = 25
          Top = 94
          Width = 340
          Height = 13
          Margins.Left = 24
          Margins.Top = 6
          Margins.Right = 24
          Margins.Bottom = 0
          Align = alTop
          Caption = 'Button Caption'
          ExplicitWidth = 72
        end
        object lblCardDescLines: TLabel
          Left = 19
          Top = 407
          Width = 83
          Height = 13
          Caption = 'Description  Lines'
        end
        object lblCardWidth: TLabel
          Left = 194
          Top = 407
          Width = 54
          Height = 13
          Caption = 'Card Width'
        end
        object Image1: TImage
          Left = 16
          Top = 426
          Width = 169
          Height = 103
          Center = True
          Proportional = True
        end
        object cxColorComboBoxCard: TcxColorComboBox
          Left = 16
          Top = 454
          BeepOnEnter = False
          Properties.AllowSelectColor = True
          Properties.ClearKey = 46
          Properties.CustomColors = <>
          Properties.DropDownListStyle = lsEditFixedList
          Properties.PrepareList = cxplHTML4
          Properties.OnChange = cxColorComboBoxCardPropertiesChange
          TabOrder = 0
          OnExit = cxColorComboBoxCardExit
          Width = 190
        end
        object pnlTagfilter: TPanel
          AlignWithMargins = True
          Left = 24
          Top = 140
          Width = 342
          Height = 45
          Margins.Left = 23
          Margins.Top = 6
          Margins.Right = 23
          Align = alTop
          BevelEdges = []
          BevelOuter = bvNone
          TabOrder = 1
          object SourceTag: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 336
            Height = 13
            Align = alTop
            Caption = 'Parent Tag '
            ExplicitWidth = 56
          end
          object edtTagList: TDBEdit
            AlignWithMargins = True
            Left = 3
            Top = 22
            Width = 336
            Height = 21
            Align = alTop
            DataField = 'PARENT_TAG'
            DataSource = dsTagGroup
            TabOrder = 0
            ExplicitTop = 24
          end
        end
        object pnlType: TPanel
          AlignWithMargins = True
          Left = 25
          Top = 44
          Width = 340
          Height = 41
          Margins.Left = 24
          Margins.Top = 12
          Margins.Right = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object lblType: TLabel
            Left = 0
            Top = 0
            Width = 340
            Height = 13
            Margins.Left = 24
            Margins.Top = 12
            Margins.Right = 24
            Align = alTop
            Caption = 'Output Type'
            ExplicitWidth = 61
          end
          object icbTable: TcxImageComboBox
            Left = 0
            Top = 13
            Margins.Left = 24
            Margins.Top = 0
            Margins.Right = 24
            Align = alClient
            EditValue = 'prdmstr'
            Properties.Items = <
              item
                Description = 'ITEMS'
                ImageIndex = 0
                Value = 'prdmstr'
              end
              item
                Description = 'SUB-MENU'
                Value = 'taggroup'
              end>
            Properties.OnChange = icbTablePropertiesChange
            TabOrder = 0
            Width = 340
          end
        end
        object pnlMyTag: TPanel
          AlignWithMargins = True
          Left = 25
          Top = 194
          Width = 340
          Height = 49
          Margins.Left = 24
          Margins.Top = 6
          Margins.Right = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object lblSelfTag: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 6
            Width = 334
            Height = 13
            Margins.Top = 6
            Align = alTop
            Caption = 'My Tag - Item Tag'
            ExplicitWidth = 88
          end
          object btn_tagDropDown: TSpeedButton
            Left = 313
            Top = 24
            Width = 23
            Height = 22
            Caption = ':::'
            OnClick = btn_tagDropDownClick
          end
          object cb_tagDropDown: TComboBox
            Left = 3
            Top = 25
            Width = 304
            Height = 21
            TabOrder = 1
            Visible = False
            OnCloseUp = cb_tagDropDownCloseUp
          end
          object lbledtTagFilter: TDBEdit
            AlignWithMargins = True
            Left = 3
            Top = 25
            Width = 304
            Height = 21
            Align = alLeft
            DataField = 'MY_TAG'
            DataSource = dsTagGroup
            TabOrder = 0
          end
        end
        object lbledtCaption: TDBEdit
          AlignWithMargins = True
          Left = 25
          Top = 110
          Width = 340
          Height = 21
          Margins.Left = 24
          Margins.Right = 24
          Align = alTop
          DataField = 'desc'
          DataSource = dsTagGroup
          TabOrder = 4
        end
        object pnlSortby: TPanel
          AlignWithMargins = True
          Left = 23
          Top = 254
          Width = 185
          Height = 41
          Margins.Left = 24
          Margins.Top = 12
          Margins.Right = 24
          AutoSize = True
          BevelEdges = []
          BevelOuter = bvNone
          ShowCaption = False
          TabOrder = 5
          object lblSortby: TLabel
            AlignWithMargins = True
            Left = 0
            Top = 0
            Width = 185
            Height = 13
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Align = alTop
            Caption = 'Sort by'
            ExplicitWidth = 35
          end
          object icbSort: TcxImageComboBox
            Left = 0
            Top = 13
            Margins.Left = 24
            Margins.Top = 6
            Margins.Right = 24
            Margins.Bottom = 6
            Align = alClient
            Properties.Items = <
              item
                Description = 'Description'
                ImageIndex = 0
                Value = 'p.[desc]'
              end
              item
                Description = 'Code'
                Value = 'p.code'
              end
              item
                Description = 'Price'
                Value = 'p.price'
              end>
            Properties.OnChange = icbSortPropertiesChange
            TabOrder = 0
            Width = 185
          end
        end
        object lbledtPicture222: TLMDLabeledFileOpenEdit
          AlignWithMargins = True
          Left = 24
          Top = 325
          Width = 337
          Height = 21
          Hint = ''
          Margins.Left = 24
          Margins.Top = 24
          Margins.Right = 24
          Bevel.Mode = bmWindows
          Caret.BlinkRate = 530
          TabOrder = 6
          Visible = False
          CustomButtons = <
            item
              Glyph.Data = {
                DE000000424DDE0000000000000076000000280000000E0000000D0000000100
                0400000000006800000000000000000000001000000010000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D00000000DDD
                DD000033333330DDDD000B033333330DDD000FB033333330DD000BFB03333333
                0D000FBFB000000000000BFBFBFBF0DDDD000FBFBFBFB0DDDD000BFB000000DD
                DD00D000DDDDDD000D00DDDDDDDDDDD00D00DDDDDDD0DD0D0D00DDDDDDDD00DD
                DD00}
              ParentFont = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Index = 0
              DisplayName = 'TLMDSpecialButton'
              ImageIndex = 0
              ListIndex = 0
              UsePngGlyph = False
            end>
          CustomButtonWidth = 18
          Filter = 'Pictuers|*.bmp;*.png;*.jpg'
          FilenameOnly = False
          EditLabel.Width = 35
          EditLabel.Height = 15
          EditLabel.Visible = False
          EditLabel.Caption = 'Picture'
        end
        object chkCardPrice: TDBCheckBox
          Left = 21
          Top = 367
          Width = 140
          Height = 17
          Caption = 'Include Price on Card'
          DataField = 'CardShowPrice'
          DataSource = dsTagGroup
          TabOrder = 7
        end
        object chkCardCode: TDBCheckBox
          Left = 189
          Top = 367
          Width = 140
          Height = 17
          Caption = 'Include Code on card'
          DataField = 'CardShowCode'
          DataSource = dsTagGroup
          TabOrder = 8
        end
        object seCardWidth: TcxDBSpinEdit
          Left = 191
          Top = 426
          DataBinding.DataField = 'CardWidth'
          DataBinding.DataSource = dsTagGroup
          Properties.LargeIncrement = 1.000000000000000000
          Properties.MaxValue = 200.000000000000000000
          Properties.MinValue = 50.000000000000000000
          TabOrder = 9
          OnClick = seCardWidthClick
          Width = 122
        end
        object seDescLines: TcxDBSpinEdit
          Left = 18
          Top = 426
          DataBinding.DataField = 'CardDescLines'
          DataBinding.DataSource = dsTagGroup
          Properties.LargeIncrement = 1.000000000000000000
          Properties.MaxValue = 6.000000000000000000
          Properties.MinValue = 1.000000000000000000
          TabOrder = 10
          Width = 111
        end
        object edtCardFont: TDBEdit
          Left = 87
          Top = 481
          Width = 121
          Height = 21
          DataField = 'CardFont'
          DataSource = dsTagGroup
          TabOrder = 11
        end
        object pnlPicture: TPanel
          AlignWithMargins = True
          Left = 25
          Top = 252
          Width = 340
          Height = 35
          Margins.Left = 24
          Margins.Top = 6
          Margins.Right = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 12
          object Label3: TLabel
            AlignWithMargins = True
            Left = 0
            Top = 0
            Width = 340
            Height = 13
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Align = alTop
            Caption = 'Picture'
            ExplicitWidth = 33
          end
          object lbledtPicture: TDBEdit
            Left = 0
            Top = 13
            Width = 304
            Height = 22
            Align = alLeft
            DataField = 'CardPicture'
            DataSource = dsTagGroup
            TabOrder = 0
            ExplicitHeight = 21
          end
          object btn_browsePics: TBitBtn
            Left = 313
            Top = 12
            Width = 23
            Height = 23
            Caption = ':::'
            TabOrder = 1
            OnClick = btn_browsePicsClick
          end
        end
      end
      object pnlDescrip: TPanel
        AlignWithMargins = True
        Left = 3
        Top = 610
        Width = 926
        Height = 46
        Align = alBottom
        TabOrder = 3
        object lblShowsup: TLabel
          Left = 19
          Top = 8
          Width = 36
          Height = 13
          Caption = 'Parent:'
        end
        object lblParenValue: TLabel
          Left = 61
          Top = 8
          Width = 64
          Height = 13
          Caption = 'lblParenValue'
        end
        object lbMyTag: TLabel
          Left = 16
          Top = 23
          Width = 39
          Height = 13
          Caption = 'My Tag:'
        end
        object lblMyTagValue: TLabel
          Left = 61
          Top = 23
          Width = 68
          Height = 13
          Caption = 'lblMyTagValue'
        end
      end
      object pnlMisc: TPanel
        AlignWithMargins = True
        Left = 399
        Top = 3
        Width = 170
        Height = 540
        Align = alLeft
        TabOrder = 4
        object lblMiscHeading: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 13
          Width = 162
          Height = 16
          Margins.Top = 12
          Align = alTop
          Alignment = taCenter
          Caption = 'Misc settings'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold, fsUnderline]
          ParentFont = False
          ExplicitWidth = 85
        end
        object lblinfo2: TLabel
          Left = 7
          Top = 58
          Width = 129
          Height = 13
          Caption = 'Use Global setting for now '
        end
        object Label2: TLabel
          Left = 8
          Top = 79
          Width = 152
          Height = 13
          Caption = 'Enter Pre code {BKSP} for Case'
        end
        object lbledtCardPreCode: TDBEdit
          Left = 8
          Top = 96
          Width = 121
          Height = 21
          DataField = 'CardPreString'
          DataSource = dsTagGroup
          TabOrder = 0
        end
        object chkUseModifier: TDBCheckBox
          Left = 8
          Top = 128
          Width = 140
          Height = 17
          Caption = 'Show Modifier '
          DataField = 'CardUseModifier'
          DataSource = dsTagGroup
          TabOrder = 1
        end
        object chkshowInactive: TDBCheckBox
          Left = 8
          Top = 35
          Width = 140
          Height = 17
          Caption = 'Show Inactive'
          DataField = 'CardShowInactive'
          DataSource = dsTagGroup
          TabOrder = 2
        end
      end
      object cxGrid1: TcxGrid
        AlignWithMargins = True
        Left = 599
        Top = 10
        Width = 330
        Height = 533
        Margins.Top = 10
        Align = alClient
        TabOrder = 5
        object TableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          FindPanel.DisplayMode = fpdmAlways
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = dsTagGroup
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          object TableView1MY_TAG: TcxGridDBColumn
            DataBinding.FieldName = 'MY_TAG'
          end
          object TableView1PARENT_TAG: TcxGridDBColumn
            DataBinding.FieldName = 'PARENT_TAG'
          end
          object TableView1desc: TcxGridDBColumn
            DataBinding.FieldName = 'desc'
          end
          object TableView1OUTPUT: TcxGridDBColumn
            DataBinding.FieldName = 'OUTPUT'
          end
          object TableView1INDX: TcxGridDBColumn
            DataBinding.FieldName = 'INDX'
          end
          object TableView1MORDER: TcxGridDBColumn
            DataBinding.FieldName = 'MORDER'
          end
          object TableView1CardColor: TcxGridDBColumn
            DataBinding.FieldName = 'CardColor'
          end
          object TableView1STATION_DEPT: TcxGridDBColumn
            DataBinding.FieldName = 'STATION_DEPT'
          end
          object TableView1BREADCRUMBS: TcxGridDBColumn
            DataBinding.FieldName = 'BREADCRUMBS'
          end
          object TableView1smldesc: TcxGridDBColumn
            DataBinding.FieldName = 'smldesc'
          end
          object TableView1InActive: TcxGridDBColumn
            DataBinding.FieldName = 'InActive'
          end
          object TableView1LineID: TcxGridDBColumn
            DataBinding.FieldName = 'LineID'
          end
          object TableView1CardFont: TcxGridDBColumn
            DataBinding.FieldName = 'CardFont'
          end
          object TableView1CardWidth: TcxGridDBColumn
            DataBinding.FieldName = 'CardWidth'
          end
          object TableView1CardDescLines: TcxGridDBColumn
            DataBinding.FieldName = 'CardDescLines'
          end
          object TableView1CardSortBy: TcxGridDBColumn
            DataBinding.FieldName = 'CardSortBy'
          end
          object TableView1CardAsGrid: TcxGridDBColumn
            DataBinding.FieldName = 'CardAsGrid'
          end
          object TableView1CardPicture: TcxGridDBColumn
            DataBinding.FieldName = 'CardPicture'
          end
          object TableView1CardShowPrice: TcxGridDBColumn
            DataBinding.FieldName = 'CardShowPrice'
          end
          object TableView1CardShowCode: TcxGridDBColumn
            DataBinding.FieldName = 'CardShowCode'
          end
          object TableView1CardShowInactive: TcxGridDBColumn
            DataBinding.FieldName = 'CardShowInactive'
          end
          object TableView1CardPreString: TcxGridDBColumn
            DataBinding.FieldName = 'CardPreString'
          end
          object TableView1CardUseModifier: TcxGridDBColumn
            DataBinding.FieldName = 'CardUseModifier'
          end
          object TableView1code: TcxGridDBColumn
            DataBinding.FieldName = 'code'
          end
          object TableView1price: TcxGridDBColumn
            DataBinding.FieldName = 'price'
          end
          object TableView1TAG_CNT: TcxGridDBColumn
            DataBinding.FieldName = 'TAG_CNT'
          end
          object TableView1DESC_CNT: TcxGridDBColumn
            DataBinding.FieldName = 'DESC_CNT'
          end
          object TableView1BOTH_CNT: TcxGridDBColumn
            DataBinding.FieldName = 'BOTH_CNT'
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = TableView1
        end
      end
      object DBNavigator1: TDBNavigator
        Left = 0
        Top = 546
        Width = 932
        Height = 25
        DataSource = dsTagGroup
        Align = alBottom
        TabOrder = 6
      end
    end
    object Panel1: TPanel
      Left = 4
      Top = 1
      Width = 375
      Height = 659
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 375
        Height = 111
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object btnUpdate: TButton
          Left = 12
          Top = 7
          Width = 114
          Height = 25
          Caption = 'Apply changes'
          TabOrder = 0
          OnClick = btnUpdateClick
        end
        object rg_NodeName: TRadioGroup
          Left = 0
          Top = 37
          Width = 375
          Height = 36
          Align = alBottom
          Caption = 'node name'
          Columns = 3
          ItemIndex = 1
          Items.Strings = (
            'Tag'
            'Caption'
            'Both')
          TabOrder = 1
          OnClick = rg_NodeNameClick
        end
        object btnDeleteNode: TButton
          Left = 253
          Top = 7
          Width = 114
          Height = 25
          Caption = 'Delete Selected Node'
          TabOrder = 2
          OnClick = btnDeleteNodeClick
        end
        object btnAddChild: TButton
          Left = 132
          Top = 7
          Width = 114
          Height = 25
          Caption = 'Create Child'
          TabOrder = 3
          OnClick = btnAddChildClick
        end
        object Panel9: TPanel
          Left = 0
          Top = 73
          Width = 375
          Height = 38
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 4
          object rg_colexpand: TRadioGroup
            Left = 0
            Top = 0
            Width = 253
            Height = 38
            Align = alLeft
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Expand'
              'Collapse')
            TabOrder = 0
            OnClick = rg_colexpandClick
          end
          object btn_DownNode: TBitBtn
            Left = 315
            Top = 6
            Width = 39
            Height = 29
            Caption = 'Down'
            TabOrder = 1
            OnClick = btn_DownNodeClick
          end
          object btn_UpNode: TBitBtn
            Left = 259
            Top = 6
            Width = 39
            Height = 29
            Caption = 'Up'
            TabOrder = 2
            OnClick = btn_UpNodeClick
          end
        end
      end
      object dxDBTreeView1: TdxDBTreeView
        Left = 0
        Top = 111
        Width = 375
        Height = 548
        RightClickSelect = True
        ShowNodeHint = False
        OnCustomDraw = dxDBTreeView1CustomDraw
        OnDragDropTreeNode = dxDBTreeView1DragDropTreeNode
        HotTrack = True
        RowSelect = True
        ToolTips = False
        DataSource = dsTagGroup
        DisplayField = 'MY_TAG'
        KeyField = 'MY_TAG'
        ListField = 'MORDER'
        ParentField = 'PARENT_TAG'
        RootValue = Null
        SeparatedSt = ' - '
        RaiseOnError = True
        ReadOnly = True
        DragMode = dmAutomatic
        Indent = 20
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        Options = [trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
        SelectedIndex = -1
        TabOrder = 1
        OnRefreshNode = dxDBTreeView1RefreshNode
        OnClick = dxDBTreeView1Click
        OnDragOver = dxDBTreeView1DragOver
        OnMouseDown = dxDBTreeView1MouseDown
        PopupMenu = PopupMenu1
        ParentFont = False
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 1312
    Height = 85
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 33
      Top = 7
      Width = 33
      Height = 16
      AutoSize = False
      Caption = 'Tag:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 33
      Top = 32
      Width = 59
      Height = 16
      AutoSize = False
      Caption = 'Station:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cb_searchCombo: TComboBox
      Left = 92
      Top = 5
      Width = 314
      Height = 21
      TabOrder = 0
      Text = 'select tag..'
    end
    object btn_search: TButton
      Left = 412
      Top = 3
      Width = 75
      Height = 25
      Caption = 'Find'
      TabOrder = 1
      OnClick = btn_searchClick
    end
    object Panel6: TPanel
      Left = 0
      Top = 47
      Width = 1312
      Height = 38
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 726
        Height = 38
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object lbl_breadcrumb: TLabel
          Left = 106
          Top = 12
          Width = 615
          Height = 16
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 6
          Top = 12
          Width = 99
          Height = 22
          AutoSize = False
          Caption = 'Bread Crumbs:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object btn_Clear: TButton
      Left = 493
      Top = 3
      Width = 75
      Height = 25
      Caption = 'Clear search'
      TabOrder = 3
      OnClick = btn_ClearClick
    end
    object cb_SearchStationDept: TComboBox
      Left = 92
      Top = 32
      Width = 221
      Height = 21
      AutoComplete = False
      TabOrder = 4
      Text = 'select station department...'
      OnClick = cb_SearchStationDeptClick
    end
    object btnAddStationDept: TButton
      Left = 319
      Top = 31
      Width = 87
      Height = 22
      Caption = 'Add Station ::'
      TabOrder = 5
      OnClick = btnAddStationDeptClick
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 840
    Top = 464
  end
  object dsTagGroup: TDataSource
    DataSet = DataModule1.q_TagGroup
    OnDataChange = dsTagGroupDataChange
    Left = 264
    Top = 96
  end
  object ds_search: TDataSource
    DataSet = DataModule1.q_search
    Left = 184
    Top = 96
  end
  object cardFontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Left = 840
    Top = 298
  end
  object PopupMenu1: TPopupMenu
    AutoPopup = False
    Left = 248
    Top = 336
    object mnuCreateParent: TMenuItem
      Caption = 'Create &root node'
      OnClick = mnuCreateParentClick
    end
    object mnuCreateChild: TMenuItem
      Caption = 'Create &child node'
      OnClick = mnuCreateChildClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object mnuCloneTreeByStationDept: TMenuItem
      Caption = 'Clone tree/selected branch'
      OnClick = mnuCloneTreeByStationDeptClick
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object DeleteSelectedNode1: TMenuItem
      Caption = '&Delete selected node'
      OnClick = DeleteSelectedNode1Click
    end
    object Makeselectednodearootnode1: TMenuItem
      Caption = '&Make selected  node a root node'
      OnClick = Makeselectednodearootnode1Click
    end
  end
  object dsTagDropDown: TDataSource
    DataSet = DataModule1.q_tagDropDown
    OnDataChange = dsTagGroupDataChange
    Left = 128
    Top = 336
  end
  object dlg_selectPicture: TOpenDialog
    Filter = 'Pictuers|*.bmp;*.png;*.jpg'
    Left = 840
    Top = 387
  end
end
