unit Utils;

interface

uses
  SysUtils, Windows, classes, controls, graphics, WinSock, WinInet;

function IsDLL: Boolean;
function GetFontInString(AFont: TFont): string;
procedure SetFontByString(AFont: TFont; strFont: string);

function GetDateByYYYYMMDD(const s: string): TDateTime;

procedure AddZeros(var s: string; intLength: Integer);

function Reverse(s: string): string;

function ReplicateChar(chFill: Char; intLen: Integer): string;
function Replicate(c: string; nLen: Integer): string;
function Space(Len: Integer): string;
function PadLSpace(strStr: string; intLen: Integer): string;
function PadRSpace(strStr: string; intLen: Integer): string;
function PadL(strStr: string; intLen: Integer; strFill: string = ' '): string;
function PadR(strStr: string; intLen: Integer; strFill: string = ' '): string;
function PadC(strStr: string; intLen: Integer; strFill: string = ' '): string;

function ChrTran(strStr, strKeyOld, strKeyNew: string): string;
function StrTran(In_String, Old_String, New_String: string): string;
{ function StrTran2(strStr, strOld, strNew: string; intFirstOccur, intQntOccurs:
  Integer): string; }
function Stuff(strStr: string; intIndex, intCount: Integer; strSub: string): string;
function Proper(strStr: string): string;
function EmptyMyStr(s: string): Boolean;
function LeftStr(s: string; Len: Integer): string;
function RightStr(s: string; Len: Integer): string;

function IsDigit(s: string; FirstChar: Boolean): Boolean;
function IsUpper(s: string; FirstChar: Boolean): Boolean;
function IsLower(s: string; FirstChar: Boolean): Boolean;
function IsAlfa(strStr: string; boolFirstChar: Boolean): Boolean;
function AddSingleQuotes(const Value: string): string;

{ AT - Case-sensitive search (like Pos) }
function AT(strSub, strStr: string; intFirstOccur: Integer): Integer;
function RAT(strSub, strStr: string; intFirstOccur: Integer): Integer;

{ ATC - Case-unsensitive search }
function ATC(strSub, strStr: string; intFirstOccur: Integer): Integer;
function RATC(strSub, strStr: string; intFirstOccur: Integer): Integer;

function Occurs(strSub, strStr: string): Integer;

function IIF(boolExpr: Boolean; var1, var2: Variant): Variant;
function IsDLLAvailable(const DLLName: string): Boolean;
function InList(varKey: Variant; varValues: array of Variant): Boolean;

function DToS(dateValue: TDateTime): string;
function CMonth(dateValue: TDateTime): string;
function CDOW(dateValue: TDateTime): string;
function Year(dateValue: TDateTime): Word;
function Month(dateValue: TDateTime): Byte;
function Day(dateValue: TDateTime): Byte;
function IsLeapYear(AYear: Integer): Boolean;
function DaysPerMonth(AYear, AMonth: Integer): Integer;
function GetCharFromVirtualKey(Key: Word): string;
function TempFileName(const Extention: string): string;
function GetSystemDIR: string;
function HDDSerial: Integer;
function GetEnvVar(const EnvVar: string): string;

procedure WriteStringToFile(const FileName, Value: string);
function ReadStringFromFile(const FileName: string): string;
function Mod10(const Value: string): Integer;

function GetFolderDialog(Handle: Integer; Caption: string; var strFolder: string): Boolean;

function GetOSComputerName: string;
function GetOSUserName: string;
function GetOSIPAddress: string;
function GetInetFile(const FileURL, FileName: String): Boolean;
procedure DeleteFiles(ADirectory, AFileMask: string; ADelSubDirs: Boolean);
function MakeValidIdent(const AText: string): string;
function ExtractParameter(AParam: string; var AValue: string): string;
function PostHttp(AFileName, AUrl: string): Boolean;
function PostHttpParams(AParamList: string): Boolean;
function FileTimeToLocalDateTime(AFileTime: TFileTime): TDateTime;

implementation

uses {$IF CompilerVersion >= 26.0}System.UITypes, {$IFEND} ShlObj, ShellAPI,
  Forms, IdBaseComponent,
  IdHTTP, IdMultipartFormData, IdGlobalProtocols;

function FileTimeToLocalDateTime(AFileTime: TFileTime): TDateTime;
var
  SystemTime, LocalTime: TSystemTime;
begin
  if not FileTimeToSystemTime(AFileTime, SystemTime) then
    RaiseLastOSError;
  if not SystemTimeToTzSpecificLocalTime(nil, SystemTime, LocalTime) then
    RaiseLastOSError;
  Result := SystemTimeToDateTime(LocalTime);
end;

function IsDLL: Boolean;
begin
  Result := ModuleIsLib and not ModuleIsPackage;
end;

function GetFontInString(AFont: TFont): string;
begin
  Result := AFont.Name + ',' + IntToStr(AFont.CharSet) + ',' + IntToStr(AFont.Color) + ',' + IntToStr(AFont.Size) + ','
    + IntToStr(Byte(AFont.Style));
end;

procedure SetFontByString(AFont: TFont; strFont: string);
// 'MS Sans Serif,8,2'
var
  i: Integer;
  s, Data: string;
begin
  if strFont = '' then
    exit;
  try
    i := Pos(',', s);
    if i > 0 then
    begin
      { Name }
      Data := Trim(Copy(s, 1, i - 1));
      if Data <> '' then
        AFont.Name := Data;
      Delete(s, 1, i);
      i := Pos(',', s);
      if i > 0 then
      begin
        { CharSet }
        Data := Trim(Copy(s, 1, i - 1));
        if Data <> '' then
          AFont.CharSet := TFontCharSet(StrToIntDef(Data, AFont.CharSet));
        Delete(s, 1, i);
        i := Pos(',', s);
        if i > 0 then
        begin
          { Color }
          Data := Trim(Copy(s, 1, i - 1));
          if Data <> '' then
            AFont.Color := TColor(StrToIntDef(Data, AFont.Color));
          Delete(s, 1, i);
          i := Pos(',', s);
          if i > 0 then
          begin
            { Size }
            Data := Trim(Copy(s, 1, i - 1));
            if Data <> '' then
              AFont.Size := StrToIntDef(Data, AFont.Size);
            Delete(s, 1, i);
            { Style }
            Data := Trim(s);
            if Data <> '' then
              AFont.Style := TFontStyles(Byte(StrToIntDef(Data, Byte(AFont.Style))));
          end
        end
      end
    end;
  except
  end;
end;

function GetDateByYYYYMMDD(const s: string): TDateTime;
begin
  Result := EncodeDate(StrToInt(Copy(s, 1, 4)), StrToInt(Copy(s, 5, 2)), StrToInt(Copy(s, 7, 2)));
end;

function GetORADate(dt: TDateTime): string;
var
  Year, Month, Day: Word;
begin
  DecodeDate(dt, Year, Month, Day);

  Result := 'TO_DATE(''' + IntToStr(Year) + PadL(IntToStr(Month), 2, '0') + PadL(IntToStr(Day), 2, '0') +
    ''', ''YYYYMMDD'')'
end;

procedure AddZeros(var s: string; intLength: Integer);
begin
  while (Length(s) < intLength) do
    s := s + '0';
end;

function GetDimListByExtInfo(const s: string): string;
var
  i, j: Integer;
begin
  i := Pos('!s!', s);
  j := Pos('!e!', s);
  if (j < 1) then
    j := Length(s);
  if (i > 0) then
    Result := Copy(s, i + 3, j - i - 3)
  else
    Result := '';
end;

function Reverse(s: string): string;
var
  i, L: Integer;
begin
  L := Length(s);
  SetLength(Result, L);
  for i := 1 to L do
  begin
    Result[L] := s[i];
    Dec(L);
  end;
end;

function ReplicateChar(chFill: Char; intLen: Integer): string;
begin
  SetLength(Result, intLen);
  FillChar(Result[1], intLen, chFill);
end;

function Replicate(c: string; nLen: Integer): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to nLen do
    Result := Result + c;
end;

function Space(Len: Integer): string;
begin
  Result := ReplicateChar(' ', Len);
end;

function PadLSpace(strStr: string; intLen: Integer): string;
begin
  FmtStr(Result, '%*s', [intLen, strStr]);
end;

function PadRSpace(strStr: string; intLen: Integer): string;
begin
  FmtStr(Result, '%-*.*s', [intLen, intLen, strStr]);
end;

function PadL(strStr: string; intLen: Integer; strFill: string = ' '): string;
begin
  Result := Copy(strStr, 1, intLen);
  Result := Replicate(strFill, intLen - Length(Result)) + Result;
end;

function PadR(strStr: string; intLen: Integer; strFill: string = ' '): string;
begin
  Result := Copy(strStr, 1, intLen);
  Result := Result + Replicate(strFill, intLen - Length(Result));
end;

function PadC(strStr: string; intLen: Integer; strFill: string = ' '): string;
var
  realLenHalfFill: Real;
begin
  Result := Copy(strStr, 1, intLen);
  realLenHalfFill := (intLen - Length(Result)) / 2 + 0.1;
  Result := Replicate(strFill, Trunc(realLenHalfFill)) + Result + Replicate(strFill, Round(realLenHalfFill));
end;

function ChrTran(strStr, strKeyOld, strKeyNew: string): string;
var
  intLenKeyOld: Integer;
  intLenKeyNew: Integer;
  intPosKey: Integer; { Current character position in source string in old-key }
  intIndexStr: Integer; { Current character index in source string }
  intIndexResult: Integer; { Current character index in result string }

begin
  Result := strStr;
  intLenKeyOld := Length(strKeyOld);
  intLenKeyNew := Length(strKeyNew);
  intIndexResult := 1;

  for intIndexStr := 1 to Length(strStr) do
  begin
    { Search a current character position in old-key }
    intPosKey := 1;
    while ((intPosKey <= intLenKeyOld) and (strStr[intIndexStr] <> strKeyOld[intPosKey])) do
      Inc(intPosKey);

    if (intPosKey <= intLenKeyOld) then { Character is find in old-key? }
      if (intPosKey <= intLenKeyNew) then { Exist analog of character in new-key? }
      begin { Exchange a character in result string }
        Result[intIndexResult] := strKeyNew[intPosKey];
        Inc(intIndexResult);
      end
      else { remove a character from result string }
        Delete(Result, intIndexResult, 1)
    else { skip a character without changes }
      Inc(intIndexResult);
  end
end;

function StrTran(In_String, Old_String, New_String: string): string;
var
  CurPos: Integer;
begin
  if (Old_String = New_String) or (Length(Old_String) = 0) then
    Result := Old_String
  else
  begin
    CurPos := 1;
    Result := '';
    while CurPos <= Length(In_String) do
    begin
      if Copy(In_String, CurPos, Length(Old_String)) = Old_String then
      begin
        Result := Result + New_String;
        CurPos := CurPos + Length(Old_String);
      end
      else
      begin
        Result := Result + Copy(In_String, CurPos, 1);
        CurPos := CurPos + 1;
      end;
    end;
  end;
end;

{ function StrTran2(strStr, strOld, strNew: string; intFirstOccur, intQntOccurs:
  Integer): string;
  var
  intLenOld: Integer;
  intLenDiffer: Integer; //Difference between lengths of strNew and strOld
  intPosOccur: Integer; //Position of current agree in source string
  intCntOccurs: Integer; //Counter for processed occurs
  begin
  Result := strStr;
  intLenOld := Length(strOld);
  intLenDiffer := Length(strNew) - intLenOld;
  intCntOccurs := 0;
  intPosOccur := 1;

  while (intPosOccur > 0) and (intCntOccurs < intQntOccurs) do
  begin
  intPosOccur := AT(strOld, strStr, intFirstOccur + intCntOccurs);
  if (intPosOccur > 0) then
  begin
  Result := Stuff(Result, intPosOccur + intCntOccurs * intLenDiffer,
  intLenOld, strNew);
  Inc(intCntOccurs);
  end;
  end;
  end; }

function Stuff(strStr: string; intIndex, intCount: Integer; strSub: string): string;
begin
  Result := strStr;
  Delete(Result, intIndex, intCount);
  Insert(strSub, Result, intIndex);
end;

function Proper(strStr: string): string;
var
  IsStartWord: Boolean;
  i: Integer;
  ch: Char;
begin
  IsStartWord := True;
  Result := '';
  if (Length(strStr) > 0) then
    for i := 1 to Length(strStr) do
    begin
      if IsStartWord then
        ch := AnsiUpperCase(strStr[i])[1]
      else
        ch := AnsiLowerCase(strStr[i])[1];
      IsStartWord := (ch = ' ');
      Result := Result + ch;
    end;
end;
{ TODO 5 -oSB EF : There is already a constant called EmptyStr in SysUtils }

function EmptyMyStr(s: string): Boolean;
begin
  Result := ((Length(s) = 0) or (Length(Trim(s)) = 0));
end;

function LeftStr(s: string; Len: Integer): string;
begin
  Result := Copy(s, 1, Len);
end;

function RightStr(s: string; Len: Integer): string;
begin
  Result := Copy(s, Succ(Length(s) - Len), Len);
end;

function IsDigit(s: string; FirstChar: Boolean): Boolean;
var
  i, Len: Integer;
  boolWasNotDecimal: Boolean;
  ADecimalSeparator: Char;
begin
  if FirstChar then
    Result := (Length(s) > 0) and (AnsiChar(s[1]) in ['0' .. '9'])
  else
  begin
    s := Trim(s);
    Len := Length(s);
    boolWasNotDecimal := True;
    i := 1;
{$IF CompilerVersion >= 26.0}
    ADecimalSeparator := FormatSettings.DecimalSeparator;
{$ELSE}
    ADecimalSeparator := DecimalSeparator;
{$IFEND}
    while ((i <= Len) and ((AnsiChar(s[i]) in ['0' .. '9']) or ((AnsiChar(s[i]) in ['-', '+']) and (i = 1)) or
      ((Char(s[i]) = ADecimalSeparator) and boolWasNotDecimal))) do
    begin
      if (s[i] = ADecimalSeparator) then
        boolWasNotDecimal := False;
      Inc(i);
    end;

    Result := (i > Len);
  end;
end;

function IsUpper(s: string; FirstChar: Boolean): Boolean;
begin
  if FirstChar then
    Result := ((Length(s) > 0) and (s[1] = AnsiUpperCase(s[1])))
  else
    Result := ((Length(s) > 0) and (s = AnsiUpperCase(s)));
end;

function IsLower(s: string; FirstChar: Boolean): Boolean;
begin
  if FirstChar then
    Result := ((Length(s) > 0) and (s[1] = AnsiLowerCase(s[1])))
  else
    Result := ((Length(s) > 0) and (s = AnsiLowerCase(s)));
end;

function IsAlfa(strStr: string; boolFirstChar: Boolean): Boolean;
type
  TAlfaSet = set of AnsiChar;
const
  AlfaSet: TAlfaSet = ['a' .. 'z', 'A' .. 'Z', '�' .. '�', '�' .. '�'];
var
  intIndex, intLen: Integer;

begin
  intLen := Length(strStr);
  if boolFirstChar then
    Result := (intLen > 0) and (AnsiChar(strStr[1]) in AlfaSet)
  else
  begin
    intIndex := 1;
    while (intIndex <= intLen) and (AnsiChar(strStr[intIndex]) in AlfaSet) do
      Inc(intIndex);
    Result := (intIndex > intLen) and (intLen > 0);
  end;
end;

function AddSingleQuotes(const Value: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(Value) do
    if Value[i] = '''' then
      Result := Result + ''''''
    else
      Result := Result + Value[i]
end;

function AT(strSub, strStr: string; intFirstOccur: Integer): Integer;
var
  intLenStr: Integer;
  intLenSub: Integer;
  intCntOccurs: Integer; { Counter for processed occurs }
  intPosInStr: Integer;
  intPosInRest: Integer;

begin
  Result := 0;
  intLenStr := Length(strStr);
  intLenSub := Length(strSub);
  intCntOccurs := 0;
  intPosInRest := 1;
  intPosInStr := 1;

  while (intPosInRest > 0) and (intCntOccurs < intFirstOccur) do
  begin
    intPosInRest := Pos(strSub, Copy(strStr, intPosInStr, intLenStr));
    if intPosInRest > 0 then
    begin
      Inc(intPosInStr, Pred(intPosInRest));
      Inc(intCntOccurs);
      if intCntOccurs = intFirstOccur then
        Result := intPosInStr
      else
        Inc(intPosInStr, intLenSub);
    end;
  end;
end;

function RAT(strSub, strStr: string; intFirstOccur: Integer): Integer;
begin
  Result := AT(Reverse(strSub), Reverse(strStr), intFirstOccur);
  if Result > 0 then
    Result := Length(strStr) - Result - Length(strSub) + 2;
end;

function ATC(strSub, strStr: string; intFirstOccur: Integer): Integer;
begin
  Result := AT(AnsiUpperCase(strSub), AnsiUpperCase(strStr), intFirstOccur);
end;

function RATC(strSub, strStr: string; intFirstOccur: Integer): Integer;
begin
  Result := RAT(AnsiUpperCase(strSub), AnsiUpperCase(strStr), intFirstOccur);
end;

function Occurs(strSub, strStr: string): Integer;
var
  intLenSub, intLenStr: Integer;
  intPos: Integer;
begin
  Result := 0;
  intLenSub := Length(strSub);
  intLenStr := Length(strStr);
  intPos := 1;
  while (intPos > 0) do
  begin
    strStr := Copy(strStr, intPos, intLenStr);
    intPos := Pos(strSub, strStr);
    if intPos > 0 then
    begin
      Inc(intPos, intLenSub);
      Inc(Result);
    end;
  end;
end;

function IIF(boolExpr: Boolean; var1, var2: Variant): Variant;
begin
  if (boolExpr) then
    Result := var1
  else
    Result := var2;
end;

function InList(varKey: Variant; varValues: array of Variant): Boolean;
var
  intHigh: Integer;
  intIndex: Integer;
begin
  intHigh := High(varValues);
  intIndex := Low(varValues);
  while ((intIndex <= intHigh) and (varKey <> varValues[intIndex])) do
    Inc(intIndex);
  Result := (intIndex <= intHigh);
end;

function DToS(dateValue: TDateTime): string;
begin
  Result := FormatDateTime('yyyymmdd', dateValue);
end;

function CMonth(dateValue: TDateTime): string;
begin
  Result := FormatDateTime('mmmm', dateValue);
end;

function CDOW(dateValue: TDateTime): string;
begin
  Result := FormatDateTime('dddd', dateValue);
end;

function Year(dateValue: TDateTime): Word;
begin
  Result := StrToInt(FormatDateTime('yyyy', dateValue));
end;

function Month(dateValue: TDateTime): Byte;
begin
  Result := StrToInt(FormatDateTime('m', dateValue));
end;

function Day(dateValue: TDateTime): Byte;
begin
  Result := StrToInt(FormatDateTime('d', dateValue));
end;

function IsLeapYear(AYear: Integer): Boolean;
begin
  Result := (AYear mod 4 = 0) and ((AYear mod 100 <> 0) or (AYear mod 400 = 0));
end;

function DaysPerMonth(AYear, AMonth: Integer): Integer;
const
  DaysInMonth: array [1 .. 12] of Integer = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
begin
  Result := DaysInMonth[AMonth];
  if (AMonth = 2) and IsLeapYear(AYear) then
    Inc(Result);
end;

function GetCharFromVirtualKey(Key: Word): string;
var
  keyboardState: TKeyboardState;
  asciiResult: Integer;
begin
  GetKeyboardState(keyboardState);

  SetLength(Result, 2);
  asciiResult := ToAscii(Key, MapVirtualKey(Key, 0), keyboardState, @Result[1], 0);
  case asciiResult of
    0:
      Result := '';
    1:
      SetLength(Result, 1);
    2:
      ;
  else
    Result := '';
  end;
end;

function GetTempFile(const Extension: string): string;
var
  Buffer: array [0 .. MAX_PATH] of Char;
begin
  repeat
    GetTempPath(SizeOf(Buffer) - 1, Buffer);
    GetTempFileName(Buffer, 'tmp', 1, Buffer);
    Result := ChangeFileExt(Buffer, Extension);
  until not FileExists(Result);
end;

function TempFileName(const Extention: string): string;
var
  TempPath, TempFileName: array [0 .. MAX_PATH] of Char;
begin
  GetTempPath(MAX_PATH, TempPath);
  GetTempFileName(TempPath, 'tmp', 1, TempFileName);
  Result := ChangeFileExt(TempFileName, '.' + Extention);
  { begin
    Result := GetTempFile(Extention); }
end;

procedure DeleteFiles(ADirectory, AFileMask: string; ADelSubDirs: Boolean);
var
  ASourceLst: string;
  FO: TSHFileOpStruct;
begin
  If Not DirectoryExists(ADirectory) Then
    exit;

  FillChar(FO, SizeOf(FO), 0);
  FO.Wnd := Application.MainForm.Handle;
  FO.wFunc := FO_DELETE;
  ASourceLst := ADirectory + '\' + AFileMask + #0;
  FO.pFrom := PChar(ASourceLst);
  if not ADelSubDirs then
    FO.fFlags := FO.fFlags OR FOF_FILESONLY;
  // Remove the next line if you want a confirmation dialog box
  FO.fFlags := FO.fFlags OR FOF_NOCONFIRMATION;
  // Add the next line for a "silent operation" (no progress box)
  FO.fFlags := FO.fFlags OR FOF_SILENT;
  SHFileOperation(FO);
end;

// get HDD Seial Number

function HDDSerial(): Integer;
var
  SerialNum: pdword;
  a, b: dword;
  Buffer: array [0 .. 255] of Char;
begin
  Result := 0;
  SerialNum := nil;
  if GetVolumeInformation('c:\', Buffer, SizeOf(Buffer), SerialNum, a, b, nil, 0) then
    Result := SerialNum^;
end;

function GetEnvVar(const EnvVar: string): string;
var
  bytesNeeded: dword;
begin
  bytesNeeded := GetEnvironmentVariable(PChar(EnvVar), nil, 0);
  if bytesNeeded > 0 then
  begin
    SetLength(Result, bytesNeeded - 1);
    GetEnvironmentVariable(PChar(EnvVar), PChar(Result), bytesNeeded);
  end
  else
    Result := '';
end;

function IsDLLAvailable(const DLLName: string): Boolean;
var
  DLLVar: THandle;
begin
  Result := False;
  DLLVar := LoadLibrary(PChar(DLLName));
  if DLLVar <> 0 then
  begin
    Result := True;
    FreeLibrary(DLLVar);
  end;
end;

function GetSystemDIR: string;
var
  arrTemp: array [0 .. MAX_PATH + 1] of Char;
begin
  Result := '';
  if (GetSystemDirectory(arrTemp, SizeOf(arrTemp)) > 0) then
  begin
    if (Copy(arrTemp, Length(arrTemp), 1) <> '\') then
      StrCat(arrTemp, '\');
    Result := arrTemp;
  end;
end;

var
  CS: TRTLCriticalSection;

procedure WriteStringToFile(const FileName, Value: string);
{$IFDEF UNICODE}
var
  Writer: TStreamWriter;
begin
  if not FileExists(FileName) then
    Writer := TStreamWriter.Create(FileName, False, TEncoding.UTF8)
  else
    Writer := TStreamWriter.Create(FileName, True, TEncoding.UTF8);
  Writer.WriteLine(Value);
  Writer.Free();
{$ELSE}
var
  Stream: TFileStream;
begin
  EnterCriticalSection(CS);
  try
    if not FileExists(FileName) then
      Stream := TFileStream.Create(FileName, fmCreate or fmShareDenyWrite)
    else
      Stream := TFileStream.Create(FileName, fmOpenReadWrite or fmShareDenyWrite);
    with Stream do
      try
        Seek(0, soFromEnd);
        Stream.Write(Value[1], Length(Value));
      finally
        Free
      end;
  finally
    LeaveCriticalSection(CS);
  end;
{$ENDIF}
end;

function ReadStringFromFile(const FileName: string): string;
var
{$IFDEF UNICODE}
  Reader: TStreamReader;
{$ELSE}
  Stream: TFileStream;
  iSize: Integer;
  Value: string;
{$ENDIF}
begin
  Result := '';
  if FileExists(FileName) then
  begin
{$IFDEF UNICODE}
    { Create a new stream writer directly. }
    Reader := TStreamReader.Create(FileName, TEncoding.UTF8);
    Result := Reader.ReadToEnd();
    Reader.Free();
{$ELSE}
    EnterCriticalSection(CS);
    try
      Stream := TFileStream.Create(FileName, fmOpenRead);
      try
        iSize := Stream.Size - Stream.Position;
        SetString(Value, nil, iSize);
        Stream.Read(Pointer(Value)^, iSize);
      finally
        FreeAndNil(Stream);
      end;
      Result := Value;
    finally
      LeaveCriticalSection(CS);
    end;
{$ENDIF}
  end;
end;

function Mod10(const Value: string): Integer;
var
  i, intOdd, intEven: Integer;
begin
  { add all odd seq numbers }
  intOdd := 0;
  i := 1;
  while (i < Length(Value)) do
  begin
    Inc(intOdd, StrToIntDef(Value[i], 0));
    Inc(i, 2);
  end;

  { add all even seq numbers }
  intEven := 0;
  i := 2;
  while (i < Length(Value)) do
  begin
    Inc(intEven, StrToIntDef(Value[i], 0));
    Inc(i, 2);
  end;

  Result := 3 * intOdd + intEven;
  { modulus by 10 to get }
  Result := Result mod 10;
  if Result <> 0 then
    Result := 10 - Result
end;

function BrowseCallbackProc(hwnd: hwnd; uMsg: UINT; lParam: lParam; lpData: lParam): Integer; stdcall;
begin
  if (uMsg = BFFM_INITIALIZED) then
    SendMessage(hwnd, BFFM_SETSELECTION, 1, lpData);
  BrowseCallbackProc := 0;
end;

function GetFolderDialog(Handle: Integer; Caption: string; var strFolder: string): Boolean;
var
  BrowseInfo: TBrowseInfo;
  ItemIDList: PItemIDList;
  JtemIDList: PItemIDList;
{$IFDEF VER150} Path: PAnsiChar; {$ENDIF}
{$IFDEF VER210} Path: PWideChar; {$ENDIF}
{$IF CompilerVersion >= 26.0} Path: PWideChar; {$IFEND}
begin
  Result := False;
  Path := StrAlloc(MAX_PATH);
  SHGetSpecialFolderLocation(Handle, CSIDL_DRIVES, JtemIDList);
  with BrowseInfo do
  begin
    hwndOwner := GetActiveWindow;
    pidlRoot := JtemIDList;
    SHGetSpecialFolderLocation(hwndOwner, CSIDL_DRIVES, JtemIDList);

    { return display name of item selected }
    pszDisplayName := StrAlloc(MAX_PATH);

    { set the title of dialog }
    lpszTitle := PChar(Caption); // 'Select the folder';
    { flags that control the return stuff }
    ulFlags := BIF_RETURNONLYFSDIRS;
    lpfn := @BrowseCallbackProc;
    { extra info that's passed back in callbacks }
    lParam := LongInt(PChar(strFolder));
  end;

  ItemIDList := SHBrowseForFolder(BrowseInfo);

  if (ItemIDList <> nil) then
    if SHGetPathFromIDList(ItemIDList, Path) then
    begin
      strFolder := Path;
      Result := True
    end;
end;

function GetOSComputerName: string;
var
  pc: PChar;
  arrSize: dword;
begin
  arrSize := 1024;
  pc := AllocMem(arrSize);
  GetComputerName(pc, arrSize);

  Result := StrPas(pc);
  FreeMem(pc);
end;

function GetOSUserName: string;
var
  arrSize: dword;
  pc: PChar;
begin
  arrSize := 1024;
  pc := AllocMem(arrSize);
  GetUserName(pc, arrSize);

  Result := StrPas(pc);
  FreeMem(pc);
end;

function GetOSIPAddress: string;
var
  wVersionRequested: Word;
  wsaData: TWSAData;
  p: PHostEnt;
  s: array [0 .. 128] of Char;
  p2: PAnsiChar;
begin
  wVersionRequested := MAKEWORD(1, 1);
  WSAStartup(wVersionRequested, wsaData);
  GetHostName(@s, 128);
  p := GetHostByName(@s);
  p2 := iNet_ntoa(PInAddr(p^.h_addr_list^)^);
  { This is the hostname:=Format('%s', [p^.h_Name]); }
  Result := Format('%s', [p2]);
  WSACleanup;
end;

function GetInetFile(const FileURL, FileName: String): Boolean;
const
  BufferSize = 1024;
var
  hURL: HInternet;
  hSession: HInternet;
  Buffer: array [1 .. BufferSize] of Byte;
  BufferLen: dword;
  f: File;
  sAppName: string;
begin
  sAppName := 'GetPublicIpAddress'; // ExtractFileName(Application.ExeName);
  hSession := InternetOpen(PChar(sAppName), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  try
    hURL := InternetOpenURL(hSession, PChar(FileURL), nil, 0, 0, 0);
    try
      AssignFile(f, FileName);
      Rewrite(f, 1);
      repeat
        InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen);
        BlockWrite(f, Buffer, BufferLen)
      until BufferLen = 0;
      CloseFile(f);
      Result := True;
    finally
      InternetCloseHandle(hURL)
    end
  finally
    InternetCloseHandle(hSession)
  end;
end;

function MakeValidIdent(const AText: string): string;
const
  Alpha = ['A' .. 'Z', 'a' .. 'z', '_'];
  AlphaNumeric = Alpha + ['0' .. '9'];

  function IsValidChar(AIndex: Integer; AChar: Char): Boolean;
  begin
    if AIndex = 1 then
      Result := AChar in Alpha
    else
      Result := AChar in AlphaNumeric;
  end;

var
  i: Integer;
begin
  Result := AText;
  for i := 1 to Length(Result) do
    if not IsValidChar(i, Result[i]) then
      Result[i] := '_';
end;

function ExtractParameter(AParam: string; var AValue: string): string;
var
  i: Integer;
  ParamName: string;
begin
  i := Pos(':', AParam);
  if i > 0 then
  begin
    ParamName := Copy(AParam, 1, i - 1);
    AValue := Copy(AParam, i + 1, 1000);
  end
  else
  begin
    ParamName := AParam;
    AValue := '';
  end;

  Result := ParamName;
end;

function PostHttp(AFileName, AUrl: string): Boolean;
var
  IdHTTP: TIdHTTP;
  AStream: TIdMultipartFormDataStream;
begin
  Result := False;
  IdHTTP := TIdHTTP.Create(nil);
  AStream := TIdMultipartFormDataStream.Create;
  try
    AStream.AddFile(AFileName, AFileName, GetMIMETypeFromFile(AFileName));
    IdHTTP.Post(AUrl, AStream);
    Result := True;
  finally
    AStream.Free;
    IdHTTP.Free;
  end;
  Sleep(3000);
end;

function GetParamStrMy(p: PChar; var Param: string): PChar;
var
  i, Len: Integer;
  Start, s: PChar;
begin
  // U-OK
  while True do
  begin
    while (p[0] <> #0) and (p[0] <= ' ') do
      Inc(p);
    if (p[0] = '"') and (p[1] = '"') then
      Inc(p, 2)
    else
      Break;
  end;
  Len := 0;
  Start := p;
  while p[0] > ' ' do
  begin
    if p[0] = '"' then
    begin
      Inc(p);
      while (p[0] <> #0) and (p[0] <> '"') do
      begin
        Inc(Len);
        Inc(p);
      end;
      if p[0] <> #0 then
        Inc(p);
    end
    else
    begin
      Inc(Len);
      Inc(p);
    end;
  end;

  SetLength(Param, Len);

  p := Start;
  s := Pointer(Param);
  i := 0;
  while p[0] > ' ' do
  begin
    if p[0] = '"' then
    begin
      Inc(p);
      while (p[0] <> #0) and (p[0] <> '"') do
      begin
        s[i] := p^;
        Inc(p);
        Inc(i);
      end;
      if p[0] <> #0 then
        Inc(p);
    end
    else
    begin
      s[i] := p^;
      Inc(p);
      Inc(i);
    end;
  end;

  Result := p;
end;

function ParamStrMy(AAllParam: string; Index: Integer): string;
var
  p: PChar;
begin
  p := PChar(AAllParam);
  while True do
  begin
    p := GetParamStrMy(p, Result);
    if (Index = -1) or (Result = '') then
      Break;
    Dec(Index);
  end;
end;

function PostHttpParams(AParamList: string): Boolean;
var
  AFileName, AUrl, AParamValue, AParamFull: string;
  AIndex: Integer;
begin
  AFileName := '';
  AUrl := '';
  AIndex := 0;
  repeat
    AParamFull := ParamStrMy(AParamList, AIndex);
    if AParamFull <> '' then
    begin
      AParamFull := ExtractParameter(AParamFull, AParamValue);
      if SameText(AParamFull, '-file') then
        AFileName := AParamValue
      else if SameText(AParamFull, '-target') then
        AUrl := AParamValue;
    end;
    Inc(AIndex);
  until AParamFull = '';
  if (AFileName <> '') and (AUrl <> '') then
    Result := PostHttp(AFileName, AUrl)
  else
    Result := False;
end;

initialization

{ initialize my Critical section. }
InitializeCriticalSection(CS);

finalization

{ finalize my Critical section. }
DeleteCriticalSection(CS);

end.
