unit uNewformGroupSetup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, cxTextEdit, Vcl.StdCtrls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  dxTokenEdit, Vcl.Samples.Spin, cxMaskEdit, cxDropDownEdit, cxColorComboBox, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, Vcl.ExtCtrls, Utils, math, dximctrl,
  cxImageComboBox, LMDControl, LMDCustomControl, LMDCustomPanel, LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit,
  LMDCustomBrowseEdit, LMDCustomFileEdit, LMDFileOpenEdit, Vcl.DBCtrls, Vcl.Buttons, dxDateRanges, Vcl.ComCtrls, dxtree,
  dxdbtree, cxTL, cxTLdxBarBuiltInMenu, cxInplaceContainer, cxTLData, cxDBTL, Vcl.AppEvnts,
  dxSkinsDefaultPainters
  , dxdbtrel, Vcl.Menus, Vcl.Mask, System.Generics.Collections,
  uFormAddStationDept, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkroom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;
Const

   DEFAULT_TREE_TEXT =  'DESC_CNT';



 type
  TmyColrList = record
    Name: string;
    hex500: string;
    hexA100: string;
  end;

  TTreeDataPtr = ^TTreeData;

  TTreeData = Record
    INDX  : integer;
    MORDER : String ;
    MY_TAG: String;
    PARENT_TAG: String;
    DESC: String;
    OUTPUT: String;
    CARD_COLOR: integer;
    STATION_DEPT: String;
    BREADCRUMBS: String;
    SMLDESC : String ;
    INACTIVE : Boolean ;
    LINEID : String ;
    CARD_FONT : String ;
    CARD_WIDTH : Integer ;
    CARD_DESC_LINES : Integer ;
    CARD_SORT_BY : String ;
    CARD_AS_GRID : Boolean ;
    CARD_PICTURE : String;
    CARD_SHOW_PRICE : Boolean ;
    CARD_SHOW_CODE : Boolean ;
    CARD_SHOW_INACTIVE : Boolean ;
    CARD_PRESTRING : String;
    CARD_USE_MODIFIER : Boolean ;
    CODE              : String ;
    Price             : Real   ;

  End;


 TSearchData = class(TObject)
    MY_TAG: String;
    PARENT_TAG: String;
    DESC: String;
  End;

  TNewfrmGroupSetup = class(TForm)
    TableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    pnlCard: TPanel;
    btnCardFont: TLMDSpeedButton;
    btnCardSample: TLMDSpeedButton;
    lblCard: TLabel;
    chkCardCode: TCheckBox;
    chkCardPrice: TCheckBox;
    cxColorComboBoxCard: TcxColorComboBox;
    pnlButton: TPanel;
    btnCardfont01: TLMDSpeedButton;
    btnSample: TLMDSpeedButton;
    lblButton: TLabel;
    btnUpdate: TButton;
    pnlMisc: TPanel;
    lblMiscHeading: TLabel;
    lblinfo2: TLabel;
    chkshowInactive: TCheckBox;
    lbledtCardPreCode: TLabeledEdit;
    chkUseModifier: TCheckBox;
    pnlSortby: TPanel;
    lblSortby: TLabel;
    icbSort: TcxImageComboBox;
    lbledtPicture: TLMDLabeledFileOpenEdit;
    pnlTagfilter: TPanel;
    SourceTag: TLabel;
    pnlBotm: TPanel;
    edtTestfilter: TEdit;
    lblfilter: TLabel;
    btnRefresh: TSpeedButton;
    btnSibling: TButton;
    btnParent: TButton;
    pnlDescrip: TPanel;
    lblShowsup: TLabel;
    pnlType: TPanel;
    lblType: TLabel;
    icbTable: TcxImageComboBox;
    lblCaption: TLabel;
    lblCardDescLines: TLabel;
    seDescLines: TSpinEdit;
    seCardWidth: TSpinEdit;
    lblCardWidth: TLabel;
    ApplicationEvents1: TApplicationEvents;
    lblParenValue: TLabel;
    lbMyTag: TLabel;
    lblMyTagValue: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Splitter2: TSplitter;
    dsTagGroup: TDataSource;
    dxDBTreeView1: TdxDBTreeView;
    Label1: TLabel;
    cb_searchCombo: TComboBox;
    btn_search: TButton;
    ds_search: TDataSource;
    Panel6: TPanel;
    Panel7: TPanel;
    lbl_breadcrumb: TLabel;
    cardFontDialog: TFontDialog;
    pnlMyTag: TPanel;
    lblSelfTag: TLabel;
    edtCardFont: TEdit;
    rg_colexpand: TRadioGroup;
    PopupMenu1: TPopupMenu;
    mnuCreateChild: TMenuItem;
    rg_NodeName: TRadioGroup;
    lbledtCaption: TDBEdit;
    edtTagList: TDBEdit;
    lbledtTagFilter: TDBEdit;
    btn_Clear: TButton;
    btnDeleteNode: TButton;
    btnAddChild: TButton;
    Panel8: TPanel;
    Label3: TLabel;
    btnAddStationDept: TButton;
    cb_SearchStationDept: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    mnuCreateParent: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    DeleteSelectedNode1: TMenuItem;
    icbStationDept: TcxImageComboBox;
    Panel9: TPanel;
    btn_UpNode: TBitBtn;
    btn_DownNode: TBitBtn;

    procedure cxColorComboBoxCardPropertiesChange(Sender: TObject);
    procedure btnUpdateClick(Sender: TObject);
    procedure dsTagGroupDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure seCardWidthChange(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure dxDBTreeView1Click(Sender: TObject);
    procedure btnSiblingClick(Sender: TObject);
    procedure btnParentClick(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure rg_colexpandClick(Sender: TObject);
    procedure dxDBTreeView1RefreshNode(Sender: TObject;
      DBTreeNode: TdxDBTreeNode);
    procedure dxDBTreeView1CustomDraw(Sender: TObject; TreeNode: TTreeNode;
      AFont: TFont; var AColor, ABkColor: TColor);
    procedure btnCardFontClick(Sender: TObject);
    procedure dxDBTreeView1DragDropTreeNode(Destination, Source: TTreeNode;
      var Accept: Boolean);
    procedure dxDBTreeView1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure mnuCreateChildClick(Sender: TObject);
    procedure rg_NodeNameClick(Sender: TObject);
    procedure btn_searchClick(Sender: TObject);
    procedure btn_ClearClick(Sender: TObject);    procedure q_TagGroupAfterScroll(DataSet: TDataSet);    procedure FormClose(Sender: TObject; var Action: TCloseAction);    procedure btnAddChildClick(Sender: TObject);    procedure btnDeleteNodeClick(Sender: TObject);    procedure btnAddStationDeptClick(Sender: TObject);    procedure mnuCreateParentClick(Sender: TObject);    procedure DeleteSelectedNode1Click(Sender: TObject);
    procedure cb_SearchStationDeptClick(Sender: TObject);
    procedure btn_UpNodeClick(Sender: TObject);
    procedure btn_DownNodeClick(Sender: TObject);

  private

    StationDept_List : TDictionary<String, String>  ;


 

    function getNodeColor(nodeColor : integer; parentKey : String) : integer;
    function getBreadCrumnbs(pStationDept, pBreadCrumbs : String) : String;
    procedure colrfil;
    procedure FilterByTag(sFilter,Sfield: string);
    function  GuidIdString: string;
    procedure prepareTree(displayField : String = DEFAULT_TREE_TEXT) ;
    procedure populateCombos;
    procedure fixTreeNodeType(pParentTag : String );
    function  readTagRow(dataset : TDataSet) : TTreeDataPtr;
    procedure displayTagProperties(ADataSet: TDataSet);

    { Private declarations }
  public

    { Public declarations }


  end;

var
  NewfrmGroupSetup: TNewfrmGroupSetup;
  FGuidLastTimePart: Integer ;


const
  SWindowClassName = 'XBrowse2RegisterHelperApp';
  clrList: array [1 .. 21] of TmyColrList = ((name: 'Red'; hex500: '$F44336'; hexA100: '$FF8A80'), (name: 'Pink';
    hex500: '$E91E63'; hexA100: '$FF80AB'), (name: 'Purple'; hex500: '$9C27B0'; hexA100: '$EA80FC'),
    (name: 'Deep Purple'; hex500: '$673AB7'; hexA100: '$B388FF'), (name: 'Indigo'; hex500: '$3F51B5';
    hexA100: '$8C9EFF'), (name: 'Blue'; hex500: '$2196F3'; hexA100: '$82B1FF'), (name: 'Light Blue'; hex500: '$03A9F4';
    hexA100: '$80D8FF'), (name: 'Cyan'; hex500: '$00BCD4'; hexA100: '$84FFFF'), (name: 'Teal'; hex500: '$009688';
    hexA100: '$A7FFEB'), (name: 'Green'; hex500: '$4CAF50'; hexA100: '$B9F6CA'), (name: 'Light Green';
    hex500: '$8BC34A'; hexA100: '$CCFF90'), (name: 'Lime'; hex500: '$CDDC39'; hexA100: '$F4FF81'), (name: 'Yellow';
    hex500: '$FFEB3B'; hexA100: '$FFFF8D'), (name: 'Amber'; hex500: '$FFC107'; hexA100: '$FFE57F'), (name: 'Orange';
    hex500: '$FF9800'; hexA100: '$FFD180'), (name: 'Deep Orange'; hex500: '$FF5722'; hexA100: '$FF9E80'),
    (name: 'Brown'; hex500: '$795548'; hexA100: '$A1887F'), (name: 'Grey'; hex500: '$9E9E9E'; hexA100: '$BDBDBD'),
    (name: 'Blue Grey'; hex500: '$607D8B'; hexA100: '$CFD8DC'), (name: 'Black'; hex500: '$000000'; hexA100: '$000000'),
    (name: 'White'; hex500: '$FFFFFF'; hexA100: '$FFFFFF'));

implementation

//uses PrdData, MainBrowse;


uses dmData;
{$R *.dfm}


 


function TNewfrmGroupSetup.getNodeColor(nodeColor : integer; parentKey : String) : integer;
begin
    if (nodeColor = 0) and (parentKey <> '') then
       Result := DataModule1.getParentColor(parentKey)
    else
       Result := nodeColor;

end;


function TNewfrmGroupSetup.getBreadCrumnbs(pStationDept, pBreadCrumbs : String) : String;
begin
    if pStationDept <> '' then
       Result := pBreadCrumbs.Replace(pStationDept + '|' ,'', [] )
    else
       Result := pBreadCrumbs ;
end;


procedure TNewfrmGroupSetup.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
var s: string;
begin
 // btnTagsetup.Enabled := (icbTable.ItemIndex=1) and (lbledtTagFilter.Text >'')  ;
  s:= IIF(icbTable.ItemIndex=1,'My Tag - Parent of Sub-Menu', 'My Tag - Use same TAG as Items to display') ;
  lblSelfTag.Caption := s ;
  chkCardCode.Visible :=   (icbTable.ItemIndex=0) ;
  chkCardPrice.Visible :=  (icbTable.ItemIndex=0) ;
  //chkCardAsGrid.Visible := (icbTable.ItemIndex=0) ;
  //pnlGrid.Visible :=        (icbTable.ItemIndex=0) ;
  //pnlCard.Visible := not    chkCardAsGrid.Checked ;

end;

procedure TNewfrmGroupSetup.btnAddChildClick(Sender: TObject);
begin
  mnuCreateChildClick(self);
end;

procedure TNewfrmGroupSetup.btnAddStationDeptClick(Sender: TObject);
var
  pCurrent_Station : string ;
  Item: TcxImageComboBoxItem;
begin
   if not Assigned(frm_AddStatioDept) then
          frm_AddStatioDept := Tfrm_AddStatioDept.Create(self);
   if frm_AddStatioDept.ShowModal = mrOK then
   begin
        pCurrent_Station := UpperCase(frm_AddStatioDept.edt_stationdept.Text) ;
        // add Item to Station Combo
        Item  :=  icbStationDept.Properties.Items.Add ;

        Item.Value := pCurrent_Station ;

        Item.Description := pCurrent_Station;

        Item.ImageIndex := -1;

        // add new Item to  StationDept_List
        StationDept_List.Add(pCurrent_Station,
                             pCurrent_Station);


        // station search combo
        cb_SearchStationDept.Items.Add(pCurrent_Station);
        //set currently added Station  in cb_SearchStationDept
        cb_SearchStationDept.ItemIndex := cb_SearchStationDept.Items.Count -1 ;

        // do search for the newly added station
        btn_searchClick(self);
   end;

end;

procedure TNewfrmGroupSetup.btnCardFontClick(Sender: TObject);
begin
   if cardFontDialog.Execute then
   begin
           edtCardFont.Text := cardFontDialog.Font.Name;
           btnCardSample.Font := cardFontDialog.Font ;
   end;

end;

procedure TNewfrmGroupSetup.btnDeleteNodeClick(Sender: TObject);
var
  ADataSet : TDataSet ;
begin
    if MessageDlg('Are you sure you want to delete node "' +dxDBTreeView1.Selected.text+ '" ?', mtConfirmation,
    [mbYes, mbNo], 0) = mrNo then
        Exit;


    // if node has children then exit
    if dxDBTreeView1.Selected.HasChildren  then
    Begin
      ShowMessage('can''t delete node, The selected node has children');
      exit;
    End;

    ADataSet := dxDBTreeView1.DataSource.DataSet ;

    // delete node
    dataModule1.deleteNode(ADataSet.FieldByName('My_TAG').AsString , ADataSet.FieldByName('PARENT_TAG').AsString);
    dxDBTreeView1.Selected.Delete;
    fixTreeNodeType(ADataSet.FieldByName('PARENT_TAG').AsString);
    ADataSet.Refresh;
    dxDBTreeView1.Refresh;



end;

procedure TNewfrmGroupSetup.btnParentClick(Sender: TObject);
var  AColumn: TcxGridColumn;
begin
     //    AColumn := TableView1.GetColumnByFi eldName('Card);
   //edtTestfilter.Text :=  lbledtTagFilter.Text ;
  FilterByTag(edtTagList.Text,'My_Tag');
  {
      if Assigned(AColumn) then
      AColumn.Visible := not  (edtTestfilter.Text >'') ;
       pnlTagfilter.Visible := not  (edtTestfilter.Text >'') ;
       }
  TableView1.ApplyBestFit;
end;

procedure TNewfrmGroupSetup.btnRefreshClick(Sender: TObject);
var  AColumn: TcxGridColumn;
begin

  AColumn := TableView1.GetColumnByFieldName('PARENT_TAG');
  FilterByTag(edtTestfilter.Text,'PARENT_TAG');
      if Assigned(AColumn) then
      AColumn.Visible := not  (edtTestfilter.Text >'') ;
       pnlTagfilter.Visible := not  (edtTestfilter.Text >'') ;
  TableView1.ApplyBestFit;
end;

procedure TNewfrmGroupSetup.btnSiblingClick(Sender: TObject);
begin
         edtTestfilter.Text :=  edtTagList.Text  ;
         btnRefreshClick(nil);
end;

procedure TNewfrmGroupSetup.btnUpdateClick(Sender: TObject);
var
  ADataSet: TDataSet;
  sCaption, sTagSort, sTable, sPicture, sTagFilter,
  sCardPreCode, sCardFont, sTagList, sStationDept, sTmp : string;
  bCardCode, bCardPrice, bshowInactive, bUseModifier: Boolean;
  iDescLines, iCardWidth, iCardColor: Integer;
begin
  if lbledtTagFilter.Text ='' then
     begin
       ShowMessage('My-Tag Cant be empty' );
       Exit
     end;



  ADataSet := dsTagGroup.DataSet;



  if edtTestfilter.Text>'' then
     edtTagList.Text := edtTestfilter.Text;


  sCaption := lbledtCaption.Text;
  sTagSort := VarToStr(icbSort.EditValue);

  if icbStationDept.ItemIndex<=0 then
          sStationDept := ''
  else
          sStationDept := VarToStr(icbStationDept.EditValue);



  sTable := VarToStr(icbTable.EditValue);

  sPicture := lbledtPicture.Text;
  sTagFilter := lbledtTagFilter.Text;
  sCardPreCode := lbledtCardPreCode.Text;
  sTagList := edtTagList.Text ;
  bCardCode := chkCardCode.Checked;
  bCardPrice := chkCardPrice.Checked;
  bshowInactive := chkshowInactive.Checked;
  bUseModifier := chkUseModifier.Checked;
  sCardFont    := edtCardFont.Text;
  iCardColor := cxColorComboBoxCard.ColorValue;
  iDescLines := seDescLines.Value;
  iCardWidth := seCardWidth.Value;

  ADataSet.Edit;
  ADataSet.FieldByName('Desc').AsString := sCaption;
  ADataSet.FieldByName('CardWidth').AsInteger := iCardWidth;
  ADataSet.FieldByName('CardSortBy').AsString := sTagSort;
  ADataSet.FieldByName('CardDescLines').AsInteger := iDescLines;
  ADataSet.FieldByName('output').AsString := sTable;
  ADataSet.FieldByName('PARENT_TAG').AsString := sTagList;
  ADataSet.FieldByName('STATION_DEPT').AsString := sStationDept;
  ADataSet.FieldByName('CardFont').AsString := sCardFont;
  ADataSet.FieldByName('CardShowCode').AsBoolean := bCardCode;
  ADataSet.FieldByName('CardShowPrice').AsBoolean := bCardPrice;
  ADataSet.FieldByName('CardColor').AsInteger := iCardColor ;
  ADataSet.FieldByName('CardPicture').AsString := sPicture;
  ADataSet.FieldByName('My_TAG').AsString := sTagFilter;
  ADataSet.FieldByName('CardShowInactive').AsBoolean := bshowInactive;
  ADataSet.FieldByName('CardPreString').AsString := sCardPreCode;
  ADataSet.FieldByName('CardUseModifier').AsBoolean := bUseModifier;
  if ADataSet.FieldByName('code').AsString.IsEmpty then
     ADataSet.FieldByName('code').AsString :=  GuidIdString;

  // update breadcrumbs in case station dept is changed
  //sTmp := ADataSet.FieldByName('BREADCRUMBS').AsString;
  //sTmp := sStationDept + Copy(sTmp, pos('|', sTmp), Length(sTmp));
  ADataSet.FieldByName('BREADCRUMBS').AsString := sStationDept + '|' + lbl_breadcrumb.Caption;
  
  ADataSet.Post;


  fixTreeNodeType(sTagFilter);
  ADataSet.Refresh;
  if assigned(dxDBTreeView1.selected) then
    dxDBTreeView1.selected.expand(true);



end;

procedure TNewfrmGroupSetup.btn_ClearClick(Sender: TObject);
begin
  cb_searchCombo.ItemIndex := -1 ;
  DataModule1.searchTagTable('');

end;

procedure TNewfrmGroupSetup.btn_DownNodeClick(Sender: TObject);
var
   ADataSet : TDataSet ;
   SavePlace: TBookmark;
   myTag, parentTag, stationDept, breadCrumbs : string ;
   aDirection : integer;
begin

  ADataSet := dxDBTreeView1.dataSource.DataSet;

    // check if node is the bottom most node
    if ( ADataSet.FieldByName('INDX').asInteger >=
         DataModule1.getMaxChildrenCount(ADataSet.FieldByName('PARENT_TAG').asString) ) then
        exit;


  ADataSet.DisableControls ;
  try
    SavePlace := ADataSet.GetBookmark ;

    myTag := ADataSet.FieldByName('MY_TAG').asString ;
    parentTag := ADataSet.FieldByName('PARENT_TAG').asString ;
    aDirection := 1 ;

    DataModule1.moveNode(myTag , parentTag, aDirection) ;

    fixTreeNodeType('');



          rg_NodeNameClick(self);

      ADataSet.GotoBookmark(SavePlace);



  finally
    ADataSet.FreeBookmark(SavePlace);
  end;

  ADataSet.EnableControls
end;

procedure TNewfrmGroupSetup.btn_searchClick(Sender: TObject);
var
  pWhereStmnt : String ;
begin


      pWhereStmnt := ' Where 1>0 ';
      if cb_searchCombo.ItemIndex >=0 then
         pWhereStmnt := pWhereStmnt + ' AND "BREADCRUMBS" LIKE ''%'+ TSearchData(cb_searchCombo.Items.Objects[cb_searchCombo.ItemIndex]).MY_TAG+'%''';
      if cb_SearchStationDept.ItemIndex >=0 then
         pWhereStmnt := pWhereStmnt + ' AND "STATION_DEPT" = '''+ cb_SearchStationDept.Items[cb_SearchStationDept.ItemIndex]+'''';

      DataModule1.searchTagTable(pWhereStmnt);
      rg_colexpandClick(nil);

end;

procedure TNewfrmGroupSetup.btn_UpNodeClick(Sender: TObject);
var
   ADataSet : TDataSet ;
   SavePlace: TBookmark;
   myTag, parentTag, stationDept, breadCrumbs : string ;
   aDirection : integer;
begin

  ADataSet := dxDBTreeView1.dataSource.DataSet;

    // check if node is the upper most node
    if ( ADataSet.FieldByName('INDX').asInteger <= 1 ) then
        exit;


  ADataSet.DisableControls ;
  try
    SavePlace := ADataSet.GetBookmark ;

    myTag := ADataSet.FieldByName('MY_TAG').asString ;
    parentTag := ADataSet.FieldByName('PARENT_TAG').asString ;
    aDirection := -1 ;

    DataModule1.moveNode(myTag , parentTag, aDirection) ;

    fixTreeNodeType('');



          rg_NodeNameClick(self);

      ADataSet.GotoBookmark(SavePlace);



  finally
    ADataSet.FreeBookmark(SavePlace);
  end;

  ADataSet.EnableControls

end;

function CustomSortProc(Node1, Node2: TTreeNode; aDirection: Longint): Integer; stdcall;
var
  val: Integer;
  Data1,Data2: TTreeDataPtr;

begin
   Data1 := Node1.Data;
   Data2 := Node2.Data;

  //val := -1 ;
  if Assigned(Data1) and Assigned(Data2) then
  begin
 {   if aDirection <> 0 then
    begin
      if Data1^.MORDER < Data2^.INDX then
        val := 1
      else if Data1^.INDX = Data2^.INDX then
        val := 0 ;
    end;
 }
    if aDirection = 0 then
    val := AnsiStrIComp(Pchar(Data1^.MORDER), PChar(Data2^.MORDER))
  else
    val := -AnsiStrIComp(PChar(Data1^.MORDER), PChar(Data2^.MORDER));

  end;
  Exit(val);
end;

procedure TNewfrmGroupSetup.prepareTree(displayField : String = DEFAULT_TREE_TEXT );
var
   aDataSet : TDataSet ;
begin
   aDataSet := dxDBTreeView1.DataSource.DataSet ;
   if assigned(aDataSet) then
   begin
       //aDataSet.Close;
       dxDBTreeView1.DataSource := nil;
       dxDBTreeView1.DataSource := dsTagGroup ;
       dxDBTreeView1.KeyField := 'MY_TAG' ;
       dxDBTreeView1.ParentField := 'PARENT_TAG' ;
       dxDBTreeView1.DisplayField := displayField ;
       dxDBTreeView1.ListField := displayField ;
      // aDataSet.Open;
       dxDBTreeView1.CustomSort(@CustomSortProc, 0);
       lbl_breadcrumb.Caption := getBreadCrumnbs(
                                  aDataSet.FieldByName('STATION_DEPT').AsString,
                                  aDataSet.FieldByName('BREADCRUMBS').AsString
                                  );
   end;
end;



procedure TNewfrmGroupSetup.rg_colexpandClick(Sender: TObject);
begin
   if rg_colexpand.ItemIndex > 0 then
       dxDBTreeView1.FullCollapse
   else
       dxDBTreeView1.FullExpand;

end;

procedure TNewfrmGroupSetup.rg_NodeNameClick(Sender: TObject);
begin
    case rg_NodeName.ItemIndex of
        0 : prepareTree('TAG_CNT');
        1 : prepareTree('DESC_CNT');
        2 : prepareTree('BOTH_CNT');
    end;

    rg_colexpandClick(nil);


end;

procedure TNewfrmGroupSetup.populateCombos;
var
   searchData : TSearchData;
 Items: TcxImageComboBoxItems;

 Item: TcxImageComboBoxItem;
Begin
     // search combo
     cb_searchCombo.Clear;
     With DataModule1 do
     Begin
         q_search.Close;
         q_search.Open;
         while not q_search.Eof do
         Begin
            //cb_searchCombo.Items.Add(q_search.FieldByName('TXT').AsString);
            searchData := TSearchData.Create;
            searchData.MY_TAG := q_search.FieldByName('MY_TAG').AsString ;
            searchData.PARENT_TAG := q_search.FieldByName('PARENT_TAG').AsString ;
            searchData.DESC := q_search.FieldByName('TXT').AsString ;
            cb_searchCombo.AddItem(q_search.FieldByName('TXT').AsString, searchData );

            q_search.Next;
         End;
     End;



     // station dept combs
     icbStationDept.Clear;
     cb_SearchStationDept.clear;
     Items := icbStationDept.Properties.Items;
     Items.BeginUpdate;
     try
        Items.Clear;
        With DataModule1 do
        Begin
             q_StationDept.Close;
             q_StationDept.Open;
             Item := Items.Add as TcxImageComboBoxItem;  // Add a new item
             Item.Value :='NONE';                    // Set the item's value
             Item.Description := 'select a station dept...';        // Set the item's description
             Item.ImageIndex := -1;          // Set the item's image index

             while not q_StationDept.Eof do      // Some iteration
             begin
               Item := Items.Add as TcxImageComboBoxItem;  // Add a new item
               Item.Value := q_StationDeptSTATION_DEPT.AsString;  // Set the item's value
               Item.Description := Item.Value;        // Set the item's description
               Item.ImageIndex := -1;          // Set the item's image index

               // station search combo
               cb_SearchStationDept.Items.Add(q_StationDeptSTATION_DEPT.AsString);
               q_StationDept.Next;

             end;
        End;
     finally
        Items.EndUpdate;
     end;
     // show the tree corresponding t a specific station
     cb_SearchStationDept.ItemIndex := 0;
     btn_searchClick(nil);


End;

procedure TNewfrmGroupSetup.fixTreeNodeType(pParentTag : String );
begin
  try
    DataModule1.fixTagType(true, ITEMS_NODE , pParentTag);
    DataModule1.fixTagType(false, SUBMENU_NODE , pParentTag);
  except
    ShowMessage('Error updating tree node OUTPUT type!');
  end;
end;

function TNewfrmGroupSetup.readTagRow(dataset : TDataSet ) : TTreeDataPtr;
var
  currentRow : TTreeDataPtr ;
Begin
   New(currentRow);
   with dataset do
   Begin
      currentRow^.MY_TAG :=  FieldByName('MY_TAG').asstring;
      currentRow^.PARENT_TAG := FieldByName('PARENT_TAG').asstring;
      currentRow^.DESC := FieldByName('DESC').asstring;
      currentRow^.OUTPUT := FieldByName('OUTPUT').asstring;
      currentRow^.CARD_COLOR := FieldByName('CardColor').AsInteger;
      {
      // if node is not a top node and it has no color then use parent color
      try
          if (currentRow^.CARD_COLOR = 0) and (currentRow^.PARENT_TAG <> '') then
              currentRow^.CARD_COLOR := DataModule1.getParentColor(currentRow^.PARENT_TAG);
      except

      end;
      }

      currentRow^.INDX := FieldByName('INDX').asInteger;
      currentRow^.MORDER := FieldByName('MORDER').asString ;

      currentRow^.STATION_DEPT := FieldByName('STATION_DEPT').asstring;
      currentRow^.BREADCRUMBS := FieldByName('BREADCRUMBS').asstring;

      currentRow^.SMLDESC := FieldByName('smldesc').AsString ;
      currentRow^.INACTIVE := FieldByName('InActive').AsBoolean ;
      currentRow^.LINEID := FieldByName('LineID').AsString ;
      currentRow^.CARD_FONT := FieldByName('CardFont').AsString ;
      currentRow^.CARD_WIDTH := FieldByName('CardWidth').AsInteger ;
      currentRow^.CARD_DESC_LINES := FieldByName('CardDescLines').AsInteger ;
      currentRow^.CARD_SORT_BY := FieldByName('CardSortBy').AsString ;
      currentRow^.CARD_AS_GRID := FieldByName('CardAsGrid').AsBoolean ;
      currentRow^.CARD_PICTURE := FieldByName('CardPicture').AsString ;
      currentRow^.CARD_SHOW_PRICE := FieldByName('CardShowPrice').AsBoolean ;
      currentRow^.CARD_SHOW_CODE := FieldByName('CardShowCode').AsBoolean ;
      currentRow^.CARD_SHOW_INACTIVE := FieldByName('CardShowInactive').AsBoolean ;
      currentRow^.CARD_PRESTRING := FieldByName('CardPreString').AsString ;
      currentRow^.CARD_USE_MODIFIER := FieldByName('CardUseModifier').AsBoolean ;
      currentRow^.CODE := FieldByName('code').AsString ;
      currentRow^.Price := FieldByName('price').AsFloat ;
   end;


   result := currentRow ;

End;



procedure TNewfrmGroupSetup.displayTagProperties(ADataSet: TDataSet);
var

  aI: Integer;

begin

  // Card settings...
  icbTable.EditValue := LowerCase(ADataSet.FieldByName('output').AsString);
  if icbTable.ItemIndex=-1 then            // if empty or not in list default to ITEM
    icbTable.ItemIndex := 0;

  icbStationDept.EditValue := ADataSet.FieldByName('STATION_DEPT').AsString;
  if icbStationDept.ItemIndex=-1 then
    icbStationDept.ItemIndex := 0;


  lbledtCaption.Text := ADataSet.FieldByName('Desc').AsString;
  edtTagList.Text := ADataSet.FieldByName('PARENT_TAG').AsString;
  lbledtTagFilter.Text := ADataSet.FieldByName('My_TAG').AsString;
  chkCardPrice.Checked := ADataSet.FieldByName('CardShowPrice').AsBoolean;
  chkCardCode.Checked := ADataSet.FieldByName('CardShowCode').AsBoolean;
  edtCardFont.Text := ADataSet.FieldByName('CardFont').AsString;
  //aI := getNodeColor(ADataSet.FieldByName('CardColor').AsInteger, edtTagList.Text);
  aI := ADataSet.FieldByName('CardColor').AsInteger;
  if aI > 0  then
     cxColorComboBoxCard.ColorValue := iif(aI > 0, aI, cxColorComboBoxCard.ColorValue)
  else
     cxColorComboBoxCard.ItemIndex := -1 ;



  // Misc Settings ...
  chkshowInactive.Checked := ADataSet.FieldByName('CardShowInactive').AsBoolean;
  lbledtCardPreCode.Text := ADataSet.FieldByName('CardPreString').AsString;
  chkUseModifier.Checked := ADataSet.FieldByName('CardUseModifier').AsBoolean;


  // Group Settings ...
  icbSort.EditValue := LowerCase(ADataSet.FieldByName('CardSortBy').AsString);
  seDescLines.Value := ADataSet.FieldByName('CardDescLines').AsInteger;
  seCardWidth.Value := ADataSet.FieldByName('CardWidth').AsInteger;
  lbledtPicture.Text := ADataSet.FieldByName('CardPicture').AsString;


  lblParenValue.Caption := ADataSet.FieldByName('PARENT_TAG').AsString;
  lblMyTagValue.Caption := ADataSet.FieldByName('My_Tag').AsString;


end;


procedure TNewfrmGroupSetup.cxColorComboBoxCardPropertiesChange(Sender: TObject);
begin
  // btnSample.Color := cxColorComboBox01.ColorValue;
  if cxColorComboBoxCard.ColorValue > 0 then
    btnCardSample.Color := iif(cxColorComboBoxCard.ColorValue > 0, cxColorComboBoxCard.ColorValue, btnCardSample.Color)
  else
    btnCardSample.Color := clBtnFace ;
end;

procedure TNewfrmGroupSetup.DeleteSelectedNode1Click(Sender: TObject);
begin
   btnDeleteNodeClick(self);
end;

procedure TNewfrmGroupSetup.dsTagGroupDataChange(Sender: TObject; Field: TField);
begin
  displayTagProperties(dsTagGroup.DataSet);

end;

procedure TNewfrmGroupSetup.dxDBTreeView1Click(Sender: TObject);
var
  ADataSet: TDataSet;
    AColumn: TcxGridColumn;
begin
     ADataSet := dsTagGroup.DataSet;
          AColumn := TableView1.GetColumnByFieldName('PARENT_TAG');
          edtTestfilter.Text := ADataSet.FieldByName('PARENT_TAG').AsString;
  //FilterByTag(edtTestfilter.Text,'PARENT_TAG');
      if Assigned(AColumn) then
      AColumn.Visible := not  (edtTestfilter.Text >'') ;
       pnlTagfilter.Visible := not  (edtTestfilter.Text >'') ;
  TableView1.ApplyBestFit;


  displayTagProperties(ADataSet);

  lbl_breadcrumb.Caption := getBreadCrumnbs(
                                aDataSet.FieldByName('STATION_DEPT').AsString,
                                aDataSet.FieldByName('BREADCRUMBS').AsString
                            );
end;


procedure TNewfrmGroupSetup.dxDBTreeView1CustomDraw(Sender: TObject;
  TreeNode: TTreeNode; AFont: TFont; var AColor, ABkColor: TColor);
var
  Data: TTreeDataPtr;
  ParentData: TTreeDataPtr;
  R : TRect;
begin

  try
   Data := TreeNode.Data;
   if assigned(TreeNode.Parent) then
       ParentData := TreeNode.Parent.Data;
  if Assigned(Data) then
  begin
    AFont.Name := Data^.CARD_FONT;
    if Data^.CARD_COLOR > 0 then
       AColor := TColor(Data^.CARD_COLOR);



  {
    // if node has color then show it
    if Data^.CARD_COLOR  <> 0 then
       AColor := TColor(Data^.CARD_COLOR)
    else if assigned(ParentData) then
       AColor := TColor(ParentData^.CARD_COLOR);

 }

  end
  except
    AFont.Style := [];
  end;


end;


procedure TNewfrmGroupSetup.dxDBTreeView1DragDropTreeNode(Destination,
  Source: TTreeNode; var Accept: Boolean);
var
   srcKey, dstKey, dstBreadCrumbs : String  ;
   dstData : TTreeDataPtr;
begin

    try
        if not Assigned(Destination) then
         begin
              exit;
         end;
        srcKey := TdxDBTreeNode(Source).KeyFieldValue ;
        dstKey := TdxDBTreeNode(Destination).KeyFieldValue  ;
        dstData := TdxDBTreeNode(Destination).Data;
        dstBreadCrumbs := dstData^.BREADCRUMBS ;



        {
        if assigned(dstData) then
        begin

            if dstData^.OUTPUT = ITEMS_NODE then
            begin
                ShowMessage('you can''t drag this sub-tree under an "ITEMS" node!');
                Accept := false ;
                exit;
            end;

        end;
        }
        DataModule1.updateNodeParent(srcKey, dstKey, dstBreadCrumbs );
        fixTreeNodeType(''{dstKey});
        Source.MoveTo(Destination, naAddChild);
        dxDBTreeView1.FullCollapse;
        dxDBTreeView1.FullExpand;

        Accept := true ;
    except
        Accept := false ;
        ShowMessage('Can not drag the selected node...');

    end;


end;


procedure TNewfrmGroupSetup.dxDBTreeView1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
var
  Src, Dst: TTreeNode;
begin
  Src := dxDBTreeView1.Selected;
  Dst := dxDBTreeView1.GetNodeAt(X,Y);
  Accept := Assigned(Dst) and (Src<>Dst);
end;

procedure TNewfrmGroupSetup.dxDBTreeView1RefreshNode(Sender: TObject;
  DBTreeNode: TdxDBTreeNode);
var
  currentRow : TTreeDataPtr ;
begin
 if assigned(TdxDBTreeView(Sender).DataSource.DataSet) then
 begin
      currentRow := readTagRow (TdxDBTreeView(Sender).DataSource.DataSet);
      DBTreeNode.Data := currentRow;
 end ;
end;



procedure TNewfrmGroupSetup.FilterByTag(sFilter,sField: string);
var
  aDataSet : TDataSet ;
begin
  aDataSet := dxDBTreeView1.DataSource.DataSet ;
  if assigned(aDataSet) then
  begin
      aDataSet.DisableControls;
      try
        aDataSet.Filtered := False;
        if sFilter='' then
          aDataSet.Filter := ''
        else
        begin
          aDataSet.Filter := Format('Contains(%s, %s)', [sField ,QuotedStr(sFilter)]);
         //dmPrdData.tblTagGroup.Filter := Format('Contains(TagList, %s)', [QuotedStr(sFilter)]   );
          aDataSet.Filtered := True;
        end;
      finally
        aDataSet.EnableControls;
      end;
  end;

end;

procedure TNewfrmGroupSetup.q_TagGroupAfterScroll(DataSet: TDataSet);
var
  ADataSet: TDataSet;
begin
  ADataSet := dsTagGroup.DataSet;
  displayTagProperties(ADataSet);

  lbl_breadcrumb.Caption := getBreadCrumnbs(
                                aDataSet.FieldByName('STATION_DEPT').AsString,
                                aDataSet.FieldByName('BREADCRUMBS').AsString
                            );
end;

procedure TNewfrmGroupSetup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   try
      fixTreeNodeType('');
   except

   end;


end;

procedure TNewfrmGroupSetup.FormCreate(Sender: TObject);
var
   Item: TcxImageComboBoxItem;
   i : integer ;
begin
   // init a hash list that will contains all distinct station_dept
   StationDept_List := System.Generics.Collections.TDictionary<String, String>.Create;
   StationDept_List.clear ;


   fixTreeNodeType('');

   populateCombos;

   // after populating station combo for the first time 
   // we populate StationDept_List
   // so when user adds new station we save it in StationDept_List
   for i:=0 to icbStationDept.Properties.Items.Count-1 do
        StationDept_List.Add(icbStationDept.Properties.Items[i].Value,
                             icbStationDept.Properties.Items[i].Value);

     
   prepareTree;
   dxDBTreeView1.FullExpand;
   dxDBTreeView1.DataSource.DataSet.First;
   displayTagProperties(dxDBTreeView1.DataSource.DataSet);
end;

procedure TNewfrmGroupSetup.FormShow(Sender: TObject);
var
  AColumn: TcxGridColumn;
begin
  colrfil();
  //DataModule1.POSConnection.IsConnected := True;
  //dxDBTreeView1.DataSource.DataSet.Active := True;

  TableView1.DataController.CreateAllItems;

 {
  AColumn := TableView1.GetColumnByFieldName('TagList');
  if Assigned(AColumn) then
  begin
    AColumn.PropertiesClass := TTagTokenEditorProperties;
    AColumn.Caption := 'Parent Tag' ;
    //TTagTokenEditorProperties(AColumn.Properties).PopupDataSource
  end;


  AColumn := TableView1.GetColumnByFieldName('CardFilterTag');
  if Assigned(AColumn) then
    begin
    AColumn.PropertiesClass := TTagTokenEditorProperties;
    AColumn.Caption := 'My Tag' ;
  end;
  }


  {
  AColumn := TableView1.GetColumnByFieldName('LineID');
  if Assigned(AColumn) then
    AColumn.Visible := False;

  AColumn := TableView1.GetColumnByFieldName('SmlDesc');
  if Assigned(AColumn) then
    AColumn.Visible := False;

  AColumn := TableView1.GetColumnByFieldName('Price');
  if Assigned(AColumn) then
    AColumn.Visible := False;

  AColumn := TableView1.GetColumnByFieldName('code');
  if Assigned(AColumn) then
    AColumn.Visible := False;


  AColumn := TableView1.GetColumnByFieldName('cat');
  if Assigned(AColumn) then
    AColumn.Visible := False;

  if  edtTestfilter.Text >'' then
  begin
       pnlTagfilter.Visible := False ;
     AColumn := TableView1.GetColumnByFieldName('PARENT_TAG');
     FilterByTag(edtTestfilter.Text,'PARENT_TAG');
     if Assigned(AColumn) then
        AColumn.Visible :=  False ;
  end;
  TableView1.ApplyBestFit;


 dxDBTreeView1.KeyField := 'My_Tag' ;
 //dxDBTreeView1.ListField := 'My_Tag' ;
 dxDBTreeView1.DisplayField := 'Desc' ;
 dxDBTreeView1.ParentField := 'PARENT_TAG' ;

 }

   // TagTokenEditor1.Visible := False ; //not used yet
end;

procedure TNewfrmGroupSetup.seCardWidthChange(Sender: TObject);
begin
  btnCardSample.Width := Max(seCardWidth.Value, 75);
end;

procedure TNewfrmGroupSetup.cb_SearchStationDeptClick(Sender: TObject);
begin
   if cb_SearchStationDept.itemIndex >= 0 then
   begin
       btn_searchClick(self);
   end;
end;

procedure TNewfrmGroupSetup.colrfil();

var
  AColor: TColor;
  intcolor: Integer;
  i: Integer;
begin
  TcxColorComboBoxProperties(cxColorComboBoxCard.Properties).NamingConvention := cxncNone;

  // AColor := RGB(255,255,1);  // Yellow

  for i := 1 to Length(clrList) do
  begin
    intcolor := clrList[i].hexA100.ToInteger();
    AColor := RGB(GetBValue(intcolor), GetGValue(intcolor), GetRValue(intcolor)); // red
    // TcxColorComboBoxProperties(cxColorComboBox01.Properties).CustomColors.AddColor(AColor, clrList[i].Name);
    TcxColorComboBoxProperties(cxColorComboBoxCard.Properties).CustomColors.AddColor(AColor, clrList[i].Name);
  end;


end;


function TNewfrmGroupSetup.GuidIdString: string;

var
  Year, Month, Day: Word;
  CurTimePart: Integer;
begin

  DecodeDate(Now, Year, Month, Day);
  Result := '';
  //Result := Result + LocationCode; // 1 LocationCode
  //Result := Result + StationCode; // 1 StationCode
  Result := Result + IntToHex(Year - 2000, 1); // 1 Year - in year 2016 it will addtnl digit
  Result := Result + Chr(Month + 64); // 1 Month
  if Day < 10 then
    Result := Result + IntToStr(Day) // 1 Day in Month
  else
    Result := Result + Chr(Day + 55);
  CurTimePart := Round(Frac(Now) * 24 * 60 * 60);
  FGuidLastTimePart := Max(CurTimePart, FGuidLastTimePart + 1);
  Result := Result + IntToHex(FGuidLastTimePart, 5); // 5 Time
  Result := Result + IntToHex(RandomRange(1, 32), 2); // 2 digit Random
end;

procedure TNewfrmGroupSetup.mnuCreateChildClick(Sender: TObject);
var
   ADataSet : TDataSet ;
   SavePlace: TBookmark;
   myTag, parentTag, stationDept, breadCrumbs : string ;
begin
    if MessageDlg('create a child under "' +dxDBTreeView1.Selected.text+ '" ?', mtConfirmation,
    [mbYes, mbNo], 0) = mrNo then
        Exit;

     ADataSet := dsTagGroup.DataSet;

  {
  //  'Items' output can't have children
  if icbTable.ItemIndex <= 0  then
  begin
     ShowMessage('you can''t add a child to an "ITEMS" node ');
     exit ;
  end;
  }

  ADataSet.DisableControls ;
  try
    SavePlace := ADataSet.GetBookmark ;

    myTag := 'new child' + intToStr(ADataSet.RecordCount+1) ;
    parentTag    := ADataSet.FieldByName('MY_TAG').AsString ;
    stationDept  := ADataSet.FieldByName('STATION_DEPT').AsString ;
    breadCrumbs  := ADataSet.FieldByName('BREADCRUMBS').AsString ;
    DataModule1.createTagNode(myTag , parentTag, stationDept, breadCrumbs+'|'+myTag) ;

    fixTreeNodeType('');

    aDataset.Close;
    aDataset.Open;


    dxDBTreeView1.FullExpand ;

    ADataSet.GotoBookmark(SavePlace);


  finally
    ADataSet.FreeBookmark(SavePlace);
  end;

  ADataSet.EnableControls

end;

procedure TNewfrmGroupSetup.mnuCreateParentClick(Sender: TObject);
var
   ADataSet : TDataSet ;
   SavePlace: TBookmark;
   myTag, parentTag, stationDept, breadCrumbs : string ;
begin

    // check station combos
    if (icbStationDept.ItemIndex < 0) and
       (cb_SearchStationDept.ItemIndex < 0) then
    begin
        ShowMessage('Choose a station, please ...');
        exit;
    end;


  ADataSet := dxDBTreeView1.dataSource.DataSet;
  ADataSet.DisableControls ;
  try
    SavePlace := ADataSet.GetBookmark ;

    myTag := 'new parent' + intToStr(dxDBTreeView1.dataSource.DataSet.RecordCount+1) ;
    parentTag    := '' ;

     // choose station from station combo
    if cb_SearchStationDept.ItemIndex < 0 then
         stationDept := VarToStr(icbStationDept.EditValue)
    else
         stationDept := cb_SearchStationDept.Items[cb_SearchStationDept.ItemIndex];



    breadCrumbs  := stationDept +'|'+myTag ;
    DataModule1.createTagNode(myTag , parentTag, stationDept, breadCrumbs) ;

    fixTreeNodeType('');

    aDataset.Close;
    aDataset.Open;
    dxDBTreeView1.Refresh ;
    dxDBTreeView1.FullExpand ;

    ADataSet.GotoBookmark(SavePlace);


  finally
    ADataSet.FreeBookmark(SavePlace);
  end;

  ADataSet.EnableControls
end;

end.
