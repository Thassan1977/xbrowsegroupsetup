object DataModule1: TDataModule1
  OldCreateOrder = False
  Height = 339
  Width = 570
  object tblTagGroup: TAdsTable
    DatabaseName = 'POSConnection'
    StoreActive = False
    AdsConnection = POSConnection
    TableName = 'taggroup.adt'
    Left = 122
    Top = 16
    object tblTagGroupMY_TAG: TAdsStringField
      DisplayWidth = 25
      FieldName = 'MY_TAG'
      Size = 25
    end
    object tblTagGroupPARENT_TAG: TAdsStringField
      FieldName = 'PARENT_TAG'
      Size = 25
    end
    object tblTagGroupdesc: TAdsStringField
      FieldName = 'desc'
      Size = 28
    end
    object tblTagGroupOUTPUT: TAdsStringField
      FieldName = 'OUTPUT'
      Size = 10
    end
    object tblTagGroupCardColor: TIntegerField
      FieldName = 'CardColor'
    end
    object tblTagGroupSTATION_DEPT: TAdsStringField
      FieldName = 'STATION_DEPT'
      Size = 15
    end
    object tblTagGroupBREADCRUMBS: TAdsStringField
      FieldName = 'BREADCRUMBS'
      Size = 200
    end
    object tblTagGroupsmldesc: TAdsStringField
      FieldName = 'smldesc'
    end
    object tblTagGroupInActive: TBooleanField
      FieldName = 'InActive'
    end
    object tblTagGroupLineID: TAdsStringField
      FieldName = 'LineID'
      Size = 40
    end
    object tblTagGroupCardFont: TAdsStringField
      FieldName = 'CardFont'
    end
    object tblTagGroupCardWidth: TIntegerField
      FieldName = 'CardWidth'
    end
    object tblTagGroupCardDescLines: TIntegerField
      FieldName = 'CardDescLines'
    end
    object tblTagGroupCardSortBy: TAdsStringField
      FieldName = 'CardSortBy'
    end
    object tblTagGroupCardAsGrid: TBooleanField
      FieldName = 'CardAsGrid'
    end
    object tblTagGroupCardPicture: TAdsStringField
      FieldName = 'CardPicture'
      Size = 50
    end
    object tblTagGroupCardShowPrice: TBooleanField
      FieldName = 'CardShowPrice'
    end
    object tblTagGroupCardShowCode: TBooleanField
      FieldName = 'CardShowCode'
    end
    object tblTagGroupCardShowInactive: TBooleanField
      FieldName = 'CardShowInactive'
    end
    object tblTagGroupCardPreString: TAdsStringField
      FieldName = 'CardPreString'
      Size = 10
    end
    object tblTagGroupCardUseModifier: TBooleanField
      FieldName = 'CardUseModifier'
    end
    object tblTagGroupcode: TAdsStringField
      FieldName = 'code'
    end
    object tblTagGroupprice: TAdsBCDField
      FieldName = 'price'
      currency = True
      Precision = 20
    end
    object tblTagGrouptagdesc: TStringField
      FieldKind = fkCalculated
      FieldName = 'tagdesc'
      Calculated = True
    end
  end
  object POSConnection: TAdsConnection
    AdsServerTypes = [stADS_REMOTE, stADS_LOCAL]
    LoginPrompt = False
    StoreConnected = False
    SQLTimeout = 1
    Left = 32
    Top = 16
  end
  object AdsSettings1: TAdsSettings
    DateFormat = 'MM/dd/ccyy'
    ShowDeleted = False
    AdsServerTypes = [stADS_LOCAL]
    Left = 32
    Top = 120
  end
  object q_search: TAdsQuery
    SQL.Strings = (
      
        'SELECT  "MY_TAG", "PARENT_TAG" , "MY_TAG" + '#39' - '#39' + "desc" as  T' +
        'XT  FROM taggroup')
    AdsConnection = POSConnection
    Left = 216
    Top = 160
    ParamData = <>
    object q_searchTXT: TAdsStringField
      FieldName = 'TXT'
      Size = 46
    end
    object q_searchMY_TAG: TAdsStringField
      FieldName = 'MY_TAG'
      Size = 25
    end
    object q_searchPARENT_TAG: TAdsStringField
      FieldName = 'PARENT_TAG'
      Size = 25
    end
  end
  object q_tmp: TAdsQuery
    AdsConnection = POSConnection
    Left = 176
    Top = 232
    ParamData = <>
  end
  object q_TagList: TAdsQuery
    RequestLive = True
    SQL.Strings = (
      'select * from TagList')
    AdsConnection = POSConnection
    Left = 104
    Top = 232
    ParamData = <>
    object q_TagListLineID: TAdsStringField
      FieldName = 'LineID'
      Size = 40
    end
    object q_TagListTableName: TAdsStringField
      FieldName = 'TableName'
      Size = 50
    end
    object q_TagListTagName: TAdsStringField
      FieldName = 'TagName'
      Size = 50
    end
    object q_TagListCnt: TIntegerField
      FieldName = 'Cnt'
    end
    object q_TagListUpdatedOn: TDateTimeField
      FieldName = 'UpdatedOn'
    end
  end
  object q_TagGroup: TAdsQuery
    BeforePost = q_TagGroupBeforePost
    AfterPost = q_TagGroupAfterPost
    OnCalcFields = q_TagGroupCalcFields
    ReadAllColumns = True
    RequestLive = True
    SQL.Strings = (
      
        'select * from taggroup ORDER BY REPLACE(UPPER(BREADCRUMBS),UPPER' +
        '(MY_TAG), STR(INDX, 4, 0) )')
    AdsConnection = POSConnection
    Left = 248
    Top = 224
    ParamData = <>
    object q_TagGroupMY_TAG: TAdsStringField
      FieldName = 'MY_TAG'
      Size = 25
    end
    object q_TagGroupPARENT_TAG: TAdsStringField
      FieldName = 'PARENT_TAG'
      Size = 25
    end
    object q_TagGroupdesc: TAdsStringField
      FieldName = 'desc'
      Size = 28
    end
    object q_TagGroupOUTPUT: TAdsStringField
      FieldName = 'OUTPUT'
      Size = 10
    end
    object q_TagGroupCardColor: TIntegerField
      FieldName = 'CardColor'
    end
    object q_TagGroupSTATION_DEPT: TAdsStringField
      FieldName = 'STATION_DEPT'
      Size = 15
    end
    object q_TagGroupBREADCRUMBS: TAdsStringField
      FieldName = 'BREADCRUMBS'
      Size = 200
    end
    object q_TagGroupsmldesc: TAdsStringField
      FieldName = 'smldesc'
    end
    object q_TagGroupInActive: TBooleanField
      FieldName = 'InActive'
    end
    object q_TagGroupLineID: TAdsStringField
      FieldName = 'LineID'
      Size = 40
    end
    object q_TagGroupCardFont: TAdsStringField
      FieldName = 'CardFont'
    end
    object q_TagGroupCardWidth: TIntegerField
      FieldName = 'CardWidth'
    end
    object q_TagGroupCardDescLines: TIntegerField
      FieldName = 'CardDescLines'
    end
    object q_TagGroupCardSortBy: TAdsStringField
      FieldName = 'CardSortBy'
    end
    object q_TagGroupCardAsGrid: TBooleanField
      FieldName = 'CardAsGrid'
    end
    object q_TagGroupCardPicture: TAdsStringField
      FieldName = 'CardPicture'
      Size = 50
    end
    object q_TagGroupCardShowPrice: TBooleanField
      FieldName = 'CardShowPrice'
    end
    object q_TagGroupCardShowCode: TBooleanField
      FieldName = 'CardShowCode'
    end
    object q_TagGroupCardShowInactive: TBooleanField
      FieldName = 'CardShowInactive'
    end
    object q_TagGroupCardPreString: TAdsStringField
      FieldName = 'CardPreString'
      Size = 10
    end
    object q_TagGroupCardUseModifier: TBooleanField
      FieldName = 'CardUseModifier'
    end
    object q_TagGroupcode: TAdsStringField
      FieldName = 'code'
    end
    object q_TagGroupprice: TAdsBCDField
      FieldName = 'price'
      currency = True
      Precision = 20
    end
    object q_TagGroupTAG_CNT: TStringField
      FieldKind = fkCalculated
      FieldName = 'TAG_CNT'
      Size = 400
      Calculated = True
    end
    object q_TagGroupDESC_CNT: TStringField
      FieldKind = fkCalculated
      FieldName = 'DESC_CNT'
      Size = 400
      Calculated = True
    end
    object q_TagGroupBOTH_CNT: TStringField
      FieldKind = fkCalculated
      FieldName = 'BOTH_CNT'
      Size = 400
      Calculated = True
    end
    object q_TagGroupINDX: TIntegerField
      FieldName = 'INDX'
    end
    object q_TagGroupMORDER: TStringField
      FieldKind = fkCalculated
      FieldName = 'MORDER'
      Calculated = True
    end
  end
  object q_StationDept: TAdsQuery
    SQL.Strings = (
      'SELECT  DISTINCT "STATION_DEPT" FROM taggroup')
    AdsConnection = POSConnection
    Left = 216
    Top = 88
    ParamData = <>
    object q_StationDeptSTATION_DEPT: TAdsStringField
      FieldName = 'STATION_DEPT'
      Size = 15
    end
  end
  object qInsetNode: TAdsQuery
    RequestLive = True
    SQL.Strings = (
      
        'INSERT INTO taggroup ("desc", "MY_TAG", "PARENT_TAG","STATION_DE' +
        'PT", "BREADCRUMBS", "OUTPUT", "INDX", "CardColor", "CardWidth", ' +
        '"CardDescLines")'
      'VALUES (:p1, :p2, :p3, :p4, :p5, :p6, :p7, :p8, 150, 1)')
    AdsConnection = POSConnection
    Left = 392
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p8'
        ParamType = ptUnknown
      end>
  end
  object q_getNodes: TAdsQuery
    AdsConnection = POSConnection
    Left = 384
    Top = 24
    ParamData = <>
  end
  object q_tmp2: TAdsQuery
    AdsConnection = POSConnection
    Left = 216
    Top = 16
    ParamData = <>
  end
  object q_tagDropDown: TAdsQuery
    OnCalcFields = q_tagDropDownCalcFields
    SQL.Strings = (
      'SELECT "TagName" , "Cnt" FROM tagList Order By "TagName"')
    AdsConnection = POSConnection
    Left = 480
    Top = 160
    ParamData = <>
    object q_tagDropDownTagName: TAdsStringField
      FieldName = 'TagName'
      Size = 50
    end
    object q_tagDropDownCnt: TIntegerField
      FieldName = 'Cnt'
    end
    object q_tagDropDownTXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'TXT'
      Calculated = True
    end
  end
  object qCloneStationDept: TAdsQuery
    SQL.Strings = (
      
        'SELECT  "MY_TAG", "PARENT_TAG" , "MY_TAG" + '#39' - '#39' + "desc" as  T' +
        'XT  FROM taggroup')
    AdsConnection = POSConnection
    Left = 120
    Top = 160
    ParamData = <>
    object AdsStringField1: TAdsStringField
      FieldName = 'TXT'
      Size = 46
    end
    object AdsStringField2: TAdsStringField
      FieldName = 'MY_TAG'
      Size = 25
    end
    object AdsStringField3: TAdsStringField
      FieldName = 'PARENT_TAG'
      Size = 25
    end
  end
  object q_CloneBrachList: TAdsQuery
    ReadAllColumns = True
    RequestLive = True
    SQL.Strings = (
      'select * from taggroup '
      'where "STATION_DEPT" = :pSD'
      
        'ORDER BY REPLACE(UPPER(BREADCRUMBS),UPPER(MY_TAG), STR(INDX, 4, ' +
        '0) )')
    AdsConnection = POSConnection
    Left = 120
    Top = 80
    ParamData = <
      item
        DataType = ftString
        Name = 'pSD'
        ParamType = ptUnknown
      end>
    object AdsStringField4: TAdsStringField
      FieldName = 'MY_TAG'
      Size = 25
    end
    object AdsStringField5: TAdsStringField
      FieldName = 'PARENT_TAG'
      Size = 25
    end
    object AdsStringField6: TAdsStringField
      FieldName = 'desc'
      Size = 28
    end
    object AdsStringField7: TAdsStringField
      FieldName = 'OUTPUT'
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'CardColor'
    end
    object AdsStringField8: TAdsStringField
      FieldName = 'STATION_DEPT'
      Size = 15
    end
    object AdsStringField9: TAdsStringField
      FieldName = 'BREADCRUMBS'
      Size = 200
    end
    object AdsStringField10: TAdsStringField
      FieldName = 'smldesc'
    end
    object BooleanField1: TBooleanField
      FieldName = 'InActive'
    end
    object AdsStringField11: TAdsStringField
      FieldName = 'LineID'
      Size = 40
    end
    object AdsStringField12: TAdsStringField
      FieldName = 'CardFont'
    end
    object IntegerField2: TIntegerField
      FieldName = 'CardWidth'
    end
    object IntegerField3: TIntegerField
      FieldName = 'CardDescLines'
    end
    object AdsStringField13: TAdsStringField
      FieldName = 'CardSortBy'
    end
    object BooleanField2: TBooleanField
      FieldName = 'CardAsGrid'
    end
    object AdsStringField14: TAdsStringField
      FieldName = 'CardPicture'
      Size = 50
    end
    object BooleanField3: TBooleanField
      FieldName = 'CardShowPrice'
    end
    object BooleanField4: TBooleanField
      FieldName = 'CardShowCode'
    end
    object BooleanField5: TBooleanField
      FieldName = 'CardShowInactive'
    end
    object AdsStringField15: TAdsStringField
      FieldName = 'CardPreString'
      Size = 10
    end
    object BooleanField6: TBooleanField
      FieldName = 'CardUseModifier'
    end
    object AdsStringField16: TAdsStringField
      FieldName = 'code'
    end
    object AdsBCDField1: TAdsBCDField
      FieldName = 'price'
      currency = True
      Precision = 20
    end
    object StringField1: TStringField
      FieldKind = fkCalculated
      FieldName = 'TAG_CNT'
      Size = 400
      Calculated = True
    end
    object StringField2: TStringField
      FieldKind = fkCalculated
      FieldName = 'DESC_CNT'
      Size = 400
      Calculated = True
    end
    object StringField3: TStringField
      FieldKind = fkCalculated
      FieldName = 'BOTH_CNT'
      Size = 400
      Calculated = True
    end
    object IntegerField4: TIntegerField
      FieldName = 'INDX'
    end
    object StringField4: TStringField
      FieldKind = fkCalculated
      FieldName = 'MORDER'
      Calculated = True
    end
  end
end
