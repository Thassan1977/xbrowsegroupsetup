unit uFormAddStationDept;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  dmData;

type
  Tfrm_AddStatioDept = class(TForm)
    Label1: TLabel;
    edt_stationdept: TEdit;
    Panel1: TPanel;
    btn_OK: TBitBtn;
    btn_Cancel: TBitBtn;
    procedure btn_OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_AddStatioDept: Tfrm_AddStatioDept;

implementation

{$R *.dfm}

procedure Tfrm_AddStatioDept.btn_OKClick(Sender: TObject);
begin
  ModalResult := mrNone;

  if edt_stationdept.Text = '' then
  begin
    ShowMessage('Please fill in the Station name...');
    exit;
  end;

  // if the selected station does exist then it can't be added "already exists" ..
  if DataModule1.checkStationExists(edt_stationdept.Text) then
  begin
    ShowMessage('The entered Station name already exists!' + #13 + 'please select another name...');
    exit;
  end;

  ModalResult := mrOk;

end;

end.
